# Quake Map Generator

A set of Python scripts for generating randomised Quake maps from prefabricated map pieces.

![A screenshot of a generated level](screenshot.jpg)*A generated level*

### Usage

Generate a level tilemap using the _hauberk_ generator:

```shell
python -m mapgen.generator.hauberk
```

Generate a `test.map` file from a _hauberk_ tilemap:

```shell
python -m tilemap_to_map
```

## tmx_to_map

Takes a Tiled `.tmx` file and builds a Quake map based on the _tile-to-prefab_ mappings in the `.json` file, e.g.:

```shell
python -m tmx_to_map -d test.map examples/tmx2map/simple.tmx examples/tmx2map/simple.json
```
