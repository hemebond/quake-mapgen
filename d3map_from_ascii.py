from pathlib import Path
from mapgen.tilemap import TileMap
from mapgen.tiles import Tiles as T, getTile
from mapgen.prefab_manager import PrefabManager
from mapgen.qmap import QuakeMap, parse_quakemap
from mapgen.vec import Vec



TILE_SIZE = 128
LEVEL_HEIGHT = 128



def main():
	with open('ascii.txt') as f:
		src = f.read().splitlines()

	tilemap = TileMap.from_strings([src])

	# with open("prefabs/d3/base.map") as f:
	# 	m = parse_quakemap(f)

	prefabs = PrefabManager(Path("prefabs/d3"))

	m = prefabs.get("base")

	wall_tiles = (T.WALL_1, T.WALL_2, T.WALL_3, T.WALL_4, T.WALL_T, T.WALL_H, T.WALL_V)

	tile_mapping = {
		"+": (prefabs.get("door"), 0),
		".": (prefabs.get("floor"), 0),
	}

	for pos, tile in tilemap.items():
		if tile is None:
			continue

		try:
			prefab, angle = tile_mapping[str(tile.symbol)]
		except KeyError:
			pass

		pos = Vec(pos)

		# Door
		if tile == T.DOOR:
			awalls = [tilemap.get(apos) in wall_tiles for apos in pos.adjacent]

			if awalls[1] and awalls[3]:
				angle = 0
			if awalls[0] and awalls[2]:
				angle = 90

			m.merge(prefab, pos.x * TILE_SIZE, pos.y * TILE_SIZE, pos.z * LEVEL_HEIGHT, angle)
			continue

		if tile == T.FLOOR:
			m.merge(prefab, pos.x * TILE_SIZE, pos.y * TILE_SIZE, pos.z * LEVEL_HEIGHT, 0)
			continue

		if tile in wall_tiles:
			afloors = [tilemap.get(apos) == T.FLOOR for apos in pos.adjacent]

			prefab = prefabs.get("wall")

			if afloors[0]:
				m.merge(prefab, pos.x * TILE_SIZE, pos.y * TILE_SIZE, pos.z * LEVEL_HEIGHT, 0)
			if afloors[1]:
				m.merge(prefab, pos.x * TILE_SIZE, pos.y * TILE_SIZE, pos.z * LEVEL_HEIGHT, 90)
			if afloors[2]:
				m.merge(prefab, pos.x * TILE_SIZE, pos.y * TILE_SIZE, pos.z * LEVEL_HEIGHT, 180)
			if afloors[3]:
				m.merge(prefab, pos.x * TILE_SIZE, pos.y * TILE_SIZE, pos.z * LEVEL_HEIGHT, 270)



	with open('test.map', 'w') as fp:
		fp.write(str(m))


if __name__ == '__main__':
	main()
