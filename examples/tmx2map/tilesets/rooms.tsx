<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.3.3" name="rooms" tilewidth="9" tileheight="9" tilecount="50" columns="5">
 <tileoffset x="-4" y="4"/>
 <image source="rooms.png" width="45" height="90"/>
 <tile id="0">
  <properties>
   <property name="height" type="int" value="7"/>
   <property name="width" type="int" value="7"/>
  </properties>
 </tile>
</tileset>
