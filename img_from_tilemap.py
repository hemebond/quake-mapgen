from PIL import Image, ImageDraw
from mapgen.tilemap import TileMap
from mapgen.loggers.tcod import TcodLogger
from mapgen.generators.hauberk import HauberkGenerator
from mapgen.tiles import Tiles as T
from mapgen.vec import Vec



def tilemap_to_img(generator, z=0):
	im = Image.new(mode="RGB", size=(generator.tilemap.width, generator.tilemap.height))
	d = ImageDraw.Draw(im)  # get draw context

	# 1,1 is the top-left of the image
	# d.point((1, 1), (255, 0, 0))

	for pos, tile in generator.tilemap.items():
		pos = Vec(pos)

		if pos.z != z:
			continue

		colour = (0, 0, 0)

		if tile in (T.FLOOR, T.ROOM, T.PREFAB_FLOOR, T.PREFAB_DOOR, T.DOOR, T.FENCE):
			colour = tile.bg

		d.point((pos.x, generator.tilemap.height - pos.y - 1), colour) # +1 because images start at 1

	# for pos, obj in generator.objects.items():
	# 	if not isinstance(obj, str):
	# 		continue

	# 	pos = Vec(pos)
	# 	colour = (0, 0, 0)

	# 	colour = {
	# 		"datacore": T.DATACORE.bg,
	# 		"sentry_station": T.SENTRY.bg,
	# 		"ammo_station": T.ARMOURY.bg,
	# 		"health_station": T.REPAIR.bg,
	# 	}.get(obj, None)

	# 	if colour:
	# 		d.point((pos.x, generator.tilemap.height + pos.y - 1), colour)


	im = im.resize((128, 128), Image.Resampling.NEAREST)
	return im

def main(width, height, floors):
	# Generate the level
	logger = TcodLogger(width * floors, height)
	generator = HauberkGenerator(logger=logger)
	generator.generate(width, height, floors)
	generator.print()

	for z in range(generator.floors):
		im = tilemap_to_img(generator, -z)
		im.save(f"radar_floor_plan_{z}.png")

	generator.logger.pause()



if __name__ == "__main__":
	import argparse

	parser = argparse.ArgumentParser(
		description='Create an image from a generated 3D tilemap'
	)
	parser.add_argument('--width', type=int, default=21)
	parser.add_argument('--height', type=int, default=21)
	parser.add_argument('--floors', type=int, default=1)

	args = parser.parse_args()

	main(args.width, args.height, args.floors)
