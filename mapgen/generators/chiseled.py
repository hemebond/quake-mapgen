"""
Based on the Chiseled Random Path generation described in https://github.com/BorisTheBrave/chiseled-random-paths/blob/master/chisel.ts

python -m mapgen.generators.chiseled
"""

import os
from random import randint, randrange, choice, random
from dataclasses import dataclass
import tcod
from enum import Enum
from mapgen.tiles import Tiles as T
from mapgen.vec import Vec
from mapgen.loggers.tcod import TcodLogger
from mapgen.tilemap import TileMap



class CellState(Enum):
	Open = 1
	Blocked = 2
	Forced = 3



class ChiseledGenerator:
	def __init__(self, logger=None):
		self.logger = logger
		self.level = None

	def generate(self, width, height, show_progress=False):
		# cell_states, path = random_path(width, height, Vec(1, 1), Vec(width-2, height-2), 10)
		generator = random_path(width, height, Vec(0, 1), Vec(width-1, height-2), 10)

		tilemap = TileMap(width, height, None)

		def render_tiles():
			for x in range(width):
				for y in range(height):
					p = Vec(x, y)
					cs = cell_states[p]

					if cs == CellState.Forced:
						tile = T.DOOR
					elif p in path:
						tile = T.FLOOR
					elif cs == CellState.Blocked:
						tile = T.WALL
					else:
						tile = None

					tilemap[Vec(p.x, p.y, 0)] = tile
			return tilemap

		for cell_states, path in generator:
			if show_progress:
				tilemap = render_tiles()
				logger.print(tilemap)  # just print at the very end

		render_tiles()
		return tilemap



def random_path(width: int, height: int, start_point: Vec, end_point: Vec, wiggliness: int = 1):
	# Create an empty tilemap
	open_cells = []
	cell_states = {}

	# Used for drawing a border
	for x in range(width):
		for y in range(height):
			cell_states[Vec(x, y)] = CellState.Blocked
	for x in range(1, width - 1):
		for y in range(1, height - 1):
			open_cells.append(Vec(x, y))
			cell_states[Vec(x, y)] = CellState.Open

	try:
		open_cells.remove(start_point)
		open_cells.remove(end_point)
	except ValueError:
		# Ignore errors generated when the start or end points are not within the open cells
		pass

	cell_states[start_point] = CellState.Forced
	cell_states[end_point] = CellState.Forced

	def _find_path():
		return find_path(width, height, start_point, end_point, cell_states)

	witness = find_path(width, height, start_point, end_point, cell_states)
	while True:
		if len(open_cells) == 0:
			yield cell_states, witness
			return

		if wiggliness == 1:
			c = choice(open_cells)
		else:
			c = weighted_random_open_cell(open_cells, witness, wiggliness)

		cell_states[c] = CellState.Blocked
		open_cells.remove(c)

		yield cell_states, witness

		if c in witness:
			# new_path = _find_path()
			new_path = find_path(width, height, start_point, end_point, cell_states)
			if new_path is None:
				cell_states[c] = CellState.Forced
			else:
				witness = new_path

		yield cell_states, witness


def find_path(width: int, height: int, start_point: Vec, end_point: Vec, cell_states: dict):
	def get_neighbours(cell: Vec) -> list:
		x, y = cell.x, cell.y

		n = []
		if x > 0:
			n.append(Vec(x - 1, y))
		if x < width - 1:
			n.append(Vec(x + 1, y))
		if y > 0:
			n.append(Vec(x, y - 1))
		if y < height - 1:
			n.append(Vec(x, y + 1))

		return [c for c in n if cell_states.get(c) != CellState.Blocked]

	current_cells = [start_point]
	next_cells = []

	distances = {}
	distances[start_point] = 0

	current_dist = 0

	# This while loop is correct
	while True:
		for cell in current_cells:
			for neighbour in get_neighbours(cell):
				if distances.get(neighbour, None) is not None:
					continue
				next_cells.append(neighbour)
				distances[neighbour] = current_dist + 1

		current_cells = next_cells
		next_cells = []

		current_dist += 1
		if distances.get(end_point, None) is not None:
			break
		if len(current_cells) == 0:
			return None

	c = end_point
	path = [c]
	while current_dist > 0:
		ns = get_neighbours(c)
		ns_towards_start = [x for x in ns if distances.get(x) == current_dist - 1]
		c = choice(ns_towards_start)
		path.append(c)
		current_dist -= 1

	return path



def weighted_random_open_cell(open_cells, path, wiggliness):
	open_path_cells = [c for c in path if c in open_cells]
	path_weight = len(open_path_cells) * wiggliness
	non_path_weight = (len(open_cells) - len(open_path_cells)) * 1
	total_weight = path_weight + non_path_weight
	r = random() * total_weight
	if r <= path_weight:
		return choice(open_path_cells)
	else:
		non_path_cells = [c for c in open_cells if c not in path]
		return choice(non_path_cells)



if __name__ == '__main__':
	WIDTH = 30
	HEIGHT = 20
	logger = TcodLogger(WIDTH, HEIGHT)
	generator = ChiseledGenerator(logger=logger)

	level = generator.generate(WIDTH, HEIGHT, show_progress=True)
	logger.print(level)  # just print at the very end

	while True:
		for event in tcod.event.wait():
			if isinstance(event, tcod.event.Quit):
				raise SystemExit()
