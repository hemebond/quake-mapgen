'''
Based on the Hauberk dungeon generation described at https://journal.stuffwithstuff.com/2014/12/21/rooms-and-mazes/

python -m mapgen.generators.hauberk
'''

import os
from random import randint, randrange, choice, random
from dataclasses import dataclass
import tcod
from enum import Enum
from mapgen.tiles import Tiles as T
from mapgen.vec import Vec
from mapgen.loggers.tcod import TcodLogger
from mapgen.tilemap import TileMap

from prefabs.prototype.prefabs import TEMPLATES



def distance(p1, p2):
	x1, y1, z1 = p1
	x2, y2, z2 = p2
	distance = ((x1 - x2)**2 + (y1 - y2)**2)**0.5
	return distance



class HauberkGenerator:
	def __init__(self, logger=None):
		self.regions : TileMap = None
		self.rooms = []
		self.prefab_rooms = []
		self.logger = logger
		self.tilemap : TileMap = None
		self.floors = 1
		self.current_level = 0
		self.current_region = -1
		self.objects = {}  # collection of cells that contain an object
		self.waypoints = {}  # collection of cells that contain waypoints


	def print(self):
		if self.logger is None:
			return

		self.logger.print(self.tilemap, self.objects, self)


	def generate(self, width, height, floors=1):
		if width % 2 == 0 or height % 2 == 0:
			raise Exception("The level must be odd-sized.")

		self.floors = floors

		self.rooms = []

		# Pre-fill the level array
		self.tilemap = TileMap(width, height)

		self.regions = TileMap(width, height)

		for z in range(floors):
			self.outline_level(width, height, z=-z)
			self.print()

		self.add_entrance(width, height, 0)
		print()

		# Add a reactor to the first level
		self.add_reactor(width, height, 0)
		print()

		if floors > 1:
			for ez in range(0, floors - 1):	# don't want elevators on the last floor
				extra_elevators = int((width * height) / 300)  # adjust the number of elevators based on the size of the level
				for _ in range(randint(1, 1 + extra_elevators)):
					self.add_elevators(width, height, z=-ez)
					self.print()

		for z in range(floors):
			z *= -1

			self.add_prefabs(width, height, z)
			self.print()

			self.add_rooms(width, height, z)
			self.print()

			self.generate_maze(width, height, z)
			self.print()

			# pause()

			# self.merge_rooms(width, height)
			# self.print()

			self.connect_regions(width, height, z)
			self.print()

			# pause()

			self.remove_deadends()
			self.print()

			self.add_extra_doors(width, height, z)
			self.print()

			# pause()

			self.remove_deadend_walls(width, height, z)
			self.print()

			# pause()

			self.add_walls(width, height, z)
			self.print()

			self.add_radar(width, height, z)

		self.add_datacores()

		self.add_health_stations()

		self.add_ammo_stations()

		self.add_sentry_stations()

		self.add_monster_spawns()

		self.add_objects()

		self.add_waypoints()

		self.print()

		return self.tilemap



	def outline_level(self, width, height, z=0, tile=T.WALL):
		for x in range(width):
			self.tilemap[x, 0, z] = tile
			self.tilemap[x, height - 1, z] = tile

		for y in range(height):
			self.tilemap[0, y, z] = tile
			self.tilemap[width - 1, y, z] = tile



	def add_entrance(self, map_width, map_height, z):
		template = TEMPLATES['entrance']
		room = TileMap.from_strings(
			template,
			{
				'.': T.PREFAB_FLOOR,
				'+': T.PREFAB_DOOR,
			}
		)

		for i in range(30):  # 30 attempts
			x = randrange(0 - room.mins.x, map_width - room.maxs.x, 2)
			y = randrange(0 - room.mins.y, map_width - room.maxs.y, 2)

			pos = Vec(x, y, z)

			setattr(room, "prefab_name", "entrance")

			if self.can_place_room(room, pos):
				setattr(room, "x", x)
				setattr(room, "y", y)
				setattr(room, "z", z)
				self.add_prefab(room, pos)

				break




	def add_reactor(self, map_width, map_height, z):
		template = TEMPLATES['reactor']

		room = TileMap.from_strings(
			template,
			{
				'.': T.ROOM,
				'+': T.PREFAB_FLOOR,
			}
		)

		room = self.generate_room_square(5, 7)
		room.fill(T.PREFAB_FLOOR, Vec(1, 1, 0), Vec(3, 3, 0))

		for i in range(30):  # 30 attempts
			x = randrange(1, map_width - room.width, 2)
			y = randrange(1, map_height - room.height, 2)

			pos = Vec(x, y, z)

			setattr(room, "prefab_name", "reactor")

			if self.can_place_room(room, pos):
				self.start_region()
				self.add_room(room, pos)

				setattr(room, "x", x)
				setattr(room, "y", y)
				setattr(room, "z", z)
				self.add_prefab(room, pos)

				break



	def add_elevators(self, map_width, map_height, z):
		tmpl = TEMPLATES['elevator']
		room = TileMap.from_strings(
			tmpl,
			{
				'.': T.PREFAB_FLOOR,
				'+': T.PREFAB_DOOR,
				'#': T.WALL,
			}
		)

		angle = 0
		for i in range(randint(0, 3)):
			room = room.rotate90()
			angle += 90

		for i in range(30):  # 30 attempts
			x = randrange(0 - room.mins.x, map_width - room.maxs.x, 2)
			y = randrange(0 - room.mins.y, map_width - room.maxs.y, 2)

			pos = Vec(x, y, z)

			setattr(room, "prefab_name", "elevator")
			setattr(room, "angle", angle)

			if not self.can_place_room(room, pos):
				continue

			self.add_prefab(room, pos)

			break



	def add_prefabs(self, map_width, map_height, z):
		templates = {tpl_name: tpl for tpl_name, tpl in TEMPLATES.items() if tpl_name not in ("elevator", "entrance", "reactor")}

		# for name, template in list(templates.items()) + list(templates.items()):
		for j in range(len(templates)):
			name = choice(list(templates.keys()))
			template = templates[name]

			room = TileMap.from_strings(
				template,
				{
					'.': T.PREFAB_FLOOR,
					'-': T.FLOOR,
					'+': T.DOOR,
					'#': T.WALL,
				}
			)

			angle = 0
			for i in range(randint(0, 3)):
				room = room.rotate90()
				angle += 90

			# room = TileMap(width, height)
			setattr(room, "prefab_name", name)
			setattr(room, "angle", angle)

			MAX_ATTEMPTS = 30
			for i in range(MAX_ATTEMPTS):
				x = randrange(0 - room.mins.x, map_width - room.maxs.x, 2)
				y = randrange(0 - room.mins.y, map_width - room.maxs.y, 2)

				pos = Vec(x, y, z)

				if self.can_place_room(room, pos):
					self.start_region()
					self.add_prefab(room, pos)
					break


	def add_rooms(self, map_width, map_height, z):
		MAX_ATTEMPTS = 30

		for i in range(MAX_ATTEMPTS):
			room = self.generate_room()
			x = randrange(1, map_width - room.width, 2)
			y = randrange(1, map_height - room.height, 2)
			pos = Vec(x, y, z)

			if self.can_place_room(room, pos):
				self.start_region()
				self.add_room(room, pos)


	def generate_room(self):
		"""
		Select a room type to generate and return it
		Currently only generating square rooms
		"""

		if randint(0, 1):
			return self.generate_room_odd()

		return self.generate_room_square()


	def generate_room_square(self, minimum=3, maximum=7):
		# Random size from minimum to maximum, in steps of 2
		room_width = randrange(minimum, maximum + 1, 2)
		room_height = randrange(minimum, maximum + 1, 2)

		room = TileMap(room_width, room_height)
		room.fill(T.ROOM, Vec(0, 0, 0), Vec(room_width - 1, room_height - 1, 0))
		return room


	def generate_room_odd(self):
		room = TileMap()
		MAX_ROOM_WIDTH = 9
		MAX_ROOM_HEIGHT = 9

		# Generate random square spaces within the room
		for i in range(randint(1, 3)):
			space_width = randrange(3, MAX_ROOM_WIDTH, 2)  # 1, 3, 5
			space_height = randrange(3, MAX_ROOM_HEIGHT, 2)

			x = randrange(0, MAX_ROOM_WIDTH - space_width, 2)
			y = randrange(0, MAX_ROOM_HEIGHT - space_height, 2)

			room.fill(T.ROOM, Vec(x, y, 0), Vec(x + space_width - 1, y + space_height - 1, 0))

		return room


	def can_place_room(self, room, pos: Vec):
		for (rx, ry, rz), tile in room.items():

			debug = getattr(room, 'prefab_name', None) is not None

			world_pos = pos + Vec(rx, ry, rz)

			# if self.logger:
			# 	self.logger.console.print(
			# 		x=world_pos.x,
			# 		y=self.logger.console.height - 1 - world_pos.y,
			# 		string='*',
			# 		fg=(254, 128, 128),
			# 		bg=(0, 0, 0),
			# 	)
			# 	self.logger.context.present(self.logger.console)

			if tile is None:
				continue

			if world_pos.x < 0 or world_pos.x > self.tilemap.width:
				return False

			if world_pos.y < 0 or world_pos.y > self.tilemap.height:
				return False

			if tile in (T.DOOR, T.PREFAB_DOOR) and (world_pos.z > 0 or world_pos.z < -(self.floors - 1)):
				return False

			world_tile = self.tilemap.get(world_pos)

			if world_tile is None:
				continue

			if world_tile is not None:
				return False

			if world_tile in (T.PREFAB, T.PREFAB_DOOR, T.PREFAB_FLOOR):
				return False

			if world_tile != tile:
				return False

		return True


	def add_room(self, room, pos: Vec):
		"""
		Carve a room into the level and add it to the list of rooms

		:param      room:  The room
		:type       room:  TileMap
		:param      pos:   x,y,z coordinates of the top-left corner of the room
		:type       pos:   Vec
		"""
		for tile_pos, tile in room.items():
			if tile is not None:
				self.tilemap[pos + tile_pos] = room[tile_pos]

				if tile == T.ROOM:
					self.regions[pos + tile_pos] = self.current_region

		setattr(room, "x", pos.x)
		setattr(room, "y", pos.y)
		setattr(room, "z", pos.z)
		self.rooms.append(room)
		self.print()



	def add_prefab(self, room, world_origin):
		'''
		Carve a room into the level and add it to the list of rooms
		'''
		print(f"Add: {getattr(room, 'prefab_name')} {world_origin}")

		for (x, y, z), tile in room.items():
			if tile is None:
				continue

			tile_world_position = Vec(world_origin) + Vec(x, y, z)
			self.tilemap[tile_world_position] = tile

		setattr(room, "x", world_origin.x)
		setattr(room, "y", world_origin.y)
		setattr(room, "z", world_origin.z)
		self.prefab_rooms.append(room)
		self.print()




	def add_walls(self, map_width, map_height, z):
		'''
		Replace None tiles with WALL tiles if they have an adjacent non-None tile
		'''
		for x in range(map_width):
			for y in range(map_height):
				pos = Vec(x, y, z)

				if self.tilemap.get(pos) is not None:
					continue

				for s in pos.surrounding:
					tile = self.tilemap.get(s)

					if tile is None:
						continue

					if tile != T.WALL:
						self.tilemap[pos] = T.WALL



	def remove_deadend_walls(self, map_width, map_height, z):
		removed_wall = True

		while removed_wall:
			removed_wall = False

			for x in range(1, map_width - 1):
				for y in range(1, map_height - 1):
					pos = Vec(x, y, z)

					if self.tilemap.get(pos) is not None:
						continue

					adjacent_corridors = 0

					for apos in pos.adjacent:
						if self.tilemap.get(apos, None) in [T.FLOOR]:
							adjacent_corridors += 1

					if adjacent_corridors < 3:
						continue

					removed_wall = True

					self.tilemap[pos] = T.FLOOR
					self.print()



	def remove_deadends(self):
		"""
		Scan through all corridor tiles and if they have only one open tile adjacent, e.g.,
		door or corridor, remove them.
		"""
		done = False

		while not done:
			done = True

			for pos, tile in self.tilemap.items():
				if tile != T.FLOOR:
					continue

				pos = Vec(pos)

				adjacent_cells = [self.tilemap.get(apos) for apos in pos.adjacent]
				exits = [c for c in adjacent_cells if c not in (T.WALL, None)]

				if len(exits) != 1:
					continue

				if len(exits) == 1 and exits[0] != T.FLOOR:
					continue

				done = False

				self.tilemap[pos] = None
				self.regions[pos] = None
				self.print()



	def can_place_door(self, position):
		"""
		two walls on opposite sides
		two floors on the other opposite sides
		"""
		tiles = [self.tilemap.get(apos) for apos in Vec(position).adjacent]

		if tiles[0] in (T.WALL, None) and tiles[2] in (T.WALL, None) \
		and tiles[1] == T.FLOOR and tiles[3] == T.FLOOR:
			return True

		if tiles[0] == T.FLOOR and tiles[2] == T.FLOOR \
		and tiles[1] in (T.WALL, None) and tiles[3] in (T.WALL, None):
			return True

		return False


	def add_extra_doors(self, map_width, map_height, z):
		"""
		Adds extra doors and laser fences

		floor tile
		no doors in surrounding
		"""
		floors = [Vec(pos) for pos, tile in self.tilemap.items() if pos[2] == z and tile == T.FLOOR]
		floors = [pos for pos in floors if self.can_place_door(pos)]
		max_new_doors = int(len(floors) * 0.2)

		for _ in range(max_new_doors):
			while(floors):
				idx = randint(0, len(floors) - 1)
				pos = floors.pop(idx)

				surrounding_tiles = [self.tilemap.get(p) for p in Vec(pos).surrounding]
				if T.DOOR in surrounding_tiles:
					continue

				self.tilemap[pos] = T.DOOR
				break


	def connect_regions(self, map_width, map_height, z):
		"""
		Find tiles that can connect two ore more regions

		:param      map_width:   The map width
		:type       map_width:   int
		:param      map_height:  The map height
		:type       map_height:  int
		"""
		connector_regions = {}

		for x in range(1, map_width - 1):
			for y in range(1, map_height - 1):
				pos = Vec(x, y, z)

				tile = self.tilemap.get(pos)

				if tile is not None:  # only try to connect on empty or door tiles
					continue

				# Make sure the connector is connecting on opposite sides, not stuck in a corner
				adjacent_regions = [self.regions.get(adj_pos) for adj_pos in pos.adjacent]

				# Do not put connectors into corners
				if not (
					(adjacent_regions[0] is None and adjacent_regions[2] is None) or
				    (adjacent_regions[1] is None and adjacent_regions[3] is None)
				):
					continue

				regions = set()

				for region in adjacent_regions:
					if region is not None:
						regions.add(region)

				if len(regions) < 2:
					continue

				connector_regions[x, y, z] = regions

				if self.logger:
					self.logger.console.print(
						x=x + (z * map_width),
						y=self.logger.console.height - 1 - y,
						string='*',
						fg=(254, 128, 128),
						bg=(0, 0, 0),
					)

		# if self.logger:
		# 	self.logger.context.present(self.logger.console)
		# 	pause()

		connectors = list(connector_regions.keys())

		# Keep track of which regions have been merged. This maps an original
		# region index to the one it has been merged to.
		merged = {}
		open_regions = set()

		for i in range(self.current_region + 1):
			merged[i] = i
			open_regions.add(i)

		# Keeping regions until only one left
		while len(open_regions) > 1 and connectors:
			# print(connectors)
			connector = choice(connectors)

			self.add_junction(connector)

			regions = [merged[region] for region in connector_regions[connector]]
			dest = regions[0]
			sources = regions[1:]

			# print("regions: {}".format(regions))
			# print("sources: {}".format(sources))
			# print("self.current_region: {}".format(self.current_region))

			# print("merged: {}".format(merged))
			for i in range(self.current_region + 1):
				if merged[i] in sources:
					merged[i] = dest
			# print("merged: {}".format(merged))

			open_regions = [s for s in open_regions if s not in sources]

			# print("open_regions: {}".format(open_regions))

			connectors_to_remove = []
			for pos in connectors:
				# TODO: Walk along wall and remove all connectors on this wall

				# Check the distance between two points/connectors
				if distance(connector, pos) < 4:
					# Maybe add another junction for this region/room
					# if random() < 0:
					# 	self.add_junction(pos)

					connectors_to_remove.append(pos)
					continue

				regions = set([merged[region] for region in connector_regions[pos]])
				# print("regions: {}".format(regions))

				if len(regions) > 1:
					continue

				# This connecter isn't needed, but connect it occasionally
				# so that the dungeon isn't singly-connected.
				# EXTRA_CONNECTION_CHANCE = 0.5
				# if random() < EXTRA_CONNECTION_CHANCE:
				# 	self.add_junction(pos)
				# 	continue

				connectors_to_remove.append(pos)

			connectors = [pos for pos in connectors if pos not in connectors_to_remove]



	def add_junction(self, pos):
		adj_tiles = []

		for adj_pos in Vec(pos).adjacent:
			adj_tiles.append(self.tilemap.get(adj_pos))

		# if the junction is between two corridors, place a corridor instead of a door
		if adj_tiles.count(T.FLOOR) > 1:
			self.tilemap[pos] = T.FLOOR
			return

		self.tilemap[pos] = T.DOOR




	def generate_maze(self, map_width, map_height, z):
		for x in range(1, map_width, 2):
			for y in range(1, map_height, 2):
				pos = Vec(x, y, z)

				if self.tilemap.get(pos):
					continue

				self.grow_maze(pos)


	def grow_maze(self, pos: Vec):
		# print('grow_maze()')
		cells = []
		last_dir = None

		self.start_region()
		self.carve(pos, T.FLOOR)
		cells.append(pos)

		while len(cells) > 0:
			cell = cells[-1]
			empty_tiles = []

			for dir in [
				Vec(0, +1, 0),  # North
				Vec(+1, 0, 0),  # East
				Vec(0, -1, 0),  # South
				Vec(-1, 0, 0),  # West
			]:
				if self.can_carve(Vec(cell), dir):
					empty_tiles.append(dir)

			WINDING_CHANCE = 0.5
			if empty_tiles:
				if last_dir in empty_tiles and random() > WINDING_CHANCE:
					dir = last_dir
				else:
					dir = choice(empty_tiles)

				self.carve(cell + dir, T.FLOOR)
				self.carve(cell + (dir * 2), T.FLOOR)

				cells.append(cell + (dir * 2))
				last_dir = dir
			else:
				# No empty adjacent tiles
				# This will step backwards through the tunneler until a new branch can be created
				cells.pop()
				last_dir = None


	def can_carve(self, pos: Vec, dir: Vec):
		# x, y = pos
		# dx, dy = dir
		# ox, oy = Vec(pos) + Vec(vmul(dir, 2))
		ox, oy, oz = pos + (dir * 2)

		if 0 < ox < self.tilemap.width:
			if 0 < oy < self.tilemap.height:
				if self.tilemap.get((ox, oy, oz), None) is None:
					return True

		return False


	def carve(self, pos: Vec, tile_type=None):
		self.tilemap[pos] = T.ROOM if tile_type is None else tile_type
		self.regions[pos] = self.current_region

	def start_region(self):
		self.current_region += 1



	def add_datacores(self):
		floors = [org for org, tile in self.tilemap.items() if tile in (T.ROOM, T.FLOOR)]

		for datacore_id in range(5):  # we want this many datacores
			attempt = 0
			while True:
				attempt += 1

				if attempt > 30:
					raise RuntimeError("Could not place datacore")

				idx = choice(range(len(floors)))
				origin = Vec(floors.pop(idx))
				adjacent_walls = [pos for pos in origin.adjacent if self.tilemap[pos] == T.WALL]

				if len(adjacent_walls) < 1:
					continue

				self.objects[origin] = T.DATACORE

				break


	def add_stations(self, station_type, station_num):
		floors = [org for org, tile in self.tilemap.items() if tile in (T.ROOM, T.FLOOR)]
		empty_floors = [org for org in floors if org not in self.objects]

		MAX_ATTEMPTS = 30  # The number of attempts to place each station

		for _ in range(station_num):  # we want this many stations
			attempt = 0
			while True:
				attempt += 1

				if attempt > MAX_ATTEMPTS:
					raise RuntimeError("Could not place ", station_type)

				idx = choice(range(len(empty_floors)))
				origin = Vec(empty_floors.pop(idx))
				adjacent_walls = [pos for pos in origin.adjacent if self.tilemap[pos] == T.WALL]

				if len(adjacent_walls) < 1:
					continue

				self.objects[origin] = station_type

				break


	def add_health_stations(self):
		self.add_stations(T.REPAIR, 10 * self.floors)


	def add_sentry_stations(self):
		self.add_stations(T.SENTRY, 10 * self.floors)


	def add_ammo_stations(self):
		self.add_stations(T.ARMOURY, 10 * self.floors)

	def add_radar(self, map_width, map_height, z):
		# TODO: change to iterate through rooms and place the security that way
		# for room in self.rooms:
		# 	...
		# return

		floors = [pos for pos, tile in self.tilemap.items() if pos[2] == z and tile == T.ROOM]

		# FIXME: Why is this possible?
		if not len(floors):
			return

		for _ in range(10):
			list_idx = choice(range(len(floors)))
			pos = Vec(floors.pop(list_idx))

			if pos not in self.objects:
				surrounding_tiles = [self.tilemap.get(p) for p in pos.surrounding]
				surrounding_floor_tiles = [tile for tile in surrounding_tiles if tile == T.ROOM]

				if len(surrounding_floor_tiles) < 8:
					continue

				print("placing radar at ", pos);
				self.objects[pos] = T.SECURITY
				break
		# else:
		# 	raise RuntimeError("Could not place radar")


	def add_monster_spawns(self, num_spawns=5):
		floors = [origin for origin, tile in self.tilemap.items() if tile in (T.FLOOR, T.ROOM)]

		for spawn_id in range(num_spawns):
			for i in range(10):
				list_idx = choice(range(len(floors)))
				origin = Vec(floors.pop(list_idx))

				if origin not in self.objects:
					self.objects[origin] = T.BREACH
					break



	def add_objects(self):
		floors = [org for org, tile in self.tilemap.items() if tile in (T.ROOM, T.FLOOR)]

		MAX_ATTEMPTS = 100
		objects_placed = 0

		for i in range(MAX_ATTEMPTS):
			floor_position = Vec(choice(floors))

			if floor_position in self.objects:
				continue

			# Do not place objects in front of doors
			adjacent_tiles = [self.tilemap.get(pos) for pos in floor_position.adjacent]
			if T.DOOR in adjacent_tiles:
				continue

			if len([pos for pos in floor_position.surrounding if pos in floors and pos not in self.objects]) < 5:
				continue

			# if self.logger:
			# 	self.logger.console.print(
			# 		x=floor_position.x + (-floor_position.z * self.tilemap.width),
			# 		y=self.logger.console.height - 1 - floor_position.y,
			# 		string='*',
			# 		fg=(128, 254, 128),
			# 		bg=(0, 0, 0),
			# 	)
			# 	self.logger.context.present(self.logger.console)

			self.objects[floor_position] = 'boxes'

			objects_placed += 1
			if objects_placed > 30:
				break

	def add_waypoints(self):
		...



if __name__ == '__main__':
	import argparse

	parser = argparse.ArgumentParser(
		description='Create a Quake map file from a generated 3D tilemap'
	)
	parser.add_argument('--width', type=int, default=17)
	parser.add_argument('--height', type=int, default=17)
	parser.add_argument('--levels', type=int, default=1)

	args = parser.parse_args()

	print(args)
	logger = TcodLogger(args.width * args.levels, args.height)
	generator = HauberkGenerator(logger=logger)
	level = generator.generate(args.width, args.height, args.levels)

	while True:
		for event in tcod.event.wait():
			match event:
				case tcod.event.Quit():
					raise SystemExit()
