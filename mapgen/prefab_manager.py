from pathlib import Path
from mapgen.qmap import QuakeMap
from copy import deepcopy
from random import choice
import fnmatch



# Parses a directory for prefabs, i.e., .txt file and .map files
# Automatically creates rotated versions
# Can return a prefab based on a tag
# can return a prefab based on size



class PrefabManager:
	def __init__(self, prefab_root_paths: Path | list):
		"""
		Find and parse all prefab .map files under a directory
		"""

		if isinstance(prefab_root_paths, Path):
			prefab_root_paths = [prefab_root_paths,]

		self.prefabs = []

		for root_path in prefab_root_paths:
			prefabs = {}

			for prefab_path in root_path.glob("**/*.map"):
				if 'autosave' in str(prefab_path):
					continue

				prefab_name = str(prefab_path.relative_to(root_path).with_suffix(""))  # remove the root dir and extension

				with open(prefab_path) as f:
					prefabs[prefab_name] = QuakeMap.load(f)

				# TODO: Read _tags property from Worldspawn and add as tags on the prefab

			self.prefabs.append(prefabs)


	def get(self, name: str, value=None):
		"""
		Returns a copy of a prefab QuakeMap

		Args:
		    name (str): Description
		    angle (int, optional): Description
		"""

		if name[-1] == "/":
			name = name + "*"

		for src in self.prefabs:
			p_names = fnmatch.filter(src.keys(), name)

			if p_names:
				p_name = choice(p_names)
				return deepcopy(src[p_name])

		return value


	def get_names(self, name: str, value=None) -> list:
		if name[-1] == "/":
			name = name + "*"

		for src in self.prefabs:
			p_names = fnmatch.filter(src.keys(), name)

			if p_names:
				return sorted(p_names)

		return value
