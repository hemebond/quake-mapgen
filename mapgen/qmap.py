"""
python -m doctest -v mapgen/qmap.py
"""

#
# Notes on Layers
#
# Layers are listed by id, not by sort index
# Entites are listed immediately after their layer
# Everything is assumed to be "Default Layer" if it has no layer specified
# First non-default layer has id 1
#



import re
import os
from pathlib import Path
from dataclasses import dataclass
from copy import copy, deepcopy
import numpy as np
import math
from types import SimpleNamespace
import logging
import typing
from textwrap import dedent




class VectorProperty(list):
	def __init__(self, value: [str, list]):
		self.value = value

	def __str__(self):
		return str(self.value)


def str_from_num(num):
	"""
	Takes a number and returns a string that has unnecessary trailing decimal removed

	:param      num:                  The number
	:type       num:                  An integer or float to make into a string

	:returns:   A string
	:rtype:     str
	"""
	if num == 0:
		return '0'

	return str(num).rstrip('0').rstrip('.')



class PlaneTexture:
	def __init__(self):
		self.name = None
		self.points = []
		self.offsets = []
		self.rotation = 0
		self.scale = SimpleNamespace(
			x=0,
			y=0,
		)


	def __copy__(self) -> "PlaneTexture":
		p = PlaneTexture()
		p.name = self.name
		p.points = self.points
		p.offsets = self.offsets
		p.scale = self.scale
		return p


	def __deepcopy__(self, memo) -> "PlaneTexture":
		p = PlaneTexture()
		p.name = deepcopy(self.name)
		p.points = [deepcopy(p) for p in self.points]
		p.offsets = deepcopy(self.offsets)
		p.scale = deepcopy(self.scale)
		return p


	def transform(self, transform) -> "PlaneTexture":
		# Extract the rotation matrix from the transform matrix
		rotation = np.identity(4)
		rotation[:3, :3] = transform[:3, :3]

		p = PlaneTexture()

		p.name = self.name
		p.rotation = self.rotation
		p.scale.x = self.scale.x
		p.scale.y = self.scale.y

		p.points.append(
			transform_point(self.points[0], rotation)
		)
		p.points.append(
			transform_point(self.points[1], rotation)
		)

		# Use the translation matrix to adjust the offsets
		# but multiply by 1/scale to neutralise the scaling
		# Also Quake y+ goes left
		p.offsets.append(
			self.offsets[0] - np.dot(p.points[0], transform[:3, 3] * (1 / p.scale.x))
		)
		p.offsets.append(
			self.offsets[1] - np.dot(p.points[1], transform[:3, 3] * (1 / p.scale.y))
		)

		return p


	def __str__(self) -> str:
		# texture.name [ n n n offset1 ] [ n n n offset2 ] rotation scalex scaley
		out = '%s [ %s %s %s %s ] [ %s %s %s %s ] %s %s %s' % (
			self.name,
			*[str_from_num(i) for i in self.points[0]],
			str_from_num(self.offsets[0]),
			*[str_from_num(i) for i in self.points[1]],
			str_from_num(self.offsets[1]),
			self.rotation,  # self.rotation,
			str_from_num(self.scale.x),
			str_from_num(self.scale.y),
		)
		return out



class Plane:
	def __init__(self):
		self.points = []
		self.texture = PlaneTexture()


	def __str__(self):
		# ( n n n ) ( n n n ) ( n n n ) texture.name [ n n n offset1 ] [ n n n offset2 ] rotation scalex scaley
		out = '( %s %s %s ) ( %s %s %s ) ( %s %s %s ) %s' % (
			*[str_from_num(i) for i in self.points[0]],
			*[str_from_num(i) for i in self.points[1]],
			*[str_from_num(i) for i in self.points[2]],
			str(self.texture),
		)
		return out


	def transform(self, transform) -> "Plane":
		p = Plane()

		for i in range(3):
			p.points.append(
				transform_point(self.points[i], transform)
			)

		p.texture = self.texture.transform(transform)

		return p


	def __copy__(self) -> "Plane":
		p = Plane()
		p.points = self.points
		p.texture = self.texture
		return p


	def __deepcopy__(self, memo) -> "Plane":
		p = Plane()
		p.points = [deepcopy(point) for point in self.points]
		p.texture = deepcopy(self.texture)
		return p



class Brush:
	def __init__(self):
		self.planes = []


	def __str__(self):
		brush_str = ""

		if hasattr(self, "_mg_id"):
			brush_str += "// brush " + getattr(self, "_mg_id") + "\n"

		brush_str += '\n'.join([
			'{',
			*[str(plane) for plane in self.planes],
			'}'
		])

		return brush_str


	def __repr__(self):
		return {
			"planes": self.planes
		}


	def transform(self, transform):
		b = Brush()
		b.planes = []

		for copy_plane in self.planes:
			b.planes.append(
				copy_plane.transform(transform)
			)

		return b

	def __copy__(self):
		b = Brush()
		b.planes = []

		for plane in self.planes:
			b.planes.append(plane)

		return b


	def __deepcopy__(self, memo):
		b = Brush()
		b.planes = []

		for plane in self.planes:
			b.planes.append(deepcopy(plane))

		return b



class Entity:
	def __init__(self):
		self.brushes = []


	def __str__(self):
		entity_str = ""

		if hasattr(self, "_mg_id"):
			entity_str += "// entity " + getattr(self, "_mg_id") + "\n"

		entity_str += '\n'.join([
			'{',
			*[f'"{k}" "{getattr(self, k)}"' for k in self.__dict__.keys() if not k.startswith("_mg_") and k != "brushes"],
			*[str(brush) for brush in self.brushes],
			'}',
		])

		return entity_str


	def __copy__(self):
		e = Entity()

		# Copy properties
		for key in self.__dict__.keys():
			if key != 'brushes':
				setattr(e, key, getattr(self, key))

		# Copy brushes
		for brush in self.brushes:
			e.brushes.append(brush)

		return e


	def __deepcopy__(self, memo):
		e = Entity()

		# Copy properties
		for key in self.__dict__.keys():
			if key != 'brushes':
				setattr(e, key, getattr(self, key))

		# Copy brushes
		for brush in self.brushes:
			e.brushes.append(deepcopy(brush))

		return e


	def transform(self, x, y, z, angle):
		translation = translation_matrix(x, y, z)
		rotation = rotation_matrix(angle)
		transform = np.dot(translation, rotation)

		e = Entity()

		for key in self.__dict__.keys():
			if key != 'brushes':
				setattr(e, key, getattr(self, key))

		if hasattr(e, 'origin'):
			origin = [float(i) for i in self.origin.split()]
			origin = transform_point(origin, transform)
			setattr(e, 'origin', ' '.join([str(i) for i in origin]))

		if hasattr(e, 'angle'):
			angle_property = int(getattr(e, 'angle'))

			if angle_property != -1 and angle_property != -2:
				setattr(e, 'angle', normalise_angle(angle_property + angle))

		angles = getattr(e, "angles", None)
		if angles:
			f_angles = [float(v) for v in angles.split()]
			f_angles[1] = normalise_angle(f_angles[1] + angle)
			setattr(e, "angles", " ".join([str(a) for a in f_angles]))

		avelocity = getattr(e, "avelocity", None)
		if avelocity:
			f_avelocity = [float(v) for v in avelocity.split()]  # x z y

		# Movedir is used in Doom 3
		movedir = getattr(e, "movedir", None)
		if movedir:
			movedir = normalise_angle(int(movedir) + angle)
			setattr(e, "movedir", str(movedir))

		for copy_brush in self.brushes:
			e.brushes.append(
				copy_brush.transform(transform)
			)

		return e



class QuakeMap(list):
	def __init__(self):
		worldspawn = Entity()
		setattr(worldspawn, 'classname', 'worldspawn')
		setattr(worldspawn, 'wad', '')
		super().__init__([worldspawn])

		self.layers = {}


	def add_wad(self, wad_path) -> "QuakeMap":
		"""
		Add a single wad path to the Worldspawn wad property
		"""

		wad_list = self[0].wad.split(';')
		wad_set = set(wad_list)
		wad_set.add(wad_path)
		self[0].wad = ";".join(wad_set)

		return self


	def __copy__(self):
		m = QuakeMap()
		m.clear()

		for entity in self:
			m.append(entity)

		return m


	def __deepcopy__(self, memo):
		m = QuakeMap()
		m.clear()

		for entity in self:
			m.append(deepcopy(entity))

		return m


	@classmethod
	def load(cls, f: typing.TextIO) -> "QuakeMap":
		"""
		Parse a .map file and return a QuakeMap object
		"""
		return parse_quakemap(f)


	def __str__(self):
		"""
		Return a string
		"""
		header = dedent("""\
			// Game: Quake
			// Format: Valve
		""")
		return header + '\n'.join([str(e) for e in self])


	def transform(self, x, y, z, angle):
		m = QuakeMap()
		m.merge(self, x, y, z, angle)
		return m


	def merge(self, other: 'QuakeMap', x, y, z, angle) -> 'QuakeMap':
		"""
		Updates this QuakeMap entity with entities and properties from other
		"""

		translation = translation_matrix(x, y, z)
		rotation = rotation_matrix(angle)
		transform = np.dot(translation, rotation)

		# TODO: Should be able to use proper numpy array functions to transform all points at once
		for entity in other:
			if entity.classname == 'worldspawn':
				e = self[0]  # worldspawn

				for key in entity.__dict__.keys():
					value = getattr(entity, key)

					if key != 'brushes':
						if key == 'wad':
							e.wad = join_wads(e.wad, value)
						else:
							setattr(e, key, value)

				for brush in entity.brushes:
					e.brushes.append(
						brush.transform(transform)
					)
			else:
				self.append(
					entity.transform(x, y, z, angle)
				)

		return self


	def get_layer_id(self, name: str) -> int:
		"""
		Takes a layer name and returns its ID

		:param      name:  The layer name
		:type       name:  str
		"""
		raise NotImplementedError()


	def delete_layer(self, name: str, id: int) -> None:
		"""
		Deletes a layer and all entities and brushes belonging to that layer

		:param      name:  The name
		:type       name:  str
		:param      id:    The identifier
		:type       id:    int

		:returns:   { description_of_the_return_value }
		:rtype:     None
		"""
		raise NotImplementedError()



# -----------------------------------------------------------------------------
#
# Parsing functions
#
# -----------------------------------------------------------------------------

def nextline(s):
	"""returns the next line of file s
	"""
	line = s.readline()
	if line == "":  # End of the file is an empty string, no \n
		return None

	return line.strip()



def parse_plane(s: typing.TextIO) -> Plane:
	p = Plane()

	tokens = s.split()

	x1, y1, z1 = [float(i) for i in tokens[1:4]]
	x2, y2, z2 = [float(i) for i in tokens[6:9]]
	x3, y3, z3 = [float(i) for i in tokens[11:14]]

	p.points = [
		[x1, y1, z1],
		[x2, y2, z2],
		[x3, y3, z3],
	]

	tx1, ty1, tz1, to1 = [float(i) for i in tokens[17:21]]
	tx2, ty2, tz2, to2 = [float(i) for i in tokens[23:27]]
	rotation, sx, sy = tokens[28:31]

	p.texture.name = tokens[15]
	p.texture.points = [
		[tx1, ty1, tz1],
		[tx2, ty2, tz2],
	]
	p.texture.offsets = [to1, to2]
	p.texture.rotation = rotation
	p.texture.scale = SimpleNamespace(
		x=float(sx),
		y=float(sy),
	)

	if p.texture.name is None:
		raise RuntimeError

	return p



def parse_brush(s: typing.TextIO) -> Brush:
	b = Brush()

	line = nextline(s)
	while line and not line.startswith("}"):
		b.planes.append(
			parse_plane(line)
		)
		line = nextline(s)

	return b



def parse_property(line: str) -> tuple:
	"""Takes a property string and returns a name value tuple

	>>> parse_property('"keyname" "string \"value\""')
	('keyname', 'string "value"')
	"""
	m = re.match(r'^"(.+)" "(.+)"$', line)
	if m:
		return (m[1], m[2])

	raise RuntimeError(f"Failed to parse property from line: {line}")



def parse_entity(s: typing.TextIO) -> Entity:
	e = Entity()

	brush_id = None  # store brush ID comments

	line = nextline(s)
	while not line.startswith("}"):
		if line.startswith('"'):
			key, value = parse_property(line)
			setattr(e, key, value)

		if line.startswith("// brush "):
			brush_id = re.match(r'^\/\/ brush (\d+)$', line).group(1)

		if line.startswith("{"):
			b = parse_brush(s)

			if (brush_id):
				setattr(b, "_mg_id", brush_id)
				brush_id = None

			e.brushes.append(b)

		line = nextline(s)

	return e



def parse_quakemap(s: typing.TextIO) -> QuakeMap:
	m = QuakeMap()
	del m[0]

	entity_id = None  # Store the // Entity 111 labels

	line = nextline(s)
	while line is not None:
		if line.startswith("// entity "):
			entity_id = re.match(r'^\/\/ entity (\d+)$', line).group(1)

		elif line.startswith("{"):
			e = parse_entity(s)

			# Add layer to map if this is a func_group layer declaration
			if getattr(e, "_tb_type", "") == "_tb_layer":
				layer_id = getattr(e, "_tb_id")

				m.layers[layer_id] = {
					"id": layer_id,
					"name": getattr(e, "_tb_name"),
					"sort_index": getattr(e, "_tb_layer_sort_index")
				}

			if entity_id:
				setattr(e, "_mg_id", entity_id)
				entity_id = None  # reset entity label after parsing each entity

			m.append(e)

		line = nextline(s)

	return m



# -----------------------------------------------------------------------------
#
# Transform functions
#
# -----------------------------------------------------------------------------

def normalise_angle(angle: float) -> float:
	"""
	Make the angle a positive value between 0 and 360
	"""
	return angle % 360



def rotation_matrix(degrees):
	"""
	Return a rotation matrix
	"""
	angle = math.radians(degrees)
	return np.array((
		(math.cos(angle), -math.sin(angle), 0.0, 0.0),
		(math.sin(angle), math.cos(angle), 0.0, 0.0),
		(0.0, 0.0, 1.0, 0.0),
		(0.0, 0.0, 0.0, 1.0)
	))



def translation_matrix(x, y, z):
	"""
	Return a translation matrix
	"""
	m = np.identity(4)
	m[:3, 3] = x, y, z
	return m



def transform_point(point, mat):
	"""
	Apply transform matrix mat to point
	"""
	return np.dot(mat, (*point, 1))[:3]



def join_wads(a: str, b: str) -> str:
	"""
	Merges two strings of wads, removing duplicates

	>>> join_wads('a.wad;b.wad;c.wad', 'c.wad;d.wad')
	'a.wad;b.wad;c.wad;d.wad'
	"""
	a_wads = wad_list(a)
	b_wads = wad_list(b)
	return ';'.join(sorted(set([wad for wad in a_wads + b_wads if wad])))



def wad_list(wad_string: str) -> list:
	"""
	Takes a wad string and returns a list of wad paths

	>>> wad_list("/a;/b")
	['/a', '/b']
	"""
	return wad_string.split(';')



def make_wads_absolute(relative_to: [str, Path], wad_string: str) -> str:
	"""
	Takes a wad string and makes each wad path absolute

	>>> make_wads_absolute("/foo/bar", "a/b;/c;../d")
	'/foo/bar/a/b;/c;/foo/d'
	"""
	return ';'.join(
		[
			str(Path(relative_to, Path(w)).resolve()) for
			w in wad_list(wad_string)
		]
	)



# -----------------------------------------------------------------------------
#
# Main
#
# -----------------------------------------------------------------------------

if __name__ == '__main__':
	#
	# Tests the functions and classes by constructing a level from prefabs
	#

	logging.basicConfig(level=logging.DEBUG)

	prefabs = {}
	prefab_path_list = [
		p for
		p in list(Path('prefabs/textured').glob('*.map'))
		if 'autosave' not in str(p)
	]

	# Load all prefabs from disk into a dict
	for p in prefab_path_list:
		with p.open() as f:
			prefab = parse_quakemap(f)

		# Change the wads to be absolute paths
		prefab[0].wad = make_wads_absolute(relative_to=p, wad_string=prefab[0].wad)

		# Add the prefab to the cache
		prefabs[p.stem] = prefab

	output = QuakeMap()

	# The add prefabs to the new map with offsets and rotation
	for name, x, y, z, angle in [
		('b1', 384, 384, 0, 180),
		('b1', 0, 768, 0, 180),
		('b1', -384, 768, 0, 270),
		('b1', -384, 384, 0, 0),
		('b1', 384, 0, 0, 90),
		('b1', 0, -384, 0, 90),
		('b1', -384, -384, 0, 0),
		('b1', -384, 0, 0, 270),
		('t1', 0, 0, 0, 0),
		('t1', 0, 384, 0, 180),
	]:
		output = output.merge(prefabs[name], x, y, z, angle)

	info_player_start = Entity()
	setattr(info_player_start, "classname", "info_player_start")
	setattr(info_player_start, "origin", "0 0 32")
	output.append(info_player_start)

	outpath = Path('test_qmap.map')
	with open(outpath, 'w') as f:
		f.write(str(output))
