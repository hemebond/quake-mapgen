# From
#( 96 0 0 ) ( 144 0 48 ) ( 144 -48 0 ) text_amper [ 0.7071067692898135 -0.7071067930832805 -2.379346697445328e-08 -3.8822498 ] [ -0.4082483110696097 -0.4082482698581158 -0.8164965809277255 7.191838 ] 60 1 1

# To
#( 0 96 0 ) ( 0 144 48 ) ( 48 144 0 ) text_amper [ 0.7071067930832805 0.7071067692898135 -2.379346697445328e-08 -3.882248 ] [ 0.4082482698581158 -0.4082483110696097 -0.8164965809277255 7.1918373 ] 0 1 1

import unittest
import numpy as np
import qmap


TEXTURE_SIZE = {
	'text_amper': (32, 32),
	'jf2con15': (128, 128)
}


# def rotation_matrix(degrees):
# 	angle = math.radians(degrees)
# 	return np.array((
# 		(math.cos(angle), -math.sin(angle), 0.0, 0.0),
# 		(math.sin(angle), math.cos(angle), 0.0, 0.0),
# 		(0.0, 0.0, 1.0, 0.0),
# 		(0.0, 0.0, 0.0, 1.0)
# 	))

# def translation_matrix(x_offset, y_offset, z_offset=0.0):
# 	return np.array((
# 		(1.0, 0.0, 0.0, x_offset),
# 		(0.0, 1.0, 0.0, y_offset),
# 		(0.0, 0.0, 1.0, z_offset),
# 		(0.0, 0.0, 0.0, 1.0)
# 	))

# # Rotation matrix
# r = R.from_matrix([
# 	[0,-1,0],
# 	[1,0,0],
# 	[0,0,1],
# ])


# def parse_line(line):
# 	tokens = line.split()
# 	x1, y1, z1 = [float(i) for i in tokens[1:4]]
# 	x2, y2, z2 = [float(i) for i in tokens[6:9]]
# 	x3, y3, z3 = [float(i) for i in tokens[11:14]]

# 	tx1, ty1, tz1, to1 = [float(i) for i in tokens[17:21]]
# 	tx2, ty2, tz2, to2 = [float(i) for i in tokens[23:27]]

# 	texture_name = tokens[15]
# 	rotation, sx, sy = tokens[-3:]

# 	sx = float(sx)
# 	sy = float(sy)

# 	return {
# 		'points': [
# 			[round(x1), round(y1), round(z1)],
# 			[round(x2), round(y2), round(z2)],
# 			[round(x3), round(y3), round(z3)],
# 		],
# 		'tpoints': [
# 			[round(tx1), round(ty1), round(tz1)],
# 			[round(tx2), round(ty2), round(tz2)],
# 		],
# 		'toffs1': to1,
# 		'toffs2': to2,
# 		'texture': texture_name,
# 		'scalex': round(sx, 4),
# 		'scaley': round(sy, 4),
# 	}

# def rotate_by(deg, vec):
# 	rotmat90 = np.array([
# 		[0,-1,0],
# 		[1,0,0],
# 		[0,0,1],
# 	])
# 	while deg > 0:
# 		vec = np.dot(rotmat90, vec)
# 		deg -= 90
# 	return vec

# def translate_by(vec, x, y, z = 0):
# 	print((*vec, 1))
# 	movmat = np.array((
# 		(1.0, 0.0, 0.0, 0),
# 		(0.0, 1.0, 0.0, 0),
# 		(0.0, 0.0, 1.0, 0),
# 		(x, y, z, 1.0)
# 	))
# 	print(movmat)
# 	return np.matmul((*vec, 1), movmat)[:3]



# def rotate_plane(angle, plane):
# 	for i in range(3):
# 		plane.points[i] = [p for p in rotate_by(angle, plane.points[i])]

# 	plane.texture.points[0] = [p for p in rotate_by(angle, plane.texture.points[0])]
# 	plane.texture.points[1] = [p for p in rotate_by(angle, plane.texture.points[1])]

# 	return plane


# def translate_plane(dist, plane):
# 	for i in range(3):
# 		plane.points[i] = [round(p) for p in translate_by(plane.points[i], dist, 0)]

# 	return plane



TEXTURE_SIZE = 128

class TestTransformPlane(unittest.TestCase):
	# def rotate(self, angle, before, after):
	# 	rotated = rotate_plane(angle, Plane(before))
	# 	target = Plane(after)

	# 	self.assertEqual(rotated.points, target.points)
	# 	self.assertEqual(rotated.texture.points[0], target.texture.points[0])
	# 	self.assertEqual(rotated.texture.points[1], target.texture.points[1])

	# def translate(self, dist, before, after):
	# 	translated = translate_plane(dist, Plane(before))
	# 	target = Plane(after)

	# 	self.assertEqual(translated.points, target.points)
	# 	self.assertEqual(translated.texture.points[0], target.texture.points[0])
	# 	self.assertEqual(translated.texture.points[1], target.texture.points[1])

	def transform(self, x, y, z, angle, before, after):
		before = qmap.Plane.from_string(before)
		after = qmap.Plane.from_string(after)

		transformed = qmap.transform_plane(before, x, y, z, angle)

		# self.assertListEqual(list(transformed.points), list(after.points))
		np.testing.assert_array_equal(np.around(transformed.points), np.around(after.points))
		np.testing.assert_array_equal(np.around(transformed.texture.points), np.around(after.texture.points))

		self.assertEqual(transformed.texture.offsets, after.texture.offsets)


	# def test_rotations(self):
	# 	self.rotate(90, '( 128 128 0 ) ( 128 0 0 ) ( 0 0 0 ) jf2con15 [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1', '( -128 128 0 ) ( 0 128 0 ) ( 0 0 0 ) jf2con15 [ 6.123233995736766e-17 1 0 0 ] [ 1 -6.123233995736766e-17 0 0 ] 270 1 1')
	# 	self.rotate(90, '( 128 0 128 ) ( 128 0 0 ) ( 0 0 0 ) jf2con15 [ 1 0 0 0 ] [ 0 -6.123233995736766e-17 -1 0 ] 0 1 1', '( 0 128 128 ) ( 0 128 0 ) ( 0 0 0 ) jf2con15 [ 2.83276944882399e-16 1 0 0 ] [ 6.123233995736766e-17 -1.7345710191123555e-32 -1 0 ] 0 1 1')
	# 	self.rotate(90, '( 128 64 110.85125168440806 ) ( 128 0 0 ) ( 0 0 0 ) jf2con15 [ 1 -4.0441131638333747e-16 2.3348698237725164e-16 0 ] [ -2.859390856108749e-32 -0.5000000000000012 -0.866025403784438 0 ] 0 1 1', '( -64 128 110.85125168440806 ) ( 0 128 0 ) ( 0 0 0 ) jf2con15 [ 4.656436563407051e-16 1 2.3348698237725164e-16 0 ] [ 0.5000000000000012 -3.0616169978683935e-17 -0.866025403784438 0 ] 306.8699 1 1')
	# 	self.rotate(90, '( 81.14874831559183 64 64 ) ( 192 64 128 ) ( 247.42562584220406 0 32 ) jf2con15 [ 0.8660254037844384 0 0.5000000000000004 25.72313 ] [ 0.4330127018922196 -0.5000000000000004 -0.7499999999999996 -83.13844 ] 333.87137 1 1', '( -64 81.14874831559183 64 ) ( -64 192 128 ) ( 0 247.42562584220406 32 ) jf2con15 [ 5.302876193624533e-17 0.8660254037844384 0.5000000000000004 25.72313 ] [ 0.5000000000000004 0.4330127018922195 -0.7499999999999996 -83.13844 ] 287.04492 1 1')

	# def test_translation(self):
	# 	self.translate(128, '( 0 0 128 ) ( 128 0 128 ) ( 128 0 0 ) jf2con15 [ 1 0 -0 0 ] [ -0 0 -1 0 ] 0 1 1', '( 128 0 128 ) ( 256 0 128 ) ( 256 0 0 ) jf2con15 [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1')
	# 	self.translate(192, '( 0 0 128 ) ( 128 0 128 ) ( 128 0 0 ) jf2con15 [ 1 0 -0 0 ] [ -0 0 -1 0 ] 0 1 1', '( 192 0 128 ) ( 320 0 128 ) ( 320 0 0 ) jf2con15 [ 1 0 0 -64 ] [ 0 0 -1 0 ] 0 1 1')

	def test_transform(self):
		self.transform(0, 0, 0, 90, '( 128 128 0 ) ( 128 0 0 ) ( 0 0 0 ) jf2con15 [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1', '( -128 128 0 ) ( 0 128 0 ) ( 0 0 0 ) jf2con15 [ 6.123233995736766e-17 1 0 0 ] [ 1 -6.123233995736766e-17 0 0 ] 270 1 1')
		self.transform(0, 0, 0, 90, '( 128 0 128 ) ( 128 0 0 ) ( 0 0 0 ) jf2con15 [ 1 0 0 0 ] [ 0 -6.123233995736766e-17 -1 0 ] 0 1 1', '( 0 128 128 ) ( 0 128 0 ) ( 0 0 0 ) jf2con15 [ 2.83276944882399e-16 1 0 0 ] [ 6.123233995736766e-17 -1.7345710191123555e-32 -1 0 ] 0 1 1')
		self.transform(0, 0, 0, 90, '( 128 64 110.85125168440806 ) ( 128 0 0 ) ( 0 0 0 ) jf2con15 [ 1 -4.0441131638333747e-16 2.3348698237725164e-16 0 ] [ -2.859390856108749e-32 -0.5000000000000012 -0.866025403784438 0 ] 0 1 1', '( -64 128 110.85125168440806 ) ( 0 128 0 ) ( 0 0 0 ) jf2con15 [ 4.656436563407051e-16 1 2.3348698237725164e-16 0 ] [ 0.5000000000000012 -3.0616169978683935e-17 -0.866025403784438 0 ] 306.8699 1 1')
		self.transform(0, 0, 0, 90, '( 81.14874831559183 64 64 ) ( 192 64 128 ) ( 247.42562584220406 0 32 ) jf2con15 [ 0.8660254037844384 0 0.5000000000000004 25.72313 ] [ 0.4330127018922196 -0.5000000000000004 -0.7499999999999996 -83.13844 ] 333.87137 1 1', '( -64 81.14874831559183 64 ) ( -64 192 128 ) ( 0 247.42562584220406 32 ) jf2con15 [ 5.302876193624533e-17 0.8660254037844384 0.5000000000000004 25.72313 ] [ 0.5000000000000004 0.4330127018922195 -0.7499999999999996 -83.13844 ] 287.04492 1 1')

		self.transform(128, 0, 0, 0, '( 0 0 128 ) ( 128 0 128 ) ( 128 0 0 ) jf2con15 [ 1 0 -0 0 ] [ -0 0 -1 0 ] 0 1 1', '( 128 0 128 ) ( 256 0 128 ) ( 256 0 0 ) jf2con15 [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1')
		self.transform(192, 0, 0, 0, '( 0 0 128 ) ( 128 0 128 ) ( 128 0 0 ) jf2con15 [ 1 0 -0 0 ] [ -0 0 -1 0 ] 0 1 1', '( 192 0 128 ) ( 320 0 128 ) ( 320 0 0 ) jf2con15 [ 1 0 0 -64 ] [ 0 0 -1 0 ] 0 1 1')


		self.transform(0, 0, 0, 90, '( 200 -128 56 ) ( 200 -128 168 ) ( 184 -120 168 ) 128_blood_1 [ -1 0 0 -8.000015 ] [ 0 0 -1 -40 ] 0 1 1', '( 128 200 56 ) ( 128 200 168 ) ( 120 184 168 ) 128_blood_1 [ -2.83276944882399e-16 -1 0 -8.000015 ] [ 0 0 -1 -40 ] 0 1 1')
		self.transform(0, 0, 0, 90, '( 184 -120 56 ) ( 184 -120 168 ) ( 136 -120 168 ) 128_blood_2 [ -1 0 0 -7.9999847 ] [ 0 0 -1 -40 ] 0 1 1', '( 120 184 56 ) ( 120 184 168 ) ( 120 136 168 ) 128_blood_2 [ -2.83276944882399e-16 -1 0 -7.9999847 ] [ 0 0 -1 -40 ] 0 1 1')
		self.transform(0, 0, 0, 90, '( 136 -120 168 ) ( 120 -128 168 ) ( 120 -128 56 ) 128_blood_3 [ -1 0 0 -8 ] [ 0 0 -1 -40 ] 0 1 1', '( 120 136 168 ) ( 128 120 168 ) ( 128 120 56 ) 128_blood_3 [ -2.83276944882399e-16 -1 0 -8 ] [ 0 0 -1 -40 ] 0 1 1')
		self.transform(0, 0, 0, 90, '( 184 -120 168 ) ( 200 -128 168 ) ( 120 -128 168 ) 128_blue_1 [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1', '( 120 184 168 ) ( 128 200 168 ) ( 128 120 168 ) 128_blue_1 [ 2.83276944882399e-16 1 0 0 ] [ 1 -2.83276944882399e-16 0 0 ] 270 1 1')
		self.transform(0, 0, 0, 90, '( 120 -128 168 ) ( 200 -128 168 ) ( 200 -128 56 ) 128_blue_2 [ 1 0 0 0 ] [ 0 0 -1 -48 ] 0 1 1', '( 128 120 168 ) ( 128 200 168 ) ( 128 200 56 ) 128_blue_2 [ 2.83276944882399e-16 1 0 0 ] [ 0 0 -1 -48 ] 0 1 1')
		self.transform(0, 0, 0, 90, '( 120 -128 56 ) ( 200 -128 56 ) ( 184 -120 56 ) 128_blue_3 [ -1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1', '( 128 120 56 ) ( 128 200 56 ) ( 120 184 56 ) 128_blue_3 [ -2.83276944882399e-16 -1 0 0 ] [ 1 -2.83276944882399e-16 0 0 ] 90 1 1')

		self.transform(128, 0, 0, 90, '( 184 -120 56 ) ( 184 -120 168 ) ( 136 -120 168 ) {bwall_climb [ -1 -0 0 0 ] [ -0 0 -1 0 ] 0 1 1', '( 248 184 56 ) ( 248 184 168 ) ( 248 136 168 ) {bwall_climb [ -2.83276944882399e-16 -1 0 -7.9999847 ] [ 0 0 -1 -40 ] 0 1 1')
		self.transform(128, 0, 0, 90, '( 200 -128 56 ) ( 200 -128 168 ) ( 184 -120 168 ) 128_blood_1 [ -0.8944148781834536 0.44723822028542903 0 0 ] [ 0 0 -1 0 ] 0 1 1', '( 256 200 56 ) ( 256 200 168 ) ( 248 184 168 ) 128_blood_1 [ -2.83276944882399e-16 -1 0 -8.000015 ] [ 0 0 -1 -40 ] 0 1 1')
		self.transform(128, 0, 0, 90, '( 136 -120 168 ) ( 120 -128 168 ) ( 120 -128 56 ) 128_blood_3 [ -0.894427190999916 -0.447213595499958 0 0 ] [ -0 0 -1 0 ] 0 1 1', '( 248 136 168 ) ( 256 120 168 ) ( 256 120 56 ) 128_blood_3 [ -2.83276944882399e-16 -1 0 -8 ] [ 0 0 -1 -40 ] 0 1 1')
		self.transform(128, 0, 0, 90, '( 184 -120 168 ) ( 200 -128 168 ) ( 120 -128 168 ) 128_blue_1 [ 1 0 -0 0 ] [ 0 -1 -0 0 ] 0 1 1', '( 248 184 168 ) ( 256 200 168 ) ( 256 120 168 ) 128_blue_1 [ 2.83276944882399e-16 1 0 0 ] [ 1 -2.83276944882399e-16 0 0 ] 270 1 1')
		self.transform(128, 0, 0, 90, '( 120 -128 168 ) ( 200 -128 168 ) ( 200 -128 56 ) 128_blue_2 [ 1 0 -0 0 ] [ 0 -0 -1 0 ] 0 1 1', '( 256 120 168 ) ( 256 200 168 ) ( 256 200 56 ) 128_blue_2 [ 2.83276944882399e-16 1 0 0 ] [ 0 0 -1 -48 ] 0 1 1')
		self.transform(128, 0, 0, 90, '( 120 -128 56 ) ( 200 -128 56 ) ( 184 -120 56 ) 128_blue_3 [ -1 0 0 0 ] [ -0 -1 -0 0 ] 0 1 1', '( 256 120 56 ) ( 256 200 56 ) ( 248 184 56 ) 128_blue_3 [ -2.83276944882399e-16 -1 0 0 ] [ 1 -2.83276944882399e-16 0 0 ] 90 1 1')






if __name__ == '__main__':
	unittest.main()
