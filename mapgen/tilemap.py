#
# 3D tile or cell map. A dict with x,y,z coordinates (tuple) as the key.
#
from mapgen.tiles import getTile
from mapgen.vec import Vec



class TileMap(dict):
	def __init__(self, width=None, height=None, tile=None):
		self._width = width
		self._height = height
		self.rooms = []
		self.objects = []

		if width and height:
			self.fill(tile, Vec(0, 0, 0), Vec(width, height, 0))


	def __getitem__(self, key):
		if len(key) == 2:
			key = (key[0], key[1], 0)

		return super().__getitem__(key)


	def __setitem__(self, key, value):
		if len(key) == 2:
			key = (key[0], key[1], 0)

		super().__setitem__(key, value)


	def fill(self, new_tile, start: Vec = None, end: Vec = None) -> "TileMap":
		start = start or Vec(0, 0, 0)
		end = end or Vec(self.width, self.height, 0)

		for z in range(min(start.z, end.z), max(start.z, end.z) + 1):
			for x in range(min(start.x, end.x), max(start.x, end.x) + 1):
				for y in range(min(start.y, end.y), max(start.y, end.y) + 1):
					self[Vec(x, y, z)] = new_tile

		return self


	@property
	def mins(self) -> Vec:
		min_x = 0
		min_y = 0
		min_z = 0

		for (x, y, z) in self.keys():
			min_x = min(min_x, x)
			min_y = min(min_y, y)
			min_z = min(min_z, z)

		return Vec(min_x, min_y, min_z)


	@property
	def maxs(self) -> Vec:
		max_x = 0
		max_y = 0
		max_z = 0

		for (x, y, z) in self.keys():
			max_x = max(max_x, x)
			max_y = max(max_y, y)
			max_z = max(max_z, z)

		return Vec(max_x, max_y, max_z)


	@property
	def width(self) -> int:
		if self._width is not None:
			return self._width

		right = 0
		left = 0

		for (x, y, z) in self.keys():
			left = min(left, x)
			right = max(right, x)

		return right - left


	@property
	def height(self) -> int:
		if self._height is not None:
			return self._height

		top = 0
		bottom = 0

		for (x, y, z) in self.keys():
			top = max(top, y)
			bottom = min(bottom, y)

		return top - bottom


	def rotate90(self):
		"""
		x, y = 2, 4
		rx, ry = -y, x
		8, 5 -> -5, 8 -> -8, -5 -> 5, -8 -> 8, 5
		"""
		new_tilemap = TileMap()
		for (x, y, z), tile in self.items():
			new_tilemap[-y, x, z] = tile

		return new_tilemap


	@classmethod
	def from_strings(cls, strings: list, mapping_overrides: dict = None) -> "TileMap":
		"""
		Returns a TileMap from a list of strings

		:param      cls:      The cls
		:type       cls:      { type_description }
		:param      strings:  The strings
		:type       strings:  { type_description }

		:returns:   A tilemap
		:rtype:     TileMap
		"""
		t = TileMap()

		mapping_overrides = mapping_overrides or {}

		for z, layer in enumerate(strings):
			for y, row in enumerate(reversed(layer)):
				for x in range(len(row)):
					ch = row[x]

					if ch is not None:
						tile = mapping_overrides.get(ch)

						if tile is None:
							tile = getTile(ch)

						t[Vec(x, y, -z)] = tile

		return t


if __name__ == '__main__':
	print("this is the new tilemap class")

	t = TileMap()
	t[0, 0] = 'foo'
	t[50, 20] = 'bar'
	print(t)
	print(t.width)

	t[3, 2, 1] = '321'
	print(t.get(Vec(3, 2, 1)))
	print(t.get((3, 2, 1)))
	print(t[3, 2, 1])
