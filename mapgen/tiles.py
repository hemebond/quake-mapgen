from enum import Enum, IntFlag, auto, IntEnum
from dataclasses import dataclass
import typing



class Colors:
	BLACK = (0, 0, 0)
	GREY = (64, 64, 64)
	DARK_GREY = (32, 32, 32)
	WHITE = (255, 255, 255)
	RED = (255, 64, 64)
	BLUE = (64, 64, 255)
	LBLUE = (128, 128, 255)
	YELLOW = (255, 255, 0)
	ORANGE = (255, 128, 32)
	PURPLE = (192, 128, 255)
	PINK = (255, 128, 192)
	GREEN = (0, 255, 0)
	BROWN = (128, 64, 0)



class Tile:
	def __init__(self, symbol: str, fg=Colors.WHITE, bg=Colors.BLACK):
		self.symbol = symbol
		self.fg = fg
		self.bg = bg

	def __str__(self):
		return str(self.symbol)



class Tiles:
	PREFAB   = Tile('P', Colors.GREY, Colors.ORANGE)
	ARMOURY  = Tile('A', Colors.YELLOW, Colors.YELLOW)
	BREACH   = Tile('B', Colors.PURPLE, Colors.PURPLE)
	BREAKER  = Tile('%', Colors.PINK, Colors.PINK)
	CORE     = Tile('C', Colors.BLUE)
	FLOOR    = Tile('.', Colors.BROWN, Colors.BROWN)
	DATACORE = Tile('D', Colors.LBLUE, Colors.LBLUE)
	DOOR     = Tile('+', Colors.WHITE, Colors.BLUE)
	EMPTY    = Tile(' ', Colors.BLACK, Colors.BLACK)
	FENCE    = Tile('X', Colors.RED, Colors.RED)
	ROOM     = Tile('▒', Colors.GREY, Colors.GREY)
	REPAIR   = Tile('R', Colors.ORANGE, Colors.ORANGE)
	SECURITY = Tile('*', Colors.YELLOW, Colors.YELLOW)
	SENTRY   = Tile('S', Colors.GREEN, Colors.GREEN)
	START    = Tile('@', Colors.WHITE)
	WALL     = Tile('#', Colors.GREY, Colors.DARK_GREY)

	PREFAB_DOOR  = Tile('+', Colors.WHITE, Colors.BLUE)
	PREFAB_FLOOR = Tile('░', Colors.BLUE, Colors.GREY)
	PREFAB_WALL  = Tile('█', Colors.GREY, Colors.BLUE)

	WALL_1 = Tile("┗", Colors.GREY, Colors.DARK_GREY)
	WALL_2 = Tile("┛", Colors.GREY, Colors.DARK_GREY)
	WALL_3 = Tile("┓", Colors.GREY, Colors.DARK_GREY)
	WALL_4 = Tile("┏", Colors.GREY, Colors.DARK_GREY)
	WALL_T = Tile("┳", Colors.GREY, Colors.DARK_GREY)
	WALL_H = Tile("━", Colors.GREY, Colors.DARK_GREY)
	WALL_V = Tile("┃", Colors.GREY, Colors.DARK_GREY)



def getTile(ch: str) -> Tile:
	"""
	Takes a tilemap character and returns a Tile object

	:param      ch:   The ASCII character used by the tile
	:type       ch:   str

	:returns:   The tile.
	:rtype:     Tile
	"""
	for k, v in Tiles.__dict__.items():
		try:
			if ch == getattr(v, 'symbol'):
				return v
		except AttributeError:
			continue

	return None
