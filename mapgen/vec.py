class Vec(tuple):
	'''
	A simple vector class
	'''

	def __new__(self, *args, **kwargs):
		"""
		Have to override __new__ because tuples are immutable
		"""

		if isinstance(args[0], tuple):
			return super().__new__(Vec, args[0])

		return super().__new__(Vec, args)

	@property
	def x(self):
		return self[0]

	@property
	def y(self):
		return self[1]

	@property
	def z(self):
		return self[2]

	def __add__(self, other):
		"""Vector addition."""
		return Vec(self.x + other[0], self.y + other[1], self.z + other[2])


	def __sub__(self, other):
		return Vec(self.x - other[0], self.y - other[1], self.z - other[2])


	def __mul__(self, other):
		"""
		Vector multiplication
		"""
		return Vec(self.x * other, self.y * other, self.z * other)


	def __rmul__(self, other):
		return self.__mul__(other)


	@property
	def surrounding(self):
		for d in [
			(+1,  0, 0),  # e
			(+1, +1, 0),  # ne
			( 0, +1, 0),  # n
			(-1, +1, 0),  # nw
			(-1,  0, 0),  # w
			(-1, -1, 0),  # sw
			( 0, -1, 0),  # s
			(+1, -1, 0),  # se
		]:
			yield self + Vec(d)

	@property
	def adjacent(self):
		for d in [
			(+1, 0, 0),    # e
			(0, +1, 0),    # n
			(-1, 0, 0),    # w
			(0, -1, 0),    # s
		]:
			yield self + Vec(d)



if __name__ == '__main__':
	v = Vec(1, 2, 0)
	print(f"Vec(1, 2, 0) * 2 = {v * 2}")
	print(f"Vec(1, 2, 0) + (1, 2, 0) = {v + (1, 2, 0)}")
