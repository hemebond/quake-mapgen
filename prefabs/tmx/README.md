# TMX Prefabs

These prefabs are for use with a Tiled _.tmx_ and _.tsx_ files.

The prefab _.map_ files must have the origin in the same position as the tile in the _.tsx_; usually in the center.
