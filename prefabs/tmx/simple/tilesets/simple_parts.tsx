<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.10" tiledversion="1.10.2" name="simple_parts" tilewidth="3" tileheight="3" tilecount="21" columns="7" objectalignment="center" fillmode="preserve-aspect-fit">
 <grid orientation="orthogonal" width="1" height="1"/>
 <transformations hflip="0" vflip="0" rotate="1" preferuntransformed="0"/>
 <image source="simple_parts.png" width="21" height="9"/>
</tileset>
