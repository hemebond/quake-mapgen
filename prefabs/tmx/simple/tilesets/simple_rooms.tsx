<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.10" tiledversion="1.10.2" name="simple_rooms" tilewidth="27" tileheight="27" tilecount="50" columns="5" objectalignment="center" fillmode="preserve-aspect-fit">
 <tileoffset x="-12" y="12"/>
 <grid orientation="orthogonal" width="1" height="1"/>
 <transformations hflip="0" vflip="0" rotate="1" preferuntransformed="0"/>
 <image source="simple_rooms.png" width="135" height="270"/>
</tileset>
