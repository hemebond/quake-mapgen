from pathlib import Path
from mapgen.qmap import QuakeMap, parse_quakemap
from mapgen.loggers.tcod import TcodLogger
from mapgen.generators.hauberk import HauberkGenerator
from mapgen.prefab_manager import PrefabManager
from mapgen.tiles import Tiles as T
import tcod



TILE_SIZE = 128
LEVEL_HEIGHT = 1024
PREFAB_SIZE = TILE_SIZE / 2
WIDTH = HEIGHT = int(4096 * 2 / TILE_SIZE)  # max size for a BSP1 quake map is ±4096u with cell size of 128u
WIDTH = HEIGHT = 17



prefabs = PrefabManager(Path("prefabs/cubes"))



def main():
	# Hauberk
	logger = TcodLogger(WIDTH, HEIGHT)
	generator = HauberkGenerator(logger=logger)
	generator.generate(WIDTH, HEIGHT, 2)
	generator.print()

	with open("prefabs/prototype/template.map") as f:
		m = parse_quakemap(f)

	place_cubes(m, generator)

	with open('test.map', 'w') as fp:
		fp.write(str(m))

	print("Output file finished")

	while True:
		for event in tcod.event.wait():
			if isinstance(event, tcod.event.Quit):
				raise SystemExit()



def place_cubes(quake_map: QuakeMap, generator):
	floor_prefab = prefabs.get('floor')
	room_prefab = prefabs.get('room')
	wall_prefab = prefabs.get('wall')
	door_prefab = prefabs.get('door')
	other_prefab = prefabs.get('other')

	for (x, y, z), tile in generator.level.items():
		if tile is None:
			continue

		if tile == T.ROOM:
			prefab = room_prefab
		elif tile == T.FLOOR:
			prefab = floor_prefab
		elif tile == T.WALL:
			prefab = wall_prefab
		elif tile == T.DOOR:
			prefab = door_prefab
		else:
			prefab = other_prefab

		quake_map.merge(prefab, x * TILE_SIZE, y * TILE_SIZE, z * LEVEL_HEIGHT, 0)



if __name__ == '__main__':
	main()
