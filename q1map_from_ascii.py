from pathlib import Path
from mapgen.tilemap import TileMap
from mapgen.tiles import Tiles, getTile
from mapgen.prefab_manager import PrefabManager
from mapgen.qmap import QuakeMap
from mapgen.vec import Vec



TILE_SIZE = 128
LEVEL_HEIGHT = 128



def main():
	with open('ascii.txt') as f:
		src = f.read().splitlines()

	tilemap = TileMap.from_strings([src])

	m = QuakeMap()

	prefabs = PrefabManager(Path("prefabs/prototype"))

	tile_mapping = {
		"┗": (prefabs.get("ascii_corner"), 0),
		"┛": (prefabs.get("ascii_corner"), 90),
		"┓": (prefabs.get("ascii_corner"), 180),
		"┏": (prefabs.get("ascii_corner"), 270),
		"┳": (prefabs.get("ascii_tee"), 0),
		"━": (prefabs.get("ascii_h"), 0),
		"┃": (prefabs.get("ascii_h"), 90),
		"┼": (prefabs.get("ascii_cross"), 0),
		"+": (prefabs.get("ascii_door"), 0),
		".": (prefabs.get("ascii_floor"), 0),
		"├"
	}

	for pos, tile in tilemap.items():
		if tile is None:
			continue

		try:
			prefab, angle = tile_mapping[str(tile.symbol)]
		except KeyError:
			continue

		pos = Vec(pos)
		m.merge(prefab, pos.x * TILE_SIZE, pos.y * TILE_SIZE, pos.z * LEVEL_HEIGHT, angle)

	with open('test.map', 'w') as fp:
		fp.write(str(m))


if __name__ == '__main__':
	main()
