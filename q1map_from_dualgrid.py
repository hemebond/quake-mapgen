#!/usr/bin/env python
# Make sure 'dejavu10x10_gs_tc.png' is in the same directory as this script.
import tcod.console
import tcod.context
import tcod.event
import tcod.tileset

from pathlib import Path
from random import randint, choice

from mapgen.qmap import QuakeMap, parse_quakemap, parse_plane, Entity, Brush
from mapgen.tilemap import TileMap
from mapgen.tiles import Tiles as T
from mapgen.vec import Vec
from mapgen.prefab_manager import PrefabManager

from mapgen.generators.hauberk import HauberkGenerator
from mapgen.loggers.tcod import TcodLogger

from img_from_tilemap import tilemap_to_img

WIDTH, HEIGHT = 80, 60  # Console width and height in tiles.
TILE_SIZE = 128
LEVEL_HEIGHT = 512



prefabs = PrefabManager([Path("prefabs/dualgrid"), Path("prefabs/prototype")])
prefab_id = 0  # maintain a count of the number of prefabs placed so we can replace target/targetnames in them

# each group is a type of wall pattern, with each entry
# being the 90ccw rotation of the previous in the group
# Bitmask is a numeric version of the string pattern, and is used to give a
# simple numeric value for replacement patterns to use
#
# wall bitmask     8,   4,  1,  2
# non-wall bitmask 128, 64, 16, 32
#
# pattern, symbol, bitmask, prefab, rotation
# walls = [
# 	[
# 		("..#.", '┐', 1, "wall_1", 0),
# 		("...#", '┌', 2, "wall_1", 90),
# 		(".#..", '└', 4, "wall_1", 180),
# 		("#...", '┘', 8, "wall_1", 270),
# 	],
# 	[
# 		("?.#.", ' ', 129, "wall_half_left", 0),
# 		("..?#", ' ', 18,  "wall_half_left", 90),
# 		(".#.?", ' ', 36,  "wall_half_left", 180),
# 		("#?..", ' ', 72,  "wall_half_left", 270),
# 	],
# 	[
# 		("#.?.", ' ', 24,  "wall_half_right", 0),
# 		("..#?", ' ', 33,  "wall_half_right", 90),
# 		(".?.#", ' ', 66,  "wall_half_right", 180),
# 		("?#..", ' ', 132, "wall_half_right", 270),
# 	],
# 	[
# 		("#.#.", '🭰', 9,  "wall_2", 0),
# 		("..##", '🭻', 3,  "wall_2", 90),
# 		(".#.#", '🭵', 6,  "wall_2", 180),
# 		("##..", '🭶', 12, "wall_2", 270),
# 	],
# 	[
# 		("#?#.", ' ', 73,  "wall_half_left", 0),
# 		("?.##", ' ', 131, "wall_half_left", 90),
# 		(".#?#", ' ', 22,  "wall_half_left", 180),
# 		("##.?", ' ', 28,  "wall_half_left", 270),
# 	],
# 	[
# 		("#.#?", ' ', 41,  "wall_half_right", 0),
# 		(".?##", ' ', 67,  "wall_half_right", 90),
# 		("?#.#", ' ', 134, "wall_half_right", 180),
# 		("##?.", ' ', 28,  "wall_half_right", 270),
# 	],
# 	[
# 		("#.##", '🭼', 11, "wall_3", 0),
# 		(".###", '🭿', 7,  "wall_3", 90),
# 		("##.#", '🭾', 14, "wall_3", 180),
# 		("###.", '🭽', 13, "wall_3", 270),
# 	]
# ]

prefab_lookup = {
	# tile formation: (prefab name, rotation)
	"....": ("wall_0", 0),

	"..#.": ("wall_1", 0),
	"...#": ("wall_1", 90),
	".#..": ("wall_1", 180),
	"#...": ("wall_1", 270),

	"?.#.": ("wall_half_left", 0),
	"..?#": ("wall_half_left", 90),
	".#.?": ("wall_half_left", 180),
	"#?..": ("wall_half_left", 270),

	"#.?.": ("wall_half_right", 0),
	"..#?": ("wall_half_right", 90),
	".?.#": ("wall_half_right", 180),
	"?#..": ("wall_half_right", 270),

	"#.#.": ("wall_2", 0),
	"..##": ("wall_2", 90),
	".#.#": ("wall_2", 180),
	"##..": ("wall_2", 270),

	"#?#.": ("wall_half_left", 0),
	"?.##": ("wall_half_left", 90),
	".#?#": ("wall_half_left", 180),
	"##.?": ("wall_half_left", 270),

	"#.#?": ("wall_half_right", 0),
	".?##": ("wall_half_right", 90),
	"?#.#": ("wall_half_right", 180),
	"##?.": ("wall_half_right", 270),

	"#.##": ("wall_3", 0),
	".###": ("wall_3", 90),
	"##.#": ("wall_3", 180),
	"###.": ("wall_3", 270),

	"####": ("wall_4", 0),
}



def get_src_tiles(tilemap: TileMap, pos: Vec) -> list:
	"""
	Takes a position and returns a list of source tiles around it

	|12
	|34
	+--

	:param      pos:  The position
	:type       pos:  Vec
	"""
	tiles = []
	for offset in [Vec(0, +1, 0), Vec(+1, +1, 0), Vec(0, 0, 0), Vec(+1, 0, 0)]:
		tile_pos = pos + offset
		tile = tilemap.get(tile_pos, ' ')

		# Rooms and floors are the same thing
		if tile == T.ROOM:
			tile = T.FLOOR

		# We only care about floors and walls. Everything else will be a prefab of some sort.
		if tile not in (T.FLOOR, T.WALL, ' '):
			tile = '?'

		tiles.append(tile)

	return tiles



def dualtile_to_world_pos(pos: Vec) -> Vec:
	# Returns a Quake map world position offset by half a tile
	return tile_pos_to_world(pos) + (Vec(0.5, 0.5, 0) * TILE_SIZE)


def tile_pos_to_world(pos: Vec) -> Vec:
	return Vec(
		pos.x * TILE_SIZE,
		pos.y * TILE_SIZE,
		pos.z * LEVEL_HEIGHT,
	)


def is_solid_wall(s: str) -> bool:
	return s == "####"


def is_empty(s: str) -> bool:
	return s == "...."



def is_wall(tile):
	# Placeholder for more advanced logic
	return tile in (T.WALL, T.PREFAB_WALL)

def is_floor(tile):
	return tile in (T.FLOOR, T.ROOM)



def make_prefab_unique(prefab, context=None):
	"""
	Replace placeholders in entity property values
	"""
	global prefab_id

	prefab_id += 1

	for ent in prefab:
		for attr_name in ent.__dict__.keys():
			if attr_name == "brushes":
				continue

			value = getattr(ent, attr_name)
			setattr(ent, attr_name, value.replace("#", f"{prefab_id}"))

	return prefab_id



def create_dualgrid_tilemap(tilemap: TileMap) -> TileMap:
	# Takes a regular tilemap and returns a dual grid version of it
	dg_tilemap = TileMap()

	for z in range(tilemap.mins.z, tilemap.maxs.z + 1):
		for y in range(tilemap.mins.y, tilemap.maxs.y):
			for x in range(tilemap.mins.x, tilemap.maxs.x):
				pos = Vec(x, y, z)

				src_tiles = get_src_tiles(tilemap, pos)
				src_tiles_str = ''.join(map(str, src_tiles))

				if is_solid_wall(src_tiles_str) or is_empty(src_tiles_str):
					# Ignore points entirely surrounded by walls or empty space
					continue

				if None in src_tiles:
					# If there are empty tiles in the list then this is on the outside of the map
					continue

				dg_tilemap[pos] = src_tiles_str

	return dg_tilemap



def place_walls(quake_map: QuakeMap, tilemap: TileMap) -> QuakeMap:
	t = create_dualgrid_tilemap(tilemap)

	for pos, src_tiles_str in t.items():
		pos = Vec(pos)

		try:
			prefab_name, prefab_rotation = prefab_lookup[src_tiles_str]
		except KeyError:
			continue

		# get list of straight wall tiles positions
		# pop random tile position from list
		# new list of long wall []
		# randomise list of cardinal directions
		# for each direction:
		#   if pos + dir in list, set direction; we now have two wall tiles in a row
		#     pop position from list
		#     keep going in dir until no tile in list
		#   break

		prefab_pos = dualtile_to_world_pos(pos)
		prefab = prefabs.get(prefab_name)

		quake_map.merge(prefab, prefab_pos.x, prefab_pos.y, prefab_pos.z, prefab_rotation)

	return quake_map



def place_doors(quake_map: QuakeMap, tilemap: TileMap) -> QuakeMap:
	"""
	Iterate through tiles in a level. If the tile is a door tile, place a door prefab into the map.

	:param      quake_map:  The quake map
	:type       quake_map:  QuakeMap
	"""
	for pos, tile in tilemap.items():
		if tile == T.DOOR:
			pos = Vec(pos)

			adjacent_tiles = [tilemap.get(adj) for adj in pos.adjacent]

			if is_wall(adjacent_tiles[1]):
				angle = 0
			else:
				angle = 90

			# prefab = prefabs.get("doors/")
			if randint(0, 1):
				prefab = prefabs.get("doors/")
			else:
				prefab = prefabs.get("laser_fence")

			make_prefab_unique(prefab)
			org = tile_pos_to_world(pos)
			quake_map.merge(prefab, org.x, org.y, org.z, angle)

	return quake_map



def place_floors(quake_map: QuakeMap, tilemap: TileMap) -> QuakeMap:
	"""
	Iterate through each tile of the generator level. If it's a floor, place a floor prefab.

	:param      quake_map:  The quake map
	:type       quake_map:  QuakeMap
	"""
	floors = [(pos, tile) for pos, tile in tilemap.items() if is_floor(tile)]
	for pos, tile in floors:
		pos = Vec(pos)
		org = tile_pos_to_world(pos)

		prefab = prefabs.get('floor')
		rotation = randint(0, 3) * 90
		quake_map.merge(prefab, org.x, org.y, org.z, rotation)

		# Add an info_notnull entity to a cell so the
		# game can understand the structure of the map
		info = Entity()
		info_org = org + Vec(0, 0, TILE_SIZE * 0.5)
		setattr(info, "classname", "info_notnull")
		setattr(info, "message", "floor")
		setattr(info, "origin", ' '.join([str(i) for i in info_org]))
		quake_map.append(info)

	return quake_map



def place_ceilings(quake_map: QuakeMap, generator) -> QuakeMap:
	"""
	Iterate through each tile of the generator level. If it's a floor, place a floor prefab.

	:param      quake_map:  The quake map
	:type       quake_map:  QuakeMap
	"""
	CEILING_HEIGHT = 128  # Will eventually get this from the generator level data

	floors = [(pos, tile) for pos, tile in generator.tilemap.items() if is_floor(tile)]
	for pos, tile in floors:
		pos = Vec(pos)
		org = tile_pos_to_world(pos)

		obj = generator.objects.get(pos, None)
		if obj == T.BREACH:
			prefab = prefabs.get("monster_spawn_ceiling")
		else:
			prefab = prefabs.get('ceiling')

		rotation = randint(0, 3) * 90
		quake_map.merge(prefab, org.x, org.y, org.z + CEILING_HEIGHT, rotation)

	return quake_map



def place_objects(quake_map: QuakeMap, generator) -> QuakeMap:
	for (x, y, z), obj in generator.objects.items():
		pos = Vec(x, y, z)
		org = tile_pos_to_world(pos)

		if obj == T.DATACORE:
			adjacent_walls = [p for p in pos.adjacent if generator.tilemap[p] == T.WALL]
			random_wall = choice(adjacent_walls)
			offset = random_wall - pos

			# Calculate the angle to face away from the wall
			angle = {
				(+1, 0, 0): 180,
				(0, +1, 0): 270,
				(-1, 0, 0): 0,
				(0, -1, 0): 90,
			}[offset]

			prefab = prefabs.get("datacore")
			make_prefab_unique(prefab)
			quake_map.merge(prefab, org.x, org.y, org.z, angle)

		elif obj in (T.REPAIR, T.SENTRY, T.ARMOURY):
			adjacent_walls = [p for p in pos.adjacent if generator.tilemap[p] == T.WALL]
			random_wall = choice(adjacent_walls)
			offset = random_wall - pos

			# Calculate the angle to face away from the wall
			angle = {
				(+1, 0, 0): 180,
				(0, +1, 0): 270,
				(-1, 0, 0): 0,
				(0, -1, 0): 90,
			}[offset]

			if obj == T.REPAIR:
				prefab = prefabs.get("station_health")
			elif obj == T.ARMOURY:
				prefab = prefabs.get("station_ammo")
			elif obj == T.SENTRY:
				prefab = prefabs.get("station_sentry")

			make_prefab_unique(prefab)
			quake_map.merge(prefab, org.x, org.y, org.z, angle)

		# elif obj == T.SECURITY:
		# 	place_radar(quake_map, generator, pos)

		elif obj == 'boxes':
			angle = choice([0, 90, 180, 270])
			prefab = prefabs.get("clutter/boxes*")
			quake_map.merge(prefab, org.x, org.y, org.z, angle)

		else:
			print(f"Skipping object type {obj}")

	return quake_map



def place_prefab_rooms(quake_map: QuakeMap, generator) -> QuakeMap:
	"""
	Iterate through each room in the level, if it's a prefab, add that prefab to quake_map as the position of the room.

	:param      quake_map:  The quake map
	:type       quake_map:  QuakeMap
	:param      level:      The level tilemap
	:type       level:      TileMap
	"""
	for room in generator.prefab_rooms:
		prefab = prefabs.get(room.prefab_name)
		angle = getattr(room, "angle", 0)

		print(f"Adding prefab {room.prefab_name} at {room.x},{room.y}")

		make_prefab_unique(prefab)
		org = tile_pos_to_world(Vec(room.x, room.y, room.z))
		quake_map.merge(prefab, org.x, org.y, org.z, angle)

	return quake_map



def place_map_hints(quake_map: QuakeMap, tilemap: TileMap) -> QuakeMap:
	"""
	Adds some point entities to the map to show where the dual grid tiles are
	"""
	for pos, tile in tilemap.items():
		pos = Vec(pos)
		org = dualtile_to_world_pos(pos)
		org = (org.x, org.y, 256)

		e = Entity()
		e.classname = "grid_point"
		e.origin = ' '.join(map(str, org))
		quake_map.append(e)

	return quake_map



def place_radar(quake_map, generator, pos):
	org = dualtile_to_world_pos(pos)

	# Add the radar wad to worldspawn so it compiles properly
	quake_map.add_wad("radar.wad")

	#
	# Place the radar display prefab
	#
	prefab = prefabs.get("radar")
	prefab_id = make_prefab_unique(prefab)

	# Update the texture name to match the current floor
	for e in prefab:
		for brush in e.brushes:
			for plane in brush.planes:
				plane.texture.name = plane.texture.name.replace("#", f"{abs(pos.z)}")

	quake_map.merge(prefab, org.x, org.y, org.z, 0)

	#
	# Place a trigger brush around the level
	#
	left = 0 - (TILE_SIZE / 2)
	right = left + (generator.tilemap.width * TILE_SIZE)
	down = 0 - (TILE_SIZE / 2)
	up = down + (generator.tilemap.height * TILE_SIZE)
	bottom = org.z - TILE_SIZE
	top = org.z + (TILE_SIZE * 2)

	e = Entity()
	setattr(e, "classname", "func_radar_zone")
	setattr(e, "target", f"radar_display_{prefab_id}")

	planes = [
	f"( {left} {up} {top} ) ( {left} {down} {top} ) ( {left} {down} {bottom} ) trigger [ 0 -1 0 0 ] [ 0 0 -1 0 ] 0 1 1",  # left
	f"( {left} {down} {top} ) ( {right} {down} {top} ) ( {right} {down} {bottom} ) trigger [ 1 0 -0 0 ] [ 0 -0 -1 0 ] 0 1 1",  # down
	f"( {left} {down} {bottom} ) ( {right} {up} {bottom} ) ( {left} {up} {bottom} ) trigger [ -1 0 0 0 ] [ -0 -1 -0 0 ] 0 1 1",  # bottom
	f"( {left} {up} {top} ) ( {right} {up} {top} ) ( {left} {down} {top} ) trigger [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1",  # top
	f"( {right} {up} {top} ) ( {left} {up} {top} ) ( {left} {up} {bottom} ) trigger [ -1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1",  # up
	f"( {right} {down} {top} ) ( {right} {up} {top} ) ( {right} {up} {bottom} ) trigger [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1",  # right
	]

	b = Brush()
	for p in planes:
		b.planes.append(parse_plane(p))

	e.brushes.append(b)
	quake_map.append(e)



def main() -> None:
	import argparse

	parser = argparse.ArgumentParser(
		description='Create a Quake map file from a generated 3D tilemap'
	)
	parser.add_argument('-o', '--outdir', type=str, default='.')
	parser.add_argument('-f', '--filename', type=str, default="test.map")
	parser.add_argument('-t', '--tcodlogger', action="store_true")
	parser.add_argument('--width', type=int, default=17)
	parser.add_argument('--height', type=int, default=17)
	parser.add_argument('--floors', type=int, default=3)

	args = parser.parse_args()

	if args.tcodlogger:
		logger = TcodLogger(args.width, args.height)
	else:
		logger = None

	generator = HauberkGenerator(logger=logger)
	generator.generate(args.width, args.height, args.floors)

	for z in range(generator.floors):
		print(f"Saving tilemap image to {args.outdir}/radar{z}.png")
		im = tilemap_to_img(generator, -z)
		im.save(f"{args.outdir}/radar{z}.png")

	# m = QuakeMap()
	with open("./prefabs/prototype/template.map") as f:
		m = parse_quakemap(f)

	m = place_walls(m, generator.tilemap)
	m = place_doors(m, generator.tilemap)
	m = place_floors(m, generator.tilemap)
	m = place_ceilings(m, generator)
	m = place_prefab_rooms(m, generator)
	m = place_objects(m, generator)

	setattr(m[0], "size", f"{args.width} {args.height} {args.floors}")

	with open(f'{args.outdir}/{args.filename}', 'w') as fp:
		fp.write(str(m))

	if logger:
		while True:
			for event in tcod.event.wait():
				if isinstance(event, tcod.event.Quit):
					raise SystemExit()


if __name__ == "__main__":
	main()
