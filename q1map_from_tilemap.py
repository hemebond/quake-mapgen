import logging
from pathlib import Path
from random import choice, choices, random, randint
import tcod
from mapgen.qmap import QuakeMap, parse_quakemap, Entity, Brush, Plane, parse_plane
from quakemapgen.generators.atthematinee import RoomAdditionGenerator, WALL_TILE, FLOOR_TILE
from quakemapgen.generators.shuffle.shuffle import Level as ShuffleGenerator
from mapgen.generators.hauberk import HauberkGenerator
from mapgen.tilemap import TileMap
from mapgen.loggers.tcod import TcodLogger
from mapgen.tiles import Tiles as T
from mapgen.vec import Vec
from mapgen.prefab_manager import PrefabManager
from img_from_tilemap import tilemap_to_img



txtlogger = logging.getLogger()
txtlogger.setLevel(logging.DEBUG)



TILE_SIZE = 128
LEVEL_HEIGHT = 512

# prefabs = PrefabManager(Path("prefabs/prototype"))
prefabs = PrefabManager([Path("prefabs/mrc"), Path("prefabs/prototype")])

occupied = {}  # a list of positions that have already been processed

prefab_id = 0  # maintain a count of the number of prefabs placed so we can replace target/targetnames in them



def main(width, height, floors):
	# Hauberk
	logger = TcodLogger(width * floors, height)
	generator = HauberkGenerator(logger=logger)
	generator.generate(width, height, floors)
	generator.print()

	for z in range(generator.floors):
		z_index = -z
		im = tilemap_to_img(generator, z_index)
		im.save(f"radar{z}.png")

	with open("./prefabs/prototype/template.map") as f:
		m = parse_quakemap(f)

	# TODO: Offset all tiles so that it's centered on 0,0,0 world origin

	place_doors(m, generator)
	place_walls(m, generator)
	place_prefab_rooms(m, generator)
	place_floors(m, generator)
	place_ceilings(m, generator)
	place_objects(m, generator)

	with open('test.map', 'w') as fp:
		fp.write(str(m))

	print("Output file finished")

	while True:
		for event in tcod.event.wait():
			if isinstance(event, tcod.event.Quit):
				raise SystemExit()



def is_wall(tile):
	# Placeholder for more advanced logic
	return tile == T.WALL


def is_floor(tile):
	# Placeholder for more advanced logic
	return tile in (T.ROOM, T.FLOOR, T.DOOR)


def is_door(tile):
	return tile == T.DOOR



def to_world(vector: Vec):
	"""
	Converts a tile/cell position to a world position
	"""
	return Vec(
		vector.x * TILE_SIZE,
		vector.y * TILE_SIZE,
		vector.z * LEVEL_HEIGHT,
	)


def place_prefab(quake_map: QuakeMap, prefab, position, angle):
	quake_map.merge(prefab, org.x, org.y, org.z + CEILING_HEIGHT, angle)



def place_floors(quake_map: QuakeMap, generator):
	"""
	Iterate through each tile of the generator level. If it's a floor, place a floor prefab.

	:param      quake_map:  The quake map
	:type       quake_map:  QuakeMap
	:param      generator:  The generator
	:type       generator:  { type_description }
	"""
	floors = [(pos, tile) for pos, tile in generator.tilemap.items() if tile in (T.FLOOR, T.ROOM)]
	for pos, tile in floors:
		pos = Vec(pos)
		org = to_world(pos)

		prefab = prefabs.get('floors/')
		quake_map.merge(prefab, org.x, org.y, org.z, 0)


def place_ceilings(quake_map: QuakeMap, generator):
	CEILING_HEIGHT = 128  # Will eventually get this from the generator level data

	floors = [(pos, tile) for pos, tile in generator.tilemap.items() if tile in (T.ROOM, T.FLOOR)]
	for pos, tile in floors:
		obj = generator.objects.get(pos, None)
		if obj == T.BREACH:
			prefab = prefabs.get("monster_spawn_ceiling")
		else:
			prefab = prefabs.get('ceilings/')

		if not prefab:
			return

		angle = choices([0, 90, 180, 270])[0]
		org = to_world(Vec(pos))
		quake_map.merge(prefab, org.x, org.y, org.z + CEILING_HEIGHT, angle)




def place_walls(quake_map: QuakeMap, generator):
	"""
	Iterate through the level and we are a floor tile, check adjacent tiles. If there is an adjacent empty
	tile then place a wall there facing towards us.

	:param      quake_map:  The quake map
	:type       quake_map:  QuakeMap
	:param      level:      The level
	:type       level:      TileMap
	"""

	for pos, tile in generator.tilemap.items():
		if tile in (T.ROOM, T.FLOOR):
			pos = Vec(pos)
			for adj_pos, angle in zip(pos.adjacent, (180, 270, 0, 90)):
				adj_tile = generator.tilemap.get(adj_pos, None)

				if adj_tile in (T.PREFAB_WALL, T.WALL):
					# prefab = prefabs.get(
					# 	choices(
					# 		["walls/0", "walls/1", "walls/2", "walls/3"],
					# 		weights=(6, 1, 1, 1)
					# 	)[0]
					# )
					if randint(0, 4) < 4:
						# Use the first wall prefab as the "default" wall
						p_names = prefabs.get_names("walls/")
						prefab = prefabs.get(p_names[0])
					else:
						prefab = prefabs.get("walls/")

					org = to_world(adj_pos)
					quake_map.merge(prefab, org.x, org.y, org.z, angle)




def tiles_match_template(level: TileMap, template: TileMap, origin: Vec):
	"""
	Returns True if every tile in the template matches the tiles at that position.

	:param      level:     The tiles
	:type       level:     TileMap
	:param      template:  The template
	:type       template:  TileMap
	"""
	for tile_position, tile in template.items():
		world_position = origin + Vec(tile_position)

		if world_position in occupied.keys():
			return False

		if level.get(world_position, None) != tile:
			return False

	return True



def place_doors(quake_map, generator):
	"""
	Iterate through tiles in a level. If the tile is a door tile, place a door prefab into the map.

	:param      quake_map:  The quake map
	:type       quake_map:  QuakeMap
	:param      level:      The level
	:type       level:      TileMap
	"""
	for pos, tile in generator.tilemap.items():
		if tile == T.DOOR:
			pos = Vec(pos)

			adjacent_tiles = [generator.tilemap.get(adj) for adj in pos.adjacent]

			if is_wall(adjacent_tiles[1]):
				angle = 0
			else:
				angle = 90

			if randint(0, 1):
				prefab = prefabs.get("doors/")
			else:
				prefab = prefabs.get("laser_fence")

			make_prefab_unique(prefab)
			org = to_world(pos)
			quake_map.merge(prefab, org.x, org.y, org.z, angle)



def make_prefab_unique(prefab, context=None):
	"""
	Replace placeholders in entity property values
	"""
	global prefab_id

	prefab_id += 1

	for ent in prefab:
		for attr_name in ent.__dict__.keys():
			if attr_name == "brushes":
				continue

			value = getattr(ent, attr_name)
			setattr(ent, attr_name, value.replace("#", f"{prefab_id}"))

	return prefab_id


def place_prefab_rooms(quake_map: QuakeMap, generator):
	"""
	Iterate through each room in the level, if it's a prefab, add that prefab to quake_map as the position of the room.

	:param      quake_map:  The quake map
	:type       quake_map:  QuakeMap
	:param      level:      The level tilemap
	:type       level:      TileMap
	"""
	for room in generator.prefab_rooms:
		prefab = prefabs.get(room.prefab_name)
		angle = getattr(room, "angle", 0)

		print(f"Adding prefab {room.prefab_name} at {room.x},{room.y}")

		make_prefab_unique(prefab)
		org = to_world(room)
		quake_map.merge(prefab, org.x, org.y, org.z, angle)



def place_objects(quake_map: QuakeMap, generator):
	for (x, y, z), obj in generator.objects.items():
		pos = Vec(x, y, z)

		if obj == T.DATACORE:
			adjacent_walls = [p for p in pos.adjacent if generator.tilemap[p] == T.WALL]
			random_wall = choice(adjacent_walls)
			offset = random_wall - pos

			# Calculate the angle to face away from the wall
			angle = {
				(+1, 0, 0): 180,
				(0, +1, 0): 270,
				(-1, 0, 0): 0,
				(0, -1, 0): 90,
			}[offset]

			org = to_world(pos)

			prefab = prefabs.get("datacore")
			make_prefab_unique(prefab)
			quake_map.merge(prefab, org.x, org.y, org.z, angle)

		elif obj in (T.REPAIR, T.SENTRY, T.ARMOURY):
			adjacent_walls = [p for p in pos.adjacent if generator.tilemap[p] == T.WALL]
			random_wall = choice(adjacent_walls)
			offset = random_wall - pos

			# Calculate the angle to face away from the wall
			angle = {
				(+1, 0, 0): 180,
				(0, +1, 0): 270,
				(-1, 0, 0): 0,
				(0, -1, 0): 90,
			}[offset]

			org = to_world(pos)

			if obj == T.REPAIR:
				prefab = prefabs.get("station_health")
			elif obj == T.ARMOURY:
				prefab = prefabs.get("station_ammo")
			elif obj == T.SENTRY:
				prefab = prefabs.get("station_sentry")

			make_prefab_unique(prefab)
			quake_map.merge(prefab, org.x, org.y, org.z, angle)

		elif obj == T.SECURITY:
			place_radar(quake_map, generator, pos)


		elif obj == 'boxes':
			angle = choice([0, 90, 180, 270])
			prefab = prefabs.get("clutter/boxes*")
			org = to_world(pos)
			quake_map.merge(prefab, org.x, org.y, org.z, angle)

		# elif obj == 'monster_spawn':
		# 	org = to_world(pos)

		# 	spawn = Entity()
		# 	setattr(spawn, 'classname', 'monster_spawn_point')
		# 	setattr(spawn, 'origin', f'{org.x} {org.y} {org.z + 32}')
		# 	quake_map.append(spawn)



def place_radar(quake_map, generator, pos):
	org = to_world(pos)

	# Add the radar wad to worldspawn so it compiles properly
	quake_map.add_wad("radar.wad")

	#
	# Place the radar display prefab
	#
	prefab = prefabs.get("radar")
	prefab_id = make_prefab_unique(prefab)

	# Update the texture name to match the current floor
	for e in prefab:
		for brush in e.brushes:
			for plane in brush.planes:
				plane.texture.name = plane.texture.name.replace("#", f"{abs(pos.z)}")

	quake_map.merge(prefab, org.x, org.y, org.z, 0)

	#
	# Place a trigger brush around the level
	#
	left = 0 - (TILE_SIZE / 2)
	right = left + (generator.tilemap.width * TILE_SIZE)
	down = 0 - (TILE_SIZE / 2)
	up = down + (generator.tilemap.height * TILE_SIZE)
	bottom = org.z - TILE_SIZE
	top = org.z + (TILE_SIZE * 2)

	e = Entity()
	setattr(e, "classname", "func_radar_zone")
	setattr(e, "target", f"radar_display_{prefab_id}")

	planes = [
	f"( {left} {up} {top} ) ( {left} {down} {top} ) ( {left} {down} {bottom} ) trigger [ 0 -1 0 0 ] [ 0 0 -1 0 ] 0 1 1",  # left
	f"( {left} {down} {top} ) ( {right} {down} {top} ) ( {right} {down} {bottom} ) trigger [ 1 0 -0 0 ] [ 0 -0 -1 0 ] 0 1 1",  # down
	f"( {left} {down} {bottom} ) ( {right} {up} {bottom} ) ( {left} {up} {bottom} ) trigger [ -1 0 0 0 ] [ -0 -1 -0 0 ] 0 1 1",  # bottom
	f"( {left} {up} {top} ) ( {right} {up} {top} ) ( {left} {down} {top} ) trigger [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1",  # top
	f"( {right} {up} {top} ) ( {left} {up} {top} ) ( {left} {up} {bottom} ) trigger [ -1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1",  # up
	f"( {right} {down} {top} ) ( {right} {up} {top} ) ( {right} {up} {bottom} ) trigger [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1",  # right
	]

	b = Brush()
	for p in planes:
		b.planes.append(parse_plane(p))

	e.brushes.append(b)
	quake_map.append(e)



def place_player_start(quake_map: QuakeMap, generator):
	for room in generator.rooms:
		for x in range(room.width):
			for y in range(room.height):
				z = 0
				origin = Vec(room.x, room.y, room.z) + Vec(x, y, z)

				if generator.level.get(origin) == T.ROOM:
					print("adding info_player_start")
					start = Entity()
					start.classname = "info_player_start"
					start.origin = f"{origin.x * TILE_SIZE} {origin.y * TILE_SIZE} {origin.z * LEVEL_HEIGHT + 32}"
					quake_map.append(start)
					print(start)
					return

	raise RuntimeError



def add_level_entities(quake_map: QuakeMap, generator):
	#
	# Adds "global" or level-wide entities such as surface lights
	#
	pass



if __name__ == '__main__':
	import argparse

	parser = argparse.ArgumentParser(
		description='Create a Quake map file from a generated 3D tilemap'
	)
	parser.add_argument('-w', '--width', type=int, default=17)
	parser.add_argument('--height', type=int, default=17)
	parser.add_argument('-l', '--floors', type=int, default=1)

	args = parser.parse_args()

	main(args.width, args.height, args.floors)
