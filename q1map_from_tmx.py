#!python3
"""Command line utility for creating MAP files from TMX files

Supported Tilemaps:
    - Tiled

Supported Games:
    - QUAKE
"""

from argparse import Action, ArgumentParser
import json
import os
import sys
import time
from pathlib import Path

import numpy
from pytmx import TiledMap, TiledTileLayer, TiledImageLayer, TiledObjectGroup
from mapgen.qmap import QuakeMap, Entity, parse_quakemap

__version__ = '0.7.0'



class ResolvePathAction(Action):
    def __call__(self, parser, namespace, values, option_string=None):
        if isinstance(values, list):
            fullpath = [os.path.expanduser(v) for v in values]
        else:
            fullpath = os.path.expanduser(values)

        setattr(namespace, self.dest, fullpath)



parser = ArgumentParser(
    prog='tmx2map',
    description='Default action is to create a map file from a tmx tilemap',
    epilog='example: tmx2map {0} {1} => creates the map file {2}'.format('e1m1.tmx', 'mapping.json', 'e1m1.map')
)

parser.add_argument('tilemap_file',
                    metavar='file.tmx',
                    action=ResolvePathAction,
                    help='Tiled tilemap file')

parser.add_argument('mapping_file',
                    metavar='mapping.json',
                    action=ResolvePathAction,
                    help='json tile mapping file')

parser.add_argument('-d',
                    metavar='file.map',
                    dest='dest',
                    default='test.map',
                    action=ResolvePathAction,
                    help='name of created map file')

parser.add_argument('-v',
                    '--version',
                    dest='version',
                    action='version',
                    version='%(prog)s {}'.format(__version__),
                    help='display version number')

parser.add_argument('-q',
                    '--quiet',
                    dest='quiet',
                    action='store_true',
                    help='quiet mode')

args = parser.parse_args()

start_time = time.time()
step_timing = [0]


def optional_print(msg=''):
    if not args.quiet:
        print(msg)


def report_error():
    error_type, error_value, error_traceback = sys.exc_info()

    if error_traceback.tb_next:
        error_traceback = error_traceback.tb_next

    filename = os.path.normpath(error_traceback.tb_frame.f_code.co_filename)
    error_message = "ERROR: {}:{}: {}: {}".format(filename,
                                                  error_traceback.tb_lineno,
                                                  error_type.__name__,
                                                  error_value)
    print(error_message, file=sys.stderr)

def record_step_time():
    delta = time.time() - start_time - step_timing[-1]
    step_timing.append(delta)
    optional_print('{:5.4f} seconds'.format(step_timing[-1]))
    optional_print()


optional_print('Loading 2D tilemap...')
# Load the tilemap
tilemap = TiledMap(args.tilemap_file)

if not tilemap:
    print('ERROR: failed to load: {}'.format(os.path.basename(args.tilemap_file)))
    sys.exit(1)

tilesets_2d_found = len(tilemap.tilesets)
optional_print('{} 2D tileset{} found'.format(str(tilesets_2d_found).rjust(6), 's' if tilesets_2d_found > 1 else ''))



# Resolve path to map file
if args.dest == os.getcwd():
    map_path = os.path.dirname(args.tilemap_file)
    map_name = os.path.basename(args.tilemap_file).split('.')[0] + '.map'
    args.dest = os.path.join(map_path, map_name)



# Create the file path if needed
destdir = os.path.dirname(args.dest) or '.'
if not os.path.exists(destdir):
    os.makedirs(destdir)

# Get full path to mappings file
cwd = os.getcwd()
args.mapping_file = os.path.normpath(os.path.join(cwd, args.mapping_file))

width = tilemap.width
height = tilemap.height

record_step_time()

optional_print('Loading 3D tiles...')
try:
    with open(args.mapping_file) as file:
        tile_mapping = json.loads(file.read())
except:
    report_error()
    tile_mapping = None

if not tile_mapping:
    print('ERROR: failed to load: {}'.format(os.path.basename(args.mapping_file)))
    sys.exit(1)

# Loading in the 3D tile data
tiles3d = {}
tile_size_3d = tile_mapping["tilesize"]
tile_size_2d = tilemap.tilewidth

tilemap_width_3d = tilemap.width * tile_size_3d
tilemap_height_3d = tilemap.height * tile_size_3d

if tilemap_width_3d > 8192:
    print('WARNING: Map x dimensions exceeds +-4096 limit.')

if tilemap_height_3d > 8192:
    print('WARNING: Map y dimensions exceeds +-4096 limit.')

tilesets_3d_found = len(tile_mapping["tilesets"])

#
# Load the 3D tiles
#
for tileset in tile_mapping["tilesets"]:
    filename = tileset["filename"]

    # Grab the tileset def from the tilemap
    tileset_definition = [t for t in tilemap.tilesets if os.path.basename(t.source) == filename]
    if not tileset_definition:
        continue

    tileset_definition = tileset_definition[0]
    tile_count = tileset_definition.tilecount
    first_gid = tileset_definition.firstgid

    for tile_id in tileset["tiles"]:
        gid = int(tile_id) + first_gid
        tile_filename = tileset["tiles"][tile_id]

        dirname = os.path.dirname(args.mapping_file)
        tile_filepath = os.path.normpath(os.path.join(dirname, tile_filename))

        if not os.path.exists(tile_filepath):
            print("WARNING: Missing 3d tile: {}".format(tile_filepath))
            continue

        with open(tile_filepath) as file:
            prefab = parse_quakemap(file)

            if hasattr(prefab[0], 'wad'):
                prefab_wads = getattr(prefab[0], 'wad', []).split(';')

                prefab[0].wad = ';'.join(
                    [
                        str(Path(Path(tile_filepath).parent, Path(wad)).resolve()) for
                        wad in prefab_wads
                    ]
                )

            tiles3d[gid] = prefab

optional_print('{} 3D tileset{} found'.format(str(tilesets_3d_found).rjust(6), 's' if tilesets_3d_found > 1 else ''))
optional_print('{} 3D tiles loaded'.format(str(len(tiles3d)).rjust(6)))

record_step_time()

optional_print('Creating map...')
entities = QuakeMap()

for name, value in tilemap.properties.items():
    setattr(entities[0], name, value)

missing_gids = []

tiles_processed = 0
brush_count = 0

for layer_index, layer in enumerate(tilemap.layers):
    layer_offset_z = layer.properties.get('Z Offset', 0)

    if isinstance(layer, TiledTileLayer):
        print(list(layer.tiles()))
        for index, tile in enumerate(layer.tiles()):
            print("tile: ", tile)

            if tile[2] is None:
                continue

            tile_gid = tilemap.get_tile_gid(tile[0], tile[1], layer_index)
            tiled_gid = tilemap.tiledgidmap[tile_gid]

            # Warn if a tile is used in the 2D tilemap, but no mapping exists
            if not tiles3d.get(tiled_gid):
                if tiled_gid not in missing_gids:
                    print("WARNING: Missing tile mapping for gid: {}".format(tiled_gid))
                    missing_gids.append(tiled_gid)

                continue

            tiles_processed += 1

            x = tile[0]
            y = -1 * tile[1]

            tilemap_offset_x = x * tile_size_3d + (tile_size_3d / 2) - (tilemap_width_3d / 2)
            tilemap_offset_y = y * tile_size_3d - (tile_size_3d / 2) + (tilemap_height_3d / 2)

            angle = 0
            hflip, vflip, dflip = tile[2][2]

            # if tile.hflip:
            #     flip_matrix = numpy.dot(flip_matrix, mathhelper.Matrices.horizontal_flip)
            #     flip_face = not flip_face
            if hflip:
                angle = 180

            # if tile.vflip:
            #     flip_matrix = numpy.dot(flip_matrix, mathhelper.Matrices.vertical_flip)
            #     flip_face = not flip_face

            # if tile.dflip:
            #     flip_matrix = numpy.dot(flip_matrix, mathhelper.Matrices.diagonal_flip)
            #     flip_face = not flip_face
            if dflip:
                angle = 270

                if vflip:
                    angle = 90

            entities.merge(tiles3d[tiled_gid], tilemap_offset_x, tilemap_offset_y, layer_offset_z, angle)

    # Object layer
    elif isinstance(layer, TiledObjectGroup):
        for obj in layer:
            object_type = 'Point'

            # if obj.ellipse:
            #     object_type = 'Ellipse'

            # elif obj.polygon:
            #     object_type = 'Polygon'

            # elif obj.polyline:
            #     object_type = 'Polyline'

            # elif obj.width > 0 and obj.height > 0:
            #     object_type = 'Rectangle'

            z = layer_offset_z

            if object_type == 'Point':
                e = Entity()
                for k, v in obj.properties.items():
                    print(f"object property: {k}, {v}")
                    if k == 'Z':
                        z += v

                    else:
                        setattr(e, k, v)



                scale = tile_size_3d / tile_size_2d
                ex = (obj.x * scale) - (tilemap_width_3d / 2)
                ey = (tilemap.height * tile_size_3d) - obj.y * scale - (tilemap_height_3d / 2)
                origin = ex, ey, z
                e.origin = "{} {} {}".format(*origin)
                e.classname = obj.name

                if hasattr(e, 'color'):
                    # convert hexadecimal to rgb
                    e.color = e.color.lstrip('#')
                    e.color = '%s %s %s' % tuple(int(e.color[i:i+2], 16) for i in (0, 2, 4))

                entities.append(e)

            # elif object_type == 'Rectangle':
            #     classname = obj.name

            #     if classname == 'worldspawn':
            #         e = worldspawn

            #     else:
            #         e = m.Entity()

            #     e.classname = obj.name

            #     for prop in obj.properties:
            #         if prop.name == 'Z':
            #             z = prop.value

            #         else:
            #             setattr(e, prop.name, prop.value)

            #     scale = tile_size_3d / tile_size_2d
            #     ex = (obj.x * scale) - (tilemap_width_3d / 2)
            #     ey = (tilemap.height * tile_size_3d) - obj.y * scale - (tilemap_height_3d / 2)
            #     origin = ex, ey, z

            #     width = obj.width * scale
            #     height = obj.height * scale

            #     left = 0
            #     right = left + width
            #     up = 0
            #     down = up - height
            #     top = 4096
            #     bottom = -4096

            #     mat = mathhelper.Matrices.translation_matrix(ex, ey)
            #     mat = numpy.dot(
            #         mat,
            #         mathhelper.Matrices.rotation_matrix(-obj.rotation)
            #     )

            #     def xform_point(point):
            #         result = numpy.dot(mat, (*point, 1))
            #         return tuple(result.tolist()[:3])

            #     texture = 'trigger'
            #     if hasattr(e, 'texture'):
            #         texture = e.texture

            #     # Create brush
            #     b = m.Brush()
            #     b.planes = []

            #     # Top
            #     p = m.Plane()
            #     p.texture_name = texture
            #     p.offset = 0, 0
            #     p.rotation = 0
            #     p.scale = 1, 1
            #     p.points = (left, up, top), (right, up, top), (left, down, top)
            #     p.points = tuple(map(xform_point, p.points))
            #     b.planes.append(p)

            #     # Bottom
            #     p = m.Plane()
            #     p.texture_name = texture
            #     p.offset = 0, 0
            #     p.rotation = 0
            #     p.scale = 1, 1
            #     p.points = (left, down, bottom), (right, up, bottom), (left, up, bottom)
            #     p.points = tuple(map(xform_point, p.points))
            #     b.planes.append(p)

            #     # Right
            #     p = m.Plane()
            #     p.texture_name = texture
            #     p.offset = 0, 0
            #     p.rotation = 0
            #     p.scale = 1, 1
            #     p.points = (right, down, top), (right, up, top), (right, up, bottom)
            #     p.points = tuple(map(xform_point, p.points))
            #     b.planes.append(p)

            #     # Left
            #     p = m.Plane()
            #     p.texture_name = texture
            #     p.offset = 0, 0
            #     p.rotation = 0
            #     p.scale = 1, 1
            #     p.points = (left, up, top), (left, down, top), (left, down, bottom)
            #     p.points = tuple(map(xform_point, p.points))
            #     b.planes.append(p)

            #     # Up
            #     p = m.Plane()
            #     p.texture_name = texture
            #     p.offset = 0, 0
            #     p.rotation = 0
            #     p.scale = 1, 1
            #     p.points = (right, up, top), (left, up, top), (left, up, bottom)
            #     p.points = tuple(map(xform_point, p.points))
            #     b.planes.append(p)

            #     # Down
            #     p = m.Plane()
            #     p.texture_name = texture
            #     p.offset = 0, 0
            #     p.rotation = 0
            #     p.scale = 1, 1
            #     p.points = (left, down, top), (right, down, top), (right, down, bottom)
            #     p.points = tuple(map(xform_point, p.points))
            #     b.planes.append(p)

            #     e.brushes.append(b)

            #   if classname != 'worldspawn':
            #       entities.append(e)

optional_print('{} 2d tiles processed'.format(str(tiles_processed).rjust(6)))
record_step_time()

optional_print('Saving: {}...'.format(os.path.basename(args.dest)))
with open(args.dest, 'w') as out_file:
    out_file.write(str(entities))

optional_print('{} brushes written'.format(str(brush_count).rjust(6)))
record_step_time()

optional_print('Complete!')
optional_print('{:5.4f} total seconds elapsed'.format(sum(step_timing)))
