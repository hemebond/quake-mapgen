#
# https://github.com/AtTheMatinee/dungeon-generation/blob/master/dungeonGenerationAlgorithms.py
#

import tcod
from tcod import libtcodpy
import random



DOOR_TILE = 32
WALL_TILE = 1
WALL_COLOUR = (128, 128, 128)
FLOOR_TILE = 0
FLOOR_COLOUR = (64, 64, 64)
ROOM_COLOURS = [
 	(124, 252, 0),
  	(127, 255, 0),
  	(173, 255, 47),
  	(0, 100, 0),
  	(0, 128, 0),
  	(34, 139, 34),
  	(0, 255, 0),
  	(50, 205, 50),
  	(144, 238, 144),
  	(152, 251, 152),
  	(143, 188, 143),
  	(0, 250, 154),
  	(0, 255, 127),
  	(46, 139, 87),
  	(102, 205, 170),
  	(60, 179, 113),
  	(32, 178, 170),
  	(47, 79, 79),
  	(0, 128, 128),
  	(0, 139, 139),
  	(0, 255, 255),
  	(0, 255, 255),
  	(224, 255, 255),
  	(0, 206, 209),
  	(64, 224, 208),
  	(72, 209, 204),
  	(175, 238, 238),
  	(127, 255, 212),
  	(176, 224, 230),
  	(95, 158, 160),
  	(70, 130, 180),
  	(100, 149, 237),
  	(0, 191, 255),
  	(30, 144, 255),
  	(173, 216, 230),
  	(135, 206, 235),
  	(135, 206, 250),
]



def print_room(room):
	for row in room:
		output = ''
		for tile in row:
			output += '#' if is_wall(tile) else '.'
		print(output)



class Prefab:
	pass


class AsciiPrefabParser:
	def __init__(self):
		self.prefabs = []



class TemplateGenerator:
	def get_prefab_from_group(self, group_name) -> Prefab:
		"""
		Returns a random prefabs from a particular group
		"""
		pass

	def get_prefab(self) -> Prefab:
		"""
		Returns a random prefab
		"""
		pass



class RoomAdditionGenerator:
	'''
	What I'm calling the Room Addition algorithm is an attempt to
	recreate the dungeon generation algorithm used in Brogue, as
	discussed at https://www.rockpapershotgun.com/2015/07/28/how-do-roguelikes-generate-levels/
	I don't think Brian Walker has ever given a name to his
	dungeon generation algorithm, so I've taken to calling it the
	Room Addition Algorithm, after the way in which it builds the
	dungeon by adding rooms one at a time to the existing dungeon.
	This isn't a perfect recreation of Brian Walker's algorithm,
	but I think it's good enough to demonstrait the concept.
	'''

	def __init__(self, logger=None):
		self.logger = logger

		self.level = []

		self.ROOM_MAX_SIZE = 18  # max height and width for cellular automata rooms
		self.ROOM_MIN_SIZE = 16  # min size in number of floor tiles, not height and width
		self.MAX_NUM_ROOMS = 30

		self.SQUARE_ROOM_MAX_SIZE = 12
		self.SQUARE_ROOM_MIN_SIZE = 6

		self.CROSS_ROOM_MAX_SIZE = 12
		self.CROSS_ROOM_MIN_SIZE = 6

		self.cavernChance = 0.4  # probability that the first room will be a cavern
		self.CAVERN_MAX_SIZE = 35  # max height an width

		self.wallProbability = 0.45
		self.neighbors = 4

		self.squareRoomChance = 0.2
		self.crossRoomChance = 0.15

		self.buildRoomAttempts = 500
		self.placeRoomAttempts = 20
		self.maxTunnelLength = 12

		self.includeShortcuts = True
		self.shortcutAttempts = 500
		self.shortcutLength = 5
		self.minPathfindingDistance = 50

	def generateLevel(self, mapWidth, mapHeight):
		self.rooms = []

		self.level = [[WALL_TILE
			for y in range(mapHeight)]
				for x in range(mapWidth)]

		# generate the first room
		room = self.generateRoom()
		roomWidth, roomHeight = self.getRoomDimensions(room)
		roomX = int((mapWidth / 2 - roomWidth / 2) - 1)
		roomY = int((mapHeight / 2 - roomHeight / 2) - 1)
		self.addRoom(roomX, roomY, room)

		# generate other rooms
		for i in range(self.buildRoomAttempts):
			room = self.generateRoom()
			# try to position the room, get roomX and roomY
			roomX, roomY, wallTile, direction, tunnelLength = self.placeRoom(room, mapWidth, mapHeight)
			if roomX and roomY:
				self.addRoom(roomX, roomY, room)
				self.addTunnel(wallTile, direction, tunnelLength)
				if len(self.rooms) >= self.MAX_NUM_ROOMS:
					break

			self.logger.print_level(self.level)

		self.logger.print_level(self.level)

		if self.includeShortcuts is True:
			self.addShortcuts(mapWidth, mapHeight)

		return self.level

	def generateRoom(self):
		# select a room type to generate
		# generate and return that room
		if self.rooms:
			# There is at least one room already
			choice = random.random()

			if choice < self.squareRoomChance:
				room = self.generateRoomSquare()
			elif self.squareRoomChance <= choice < (self.squareRoomChance + self.crossRoomChance):
				room = self.generateRoomCross()
			else:
				room = self.generateRoomCellularAutomata()

		else:  # it's the first room
			choice = random.random()
			if choice < self.cavernChance:
				room = self.generateRoomCavern()
			else:
				room = self.generateRoomSquare()

		# Give the room floor tiles the room number/id
		roomWidth, roomHeight = self.getRoomDimensions(room)
		room_idx = 2 + len(self.rooms)
		for x in range(roomWidth):
			for y in range(roomHeight):
				if is_floor(room[x][y]):
					room[x][y] = room_idx

		return room

	def generateRoomCross(self):
		roomHorWidth = int((random.randint(self.CROSS_ROOM_MIN_SIZE+2, self.CROSS_ROOM_MAX_SIZE))/2*2)
		roomVirHeight = int((random.randint(self.CROSS_ROOM_MIN_SIZE+2, self.CROSS_ROOM_MAX_SIZE))/2*2)
		roomHorHeight = int((random.randint(self.CROSS_ROOM_MIN_SIZE, roomVirHeight-2))/2*2)
		roomVirWidth = int((random.randint(self.CROSS_ROOM_MIN_SIZE, roomHorWidth-2))/2*2)

		room = [[WALL_TILE
			for y in range(roomVirHeight)]
				for x in range(roomHorWidth)]

		# Fill in horizontal space
		virOffset = int(roomVirHeight / 2 - roomHorHeight / 2)
		for y in range(virOffset, roomHorHeight + virOffset):
			for x in range(0, roomHorWidth):
				room[x][y] = FLOOR_TILE

		# Fill in virtical space
		horOffset = int(roomHorWidth / 2 - roomVirWidth / 2)
		for y in range(0, roomVirHeight):
			for x in range(horOffset, roomVirWidth + horOffset):
				room[x][y] = FLOOR_TILE

		return room

	def generateRoomSquare(self):
		roomWidth = random.randint(self.SQUARE_ROOM_MIN_SIZE, self.SQUARE_ROOM_MAX_SIZE)
		roomHeight = random.randint(
			max(int(roomWidth * 1.0), self.SQUARE_ROOM_MIN_SIZE),
			min(int(roomWidth * 1.0), self.SQUARE_ROOM_MAX_SIZE)
		)

		room = [[WALL_TILE
			for y in range(roomHeight)]
				for x in range(roomWidth)]

		room = [[FLOOR_TILE
			for y in range(1, roomHeight - 1)]
				for x in range(1, roomWidth - 1)]

		return room

	def generateRoomCellularAutomata(self):
		while True:
			# if a room is too small, generate another
			room = [[WALL_TILE
				for y in range(self.ROOM_MAX_SIZE)]
					for x in range(self.ROOM_MAX_SIZE)]

			# random fill map
			for y in range(2, self.ROOM_MAX_SIZE-2):
				for x in range(2, self.ROOM_MAX_SIZE-2):
					if random.random() >= self.wallProbability:
						room[x][y] = FLOOR_TILE

			# create distinctive regions
			for i in range(4):
				for y in range(1, self.ROOM_MAX_SIZE-1):
					for x in range(1, self.ROOM_MAX_SIZE-1):

						# if the cell's neighboring walls > self.neighbors, set it to 1
						if self.getAdjacentWalls(x, y, room) > self.neighbors:
							room[x][y] = WALL_TILE
						# otherwise, set it to 0
						elif self.getAdjacentWalls(x, y, room) < self.neighbors:
							room[x][y] = FLOOR_TILE

			# floodfill to remove small caverns
			room = self.floodFill(room)

			# start over if the room is completely filled in
			roomWidth, roomHeight = self.getRoomDimensions(room)
			for x in range(roomWidth):
				for y in range(roomHeight):
					if is_floor(room[x][y]):
						return room

	def generateRoomCavern(self):
		while True:
			# if a room is too small, generate another
			room = [[WALL_TILE
				for y in range(self.CAVERN_MAX_SIZE)]
					for x in range(self.CAVERN_MAX_SIZE)]

			# random fill map
			for y in range(2, self.CAVERN_MAX_SIZE-2):
				for x in range(2, self.CAVERN_MAX_SIZE-2):
					if random.random() >= self.wallProbability:
						room[x][y] = FLOOR_TILE

			# create distinctive regions
			for i in range(4):
				for y in range(1, self.CAVERN_MAX_SIZE - 1):
					for x in range(1, self.CAVERN_MAX_SIZE - 1):

						# if the cell's neighboring walls > self.neighbors, set it to 1
						if self.getAdjacentWalls(x, y, room) > self.neighbors:
							room[x][y] = WALL_TILE
						# otherwise, set it to 0
						elif self.getAdjacentWalls(x, y, room) < self.neighbors:
							room[x][y] = FLOOR_TILE

			# floodfill to remove small caverns
			room = self.floodFill(room)

			# start over if the room is completely filled in
			roomWidth, roomHeight = self.getRoomDimensions(room)
			for x in range(roomWidth):
				for y in range(roomHeight):
					if is_floor(room[x][y]):
						return room

	def floodFill(self, room):
		'''
		Find the largest region. Fill in all other regions.
		'''
		roomWidth, roomHeight = self.getRoomDimensions(room)
		largestRegion = set()

		for x in range(roomWidth):
			for y in range(roomHeight):
				if is_floor(room[x][y]):
					newRegion = set()
					tile = (x,y)
					toBeFilled = set([tile])
					while toBeFilled:
						tile = toBeFilled.pop()

						if tile not in newRegion:
							newRegion.add(tile)

							room[tile[0]][tile[1]] = WALL_TILE

							# check adjacent cells
							x = tile[0]
							y = tile[1]
							north = (x, y - 1)
							south = (x, y + 1)
							east = (x + 1, y)
							west = (x - 1, y)

							for direction in [north, south, east, west]:
								if is_floor(room[direction[0]][direction[1]]):
									if direction not in toBeFilled and direction not in newRegion:
										toBeFilled.add(direction)

					if len(newRegion) >= self.ROOM_MIN_SIZE:
						if len(newRegion) > len(largestRegion):
							largestRegion.clear()
							largestRegion.update(newRegion)

		for tile in largestRegion:
			room[tile[0]][tile[1]] = FLOOR_TILE

		return room

	def placeRoom(self, room, mapWidth, mapHeight):  # (self,room,direction,)
		roomX = None
		roomY = None

		roomWidth, roomHeight = self.getRoomDimensions(room)

		# try n times to find a wall that lets you build room in that direction
		for i in range(self.placeRoomAttempts):
			# try to place the room against the tile, else connected by a tunnel of length i

			wallTile = None
			direction = self.getDirection()
			while wallTile is None:
				'''
				randomly select tiles until you find
				a wall that has another wall in the
				chosen direction and has a floor in the
				opposite direction.
				'''
				# direction == tuple(dx,dy)
				tileX = random.randint(1, mapWidth - 2)
				tileY = random.randint(1, mapHeight - 2)
				if (
					(is_wall(self.level[tileX][tileY])) and
					(is_wall(self.level[tileX + direction[0]][tileY + direction[1]])) and
					(is_floor(self.level[tileX - direction[0]][tileY - direction[1]]))
				):
					wallTile = (tileX, tileY)

			# spawn the room touching wallTile
			startRoomX = None
			startRoomY = None
			'''
			TODO: replace this with a method that returns a
			random floor tile instead of the top left floor tile
			'''
			while startRoomX is None and startRoomY is None:
				x = random.randint(0, roomWidth - 1)
				y = random.randint(0, roomHeight - 1)
				if is_floor(room[x][y]):
					startRoomX = wallTile[0] - x
					startRoomY = wallTile[1] - y

			# then slide it until it doesn't touch anything
			for tunnelLength in range(self.maxTunnelLength):
				possibleRoomX = startRoomX + direction[0] * tunnelLength
				possibleRoomY = startRoomY + direction[1] * tunnelLength

				enoughRoom = self.getOverlap(room, possibleRoomX, possibleRoomY, mapWidth, mapHeight)

				if enoughRoom:
					roomX = possibleRoomX
					roomY = possibleRoomY

					# build connecting tunnel
					#Attempt 1
					'''
					for i in range(tunnelLength+1):
						x = wallTile[0] + direction[0]*i
						y = wallTile[1] + direction[1]*i
						self.level[x][y] = 0
					'''
					# moved tunnel code into self.generateLevel()

					return roomX, roomY, wallTile, direction, tunnelLength

		return None, None, None, None, None

	def addRoom(self, roomX, roomY, room):
		roomWidth, roomHeight = self.getRoomDimensions(room)
		for x in range(roomWidth):
			for y in range(roomHeight):
				if is_floor(room[x][y]):
					self.level[int(roomX) + x][int(roomY) + y] = room[x][y]

		self.rooms.append(room)

	def addTunnel(self, wallTile, direction, tunnelLength):
		# carve a tunnel from a point in the room back to
		# the wall tile that was used in its original placement

		startX = wallTile[0] + direction[0] * tunnelLength
		startY = wallTile[1] + direction[1] * tunnelLength
		# self.level[startX][startY] = 1

		for i in range(self.maxTunnelLength):
			x = startX - direction[0] * i
			y = startY - direction[1] * i

			try:
				if is_wall(self.level[x][y]):
					self.level[x][y] = FLOOR_TILE
			except IndexError:
				return

			# # If you want doors, this is where the code should go
			# if (
			# 	(x + direction[0]) == wallTile[0] and
			# 	(y + direction[1]) == wallTile[1]
			# ):
			# 	# self.level[x + direction[0]][y + direction[1]] = DOOR_TILE
			# 	px = x + direction[0]
			# 	py = y + direction[1]

			# 	try:
			# 		# Check the tiles either "side" of the tunnel start
			# 		if (
			# 			is_wall(self.level[px + direction[1]][py + direction[0]]) and
			# 			is_wall(self.level[px - direction[1]][py - direction[0]])
			# 		):
			# 			self.level[px][py] = DOOR_TILE
			# 	except IndexError:
			# 		pass

	def getRoomDimensions(self, room) -> tuple:
		if room:
			roomWidth = len(room)
			roomHeight = len(room[0])
			return roomWidth, roomHeight
		else:
			roomWidth = 0
			roomHeight = 0
			return roomWidth, roomHeight

	def getAdjacentWalls(self, tileX, tileY, room):  # finds the walls in 8 directions
		wallCounter = 0
		for x in range(tileX - 1, tileX + 2):
			for y in range(tileY - 1, tileY + 2):
				if is_wall(room[x][y]):
					if (x != tileX) or (y != tileY):  # exclude (tileX,tileY)
						wallCounter += 1
		return wallCounter

	def getDirection(self):
		# direction = (dx,dy)
		north = (0, -1)
		south = (0, 1)
		east = (1, 0)
		west = (-1, 0)

		direction = random.choice([north, south, east, west])
		return direction

	def getOverlap(self, room, roomX, roomY, mapWidth, mapHeight):
		'''
		for each 0 in room, check the cooresponding tile in
		self.level and the eight tiles around it. Though slow,
		that should insure that there is a wall between each of
		the rooms created in this way.
		<> check for overlap with self.level
		<> check for out of bounds
		'''
		roomWidth, roomHeight = self.getRoomDimensions(room)
		for x in range(roomWidth):
			for y in range(roomHeight):
				if is_floor(room[x][y]):
					# Check to see if the room is out of bounds
					if ((1 <= (x + roomX) < mapWidth - 1) and (1 <= (y + roomY) < mapHeight - 1)):
						# Check for overlap with a one tile buffer
						if is_floor(self.level[x + roomX - 1][y + roomY - 1]):  # top left
							return False
						if is_floor(self.level[x + roomX][y + roomY - 1]):  # top center
							return False
						if is_floor(self.level[x + roomX + 1][y + roomY - 1]):  # top right
							return False

						if is_floor(self.level[x + roomX - 1][y + roomY]):  # left
							return False
						if is_floor(self.level[x + roomX][y + roomY]):  # center
							return False
						if is_floor(self.level[x + roomX + 1][y + roomY]):  # right
							return False

						if is_floor(self.level[x + roomX - 1][y + roomY + 1]):  # bottom left
							return False
						if is_floor(self.level[x + roomX][y + roomY + 1]):  # bottom center
							return False
						if is_floor(self.level[x + roomX + 1][y + roomY + 1]):  # bottom right
							return False

					else:  # room is out of bounds
						return False
		return True

	def addShortcuts(self, mapWidth, mapHeight):
		'''
		I use libtcodpy's built in pathfinding here, since I'm
		already using libtcodpy for the iu. At the moment,
		the way I find the distance between
		two points to see if I should put a shortcut there
		is horrible, and its easily the slowest part of this
		algorithm. If I think of a better way to do this in
		the future, I'll implement it.
		'''

		# initialize the libtcodpy map
		libtcodMap = tcod.map.Map(mapWidth, mapHeight)
		self.recomputePathMap(mapWidth, mapHeight, libtcodMap)

		for i in range(self.shortcutAttempts):
			# check i times for places where shortcuts can be made
			while True:
				# Pick a random floor tile
				floorX = random.randint(self.shortcutLength + 1, (mapWidth - self.shortcutLength - 1))
				floorY = random.randint(self.shortcutLength + 1, (mapHeight - self.shortcutLength - 1))
				if is_floor(self.level[floorX][floorY]):
					if (
						is_wall(self.level[floorX - 1][floorY]) or
						is_wall(self.level[floorX + 1][floorY]) or
						is_wall(self.level[floorX][floorY - 1]) or
						is_wall(self.level[floorX][floorY + 1])
					):
						break

			# look around the tile for other floor tiles
			for x in range(-1, 2):
				for y in range(-1, 2):
					if x != 0 or y != 0:  # Exclude the center tile
						newX = floorX + (x * self.shortcutLength)
						newY = floorY + (y * self.shortcutLength)
						if is_floor(self.level[newX][newY]):
							# run pathfinding algorithm between the two points
							# back to the libtcodpy nonesense
							pathMap = libtcodpy.path_new_using_map(libtcodMap)
							libtcodpy.path_compute(pathMap, floorX, floorY, newX, newY)
							distance = libtcodpy.path_size(pathMap)

							if distance > self.minPathfindingDistance:
								# make shortcut
								self.carveShortcut(floorX, floorY, newX, newY)
								self.recomputePathMap(mapWidth, mapHeight, libtcodMap)

	def recomputePathMap(self, mapWidth, mapHeight, libtcodMap):
		for x in range(mapWidth):
			for y in range(mapHeight):
				if is_wall(self.level[x][y]):
					# tcod.map_set_properties(libtcodMap,x,y,False,False)
					libtcodMap.transparent[x, y] = False
					libtcodMap.walkable[x, y] = False
				else:
					# tcod.map_set_properties(libtcodMap,x,y,True,True)
					libtcodMap.transparent[x, y] = True
					libtcodMap.walkable[x, y] = True

	def carveShortcut(self, x1, y1, x2, y2):
		if x1 - x2 == 0:
			# Carve vertical tunnel
			for y in range(min(y1, y2), max(y1, y2) + 1):
				if is_wall(self.level[x1][y]):
					self.level[x1][y] = FLOOR_TILE

		elif y1 - y2 == 0:
			# Carve horizontal tunnel
			for x in range(min(x1, x2), max(x1, x2) + 1):
				if is_wall(self.level[x][y1]):
					self.level[x][y1] = FLOOR_TILE

		elif (y1 - y2) / (x1 - x2) == 1:
			# Carve NW to SE Tunnel
			x = min(x1, x2)
			y = min(y1, y2)
			while x != max(x1, x2):
				x += 1
				if is_wall(self.level[x][y]):
					self.level[x][y] = FLOOR_TILE
				y += 1
				if is_wall(self.level[x][y]):
					self.level[x][y] = FLOOR_TILE

		elif (y1 - y2) / (x1 - x2) == -1:
			# Carve NE to SW Tunnel
			x = min(x1, x2)
			y = max(y1, y2)
			while x != max(x1, x2):
				x += 1
				if is_wall(self.level[x][y]):
					self.level[x][y] = FLOOR_TILE
				y -= 1
				if is_wall(self.level[x][y]):
					self.level[x][y] = FLOOR_TILE

	def checkRoomExists(self, room):
		roomWidth, roomHeight = self.getRoomDimensions(room)
		for x in range(roomWidth):
			for y in range(roomHeight):
				if is_floor(room[x][y]):
					return True
		return False



def is_wall(value):
	# Placeholder for more advanced logic
	return value == WALL_TILE



def is_floor(value):
	# Placeholder for more advanced logic
	return not is_wall(value)



def is_door(value):
	return value == DOOR_TILE



class TcodLogger:
	def __init__(self, context, console):
		self.context = context
		self.console = console

	def print_level(self, level):
		self.console.clear()

		total_room_colours = len(ROOM_COLOURS)

		for y in range(self.console.height):
			for x in range(self.console.width):
				cell = level[x][y]

				if is_wall(cell):
					char = ' '
					fg = WALL_COLOUR
					bg = WALL_COLOUR
				if is_floor(cell):
					char = ' '
					fg = WALL_COLOUR
					bg = FLOOR_COLOUR
				if cell > 2:
					char = ' '
					fg = WALL_COLOUR
					bg = ROOM_COLOURS[cell % total_room_colours]

				self.console.print(
					x=x,
					y=y,
					string=char,
					fg=fg,
					bg=bg,
				)

		self.context.present(self.console)



class World:
	pass



if __name__ == "__main__":
	WIDTH = HEIGHT = int(4096 * 2 / 128)  # max size for a BSP1 quake map is +-4096u with cell size of 128u
	# WIDTH, HEIGHT = 48, 48  # tcod console width and height in tiles.

	# Load the font, a 32 by 8 tile font with libtcod's old character layout.
	tileset = tcod.tileset.load_tilesheet(
		"dejavu10x10_gs_tc.png", 32, 8, tcod.tileset.CHARMAP_TCOD,
	)
	# Create the main console.
	console = tcod.console.Console(WIDTH, HEIGHT, order="F")
	# Create a window based on this console and tileset.
	with tcod.context.new(  # New window for a console of size columns×rows.
		columns=console.width, rows=console.height, tileset=tileset,
	) as context:
		logger = TcodLogger(context, console)
		generator = RoomAdditionGenerator(logger=logger)
		level = generator.generateLevel(WIDTH, HEIGHT)
		logger.print_level(level)
		print(level)

		while True:  # Main loop, runs until SystemExit is raised.
			# console.clear()
			# console.print(x=0, y=0, string="Hello World!")
			# context.present(console)  # Show the console.

			# This event loop will wait until at least one event is processed before exiting.
			# For a non-blocking event loop replace `tcod.event.wait` with `tcod.event.get`.
			for event in tcod.event.wait():
				# context.convert_event(event)  # Sets tile coordinates for mouse events.
				# print(event)  # Print event names and attributes.
				if isinstance(event, tcod.event.Quit):
					raise SystemExit()
		# The window will be closed after the above with-block exits.
