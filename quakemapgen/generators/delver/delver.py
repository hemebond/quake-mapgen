from random import randint

class Tile:
	def __init__(self, typ: str, rot: int, exits: int):
		self.type = typ
		self.rot = rot
		self.exits = exits

		self.exit_left = False
		self.exit_right = False
		self.exit_top = False
		self.exit_bottom = False

		if typ in ['start', 'end', 'beginning', 'finish']:
			self.exit_top = True
		elif typ == 'hall':
			self.exit_top = True
			self.exit_bottom = True
		elif typ == 'intersection':
			self.exit_left = True
			self.exit_right = True
			self.exit_top = True
			self.exit_bottom = True
		elif typ == 'corner':
			self.exit_top = True
			self.exit_right = True
		elif typ == 'tri_intersection':
			self.exit_top = True
			self.exit_right = True
			self.exit_bottom = True

		self.rotate(rot)

	def rotate(self, times: int):
		for i in range(times):
			swap_top = self.exit_top
			swap_right = self.exit_right
			swap_bottom = self.exit_bottom
			swap_left = self.exit_left

			self.exit_top = swap_left
			self.exit_right = swap_top
			self.exit_bottom = swap_right
			self.exit_left = swap_bottom

	def can_connect(other_tile, dir: int):
		if dir == 0 and self.exit_left and other_tile.exit_right:
			return True
		if dir == 1 and self.exit_top and other_tile.exit_bottom:
			return True
		if dir == 2 and self.exit_right and other_tile.exit_left:
			return True
		if dir == 3 and self.exit_bottom and other_tile.exit_top:
			return True

		return False


	def __repr__(self):
		return "<%s with %i exits>" % (self.type, self.exits)


class Level:
	def __init__(self, width: int, height: int):
		self.width = width
		self.height = height
		self.tiles = [] # a list of available tile pieces
		self.entities = []


class DungeonGenerator:
	def make_dungeon(self, map_size: int):
		self.map_size = map_size
		tiles = {
			'start': ['e1'],
			'corner': ['c1'],
			'hall': ['h1'],
			'intersection': ['i1'],
			'tri_intersection': ['t1'],
			'end': ['e1'],
		}
		# list of generated tiles
		self.generated_tiles = [None] * (map_size * map_size) # genTiles = new GenTile[MAPSIZE * MAPSIZE];
		self.visited = [None] * (map_size * map_size)
		self.max_complexity = map_size * map_size
		self.current_complexity = 0

		self.make_gen_tiles(tiles, map_size)

		for x in range(map_size):
			for y in range(map_size):
				tile = self.generated_tiles[x + y * map_size]

				if tile is not None:
					tile_entries = tiles.get(tile.type)
					level_tile = None

					generated_room = False

		max_rooms = randint(1, 3)
		gen_rooms = 0
		max_attempts = 100
		gen_attempts = 0

		while gen_rooms < max_rooms and gen_attempts < max_attempts:
			gen_attempts += 1

			tile = self.generated_tiles[randint(0, map_size-1) + randint(0, map_size-1) * map_size]

			# if tile is not None:
			# 	if tile.type in tiles['room']:
			# 		tile.type = tile.type + '_room'
			# 		gen_rooms += 1


	def make_gen_tiles(self, tiles: dict, map_size: int):
		tile_count = 0
		self.current_complexity = 0

		while tile_count <= 3 or self.current_complexity <= 2:
			# print('tile_count: %i, current_complexity: %i' % (tile_count, self.current_complexity))
			for i in range(map_size * map_size):
				self.generated_tiles[i] = None
				self.visited[i] = None

			self.gen_tiles(tiles, map_size)

			# print(self.generated_tiles)

			tile_count = 0
			for i in range(len(self.generated_tiles)):
				if self.generated_tiles[i] is not None:
					tile_count += 1


	def gen_tiles(self, tiles: dict, map_size: int):
		# print('gen_tiles()')
		gen_round = 0

		# add the start tile
		self.gen_tile_at('start', randint(0, 3), 1, randint(0, map_size-1), randint(0, map_size-1))

		while self.do_gen_round(gen_round, map_size):
			gen_round += 1
			# print('gen_round: %s' % gen_round)
			# print(self.generated_tiles)


	def do_gen_round(self, gen_round: int, map_size: int):
		# print('do_gen_round(gen_round=%i)' % gen_round)
		for x in range(self.map_size):
			for y in range(self.map_size):
				vis = self.visited[x + y * self.map_size]

				if vis is not None and vis == False:
					return self.find_tile_for(x, y, gen_round)

		return False


	def gen_tile_at(self, tile_type, rot: int, exits: int, x: int, y: int):
		# print('gen_tile_at(type=%s, rot=%i, exits=%i, x=%i, y=%i)' % (tile_type, rot, exits, x, y))
		t = Tile(tile_type, rot, exits)
		self.generated_tiles[x + y * self.map_size] = t
		self.visited[x + y * self.map_size] = True

		# is this doing anything? Does it just reset the adjacent square?
		if t.exit_left and not self.is_out_of_bounds(x - 1, y) and self.get_visited(x - 1, y) != True:
			self.set_visited(False, x - 1, y)
		if t.exit_top and not self.is_out_of_bounds(x, y - 1) and self.get_visited(x, y - 1) != True:
			self.set_visited(False, x, y - 1)
		if t.exit_right and not self.is_out_of_bounds(x + 1, y) and self.get_visited(x + 1, y) != True:
			self.set_visited(False, x + 1, y)
		if t.exit_bottom and not self.is_out_of_bounds(x, y + 1) and self.get_visited(x, y + 1) != True:
			self.set_visited(False, x, y + 1)

		return t


	def find_tile_for(self, x: int, y: int, gen_round: int):
		# print('find_tile_for(%i, %i, %i)' % (x, y, gen_round))
		can_place_list = [] # list of Tile

		available_tiles = self.get_available_tiles(gen_round)

		for tile_type in available_tiles:
			for i in range(4):
				try_tile = Tile(tile_type, i, 2) # 2 rotations?
				can_place = True

				up = self.get_tile_at(x, y -1)
				down = self.get_tile_at(x, y + 1)
				left = self.get_tile_at(x - 1, y)
				right = self.get_tile_at(x + 1, y)

				if up is not None and up.exit_bottom != try_tile.exit_top:
					can_place = False
				if down is not None and down.exit_top != try_tile.exit_bottom:
					can_place = False
				if left is not None and left.exit_right != try_tile.exit_left:
					can_place = False
				if right is not None and right.exit_left != try_tile.exit_right:
					can_place = False

				if try_tile.exit_top and self.is_out_of_bounds(x, y - 1):
					can_place = False
				elif try_tile.exit_bottom and self.is_out_of_bounds(x, y + 1):
					can_place = False;
				elif try_tile.exit_left and self.is_out_of_bounds(x - 1, y):
					can_place = False;
				elif try_tile.exit_right and self.is_out_of_bounds(x + 1, y):
					can_place = False;

				if can_place:
					can_place_list.append(try_tile)

		if len(can_place_list) > 0:
			gen = can_place_list[randint(0, len(can_place_list) - 1)]
			self.gen_tile_at(gen.type, gen.rot, 2, x, y)

			if gen.type == 'intersection':
				self.current_complexity += 2
			if gen.type == 'tri_intersection':
				self.current_complexity += 1

			return True

		return False


	def is_out_of_bounds(self, x: int, y: int):
		if x < 0 or x >= self.map_size or y < 0 or y >= self.map_size:
			return True

		return False


	def get_available_tiles(self, gen_round: int):
		available_tiles = [] # list of tile types

		available_tiles.append('corner')
		available_tiles.append('hall')

		if self.max_complexity > 0 and self.current_complexity < self.max_complexity:
			available_tiles.append('intersection')
			available_tiles.append('tri_intersection')

		if gen_round > 2:
			available_tiles.append('end')

		return available_tiles


	def get_tile_at(self, x: int, y: int):
		if x >= 0 and x < self.map_size and y >= 0 and y < self.map_size:
			return self.generated_tiles[x + y * self.map_size]

		return None


	def get_visited(self, x: int, y: int):
		if self.visited[x + y * self.map_size] is None:
			return False

		return self.visited[x + y * self.map_size]


	def set_visited(self, val: bool, x: int, y: int):
		if x >= 0 and x < self.map_size and y >= 0 and y < self.map_size:
			self.visited[x + y * self.map_size] = val



if __name__ == '__main__':
	MAP_SIZE = 8
	dg = DungeonGenerator()
	dg.make_dungeon(MAP_SIZE)
	print(dg)

	symbols = {
		'0111': '┳',
		'1101': '┻',
		'1111': '╋',
		'0001': '╸',
		'0100': '╺',
		'0010': '╻',
		'1000': '╹',
		'0101': '━',
		'1010': '┃',
		'0110': '┏',
		'0011': '┓',
		'1100': '┗',
		'1001': '┛',
		'1110': '┣',
		'1011': '┫',
	}

	class clr:
	    HEADER = '\033[95m'
	    OKBLUE = '\033[94m'
	    OKGREEN = '\033[92m'
	    WARNING = '\033[93m'
	    FAIL = '\033[91m'
	    ENDC = '\033[0m'
	    BOLD = '\033[1m'
	    UNDERLINE = '\033[4m'

	for y in range(MAP_SIZE):
		r = ''
		for x in range(MAP_SIZE):
			t = dg.generated_tiles[x + y * MAP_SIZE]

			if t is not None:
				s = ''

				s += '1' if t.exit_top else '0'
				s += '1' if t.exit_right else '0'
				s += '1' if t.exit_bottom else '0'
				s += '1' if t.exit_left else '0'


				if t.type == 'start':
					r += f'{clr.OKGREEN}{str(symbols[s])}{clr.ENDC}'
				else:
					r += str(symbols[s])

			else:
				r += ' '
		print(r)
