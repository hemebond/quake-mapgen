# the Nuclear Throne / kebabskal digger

# 1. dig in random directions
# 2. remove dead ends
# 3. one node at a time, expand a room around it for ~6 iterations, but the column/row _must_ contain a node

from random import random, randint
# from tmxlib.map import Map
# from tmxlib.layer import TileLayer
# from tmxlib.tileset import ImageTileset, Tileset


MAP_SIZE = 32
TILE_WIDTH = 16
TILE_HEIGHT = 16
# tm = Map((MAP_SIZE, MAP_SIZE), (TILE_WIDTH, TILE_HEIGHT))
# ts = ImageTileset.open('../tilesets/prototype.tsx')
# ts.source = '../tilesets/prototype.tsx'
# # to = tmxlib.ObjectLayer(tm, "entities")
# tl = TileLayer(tm, "default")

UP = 1
RT = 2
DN = 4
LT = 8



def direction_to_vector(direction):
	v = Vec(0, 0)

	if direction & UP:
		v += Vec(0, -1)
	elif direction & DN:
		v += Vec(0,  1)

	if direction & RT:
		v += Vec( 1, 0)
	elif direction & LT:
		v += Vec(-1, 0)

	return v


class Vec:
	def __init__(self, x:int, y:int):
		self.x = x
		self.y = y

	def __add__(self, vec2):
		return Vec(self.x + vec2.x, self.y + vec2.y)



class Digger:
	def __init__(self, x:int, y:int):
		self.pos = Vec(x, y)



class Node:
	def __init__(self, x:int, y:int, connections:int):
		self.x = x
		self.y = y
		self.connections = connections



if __name__ == '__main__':
	nodes = []

	m = ['█'] * (MAP_SIZE * MAP_SIZE)

	# place the digger somewhere on the map
	digger = Vec(int(random() * MAP_SIZE), int(random() * MAP_SIZE))
	m[digger.x + digger.y * MAP_SIZE] = ' '

	for i in range(200):
		direction = [UP, DN, LT, RT][int(random() * 4)]
		# print(direction)
		# print(direction_to_vector(direction))
		new_pos = digger + direction_to_vector(direction)

		if new_pos.x < 0 or new_pos.x >= MAP_SIZE or new_pos.y < 0 or new_pos.y >= MAP_SIZE:
			continue

		digger = new_pos
		m[digger.x + digger.y * MAP_SIZE] = ' '

	# tm.layers.append(tl)
	# tm.layers.append(to)
	# tm.properties['wad'] = "prefabs/gfx/prototype.wad"

	# tmx_filename = 'test.tmx'
	# if sys.argv[1]:
	# 	tmx_filename = sys.argv[1] + '.tmx'

	# tm.save(tmx_filename)

	for y in range(MAP_SIZE):
		output = ''

		for x in range(MAP_SIZE):
			output += m[x + y * MAP_SIZE]

		print(output)
