from random import random, randint, shuffle
from types import SimpleNamespace

# http://journal.stuffwithstuff.com/2014/12/21/rooms-and-mazes/
# https://github.com/munificent/hauberk/blob/db360d9efa714efb6d937c31953ef849c7394a39/lib/src/content/dungeon.dart

class bcolors:
	HEADER = '\033[95m'
	OKBLUE = '\033[94m'
	OKGREEN = '\033[92m'
	WARNING = '\033[93m'
	FAIL = '\033[91m'
	ENDC = '\033[0m'
	BOLD = '\033[1m'
	UNDERLINE = '\033[4m'

NORTH = 0
EAST  = 1
SOUTH = 2
WEST  = 4

DIRECTIONS = [(0,-1), (1,0), (0,1), (-1,0)]

# tuples of width, height, exits
rooms = [
	(3,4,SOUTH)
]

regions = [] # 2d list

def generate(map_size):
	# fill the map with # as walls
	map = SimpleNamespace(
		cells=[['#' for i in range(map_size)] for j in range(map_size)],
		width=map_size,
		height=map_size,
	)

	rooms = []
	add_rooms(map, map_size, rooms)

	# carve the rooms out of the map
	for room in rooms:
		print(room)
		for w in range(room.width):
			for h in range(room.height):
				map.cells[room.x + w][room.y + h] = ' '

	for y in range(map_size):
		print(''.join(map.cells[y]))
	print('')

	# Fill in all of the empty space with mazes.
	for y in range(1, map_size, 2):
		for x in range(1, map_size, 2):
			if map.cells[x][y] != '#':
				continue
			grow_maze(map, map_size, x, y)

	# connect_regions(map, map_size)

	return map



def connect_regions(map, map_size):
	# find all the tiles that can connect two or more regions
	connector_regions = {}

	for x in range(map_size):
		for y in range(map_size):
			if map.cells[x][y] != '#':
				continue

			regions = []


def grow_maze(map, map_size, x, y):
	print('grow_maze(map, map_size, %i, %i)' % (x, y))
	c = '.'
	cells = []
	last_dir = None # this stores the vector of the last direction we moved

	map.cells[x][y] = c # carve

	cells.append((x,y))

	while len(cells) > 0:
		cell = cells[-1] # last cell in the list
		x, y = cell
		unmade_cells = []

		for d in DIRECTIONS:
			if can_carve(map, (x,y), d):
				unmade_cells.append(d)

		print(unmade_cells)

		if len(unmade_cells) > 0:
			if last_dir in unmade_cells and random() > 0.5:
				d = last_dir
			else:
				shuffle(unmade_cells)
				d = unmade_cells[0]

			map.cells[x + d[0]][y + d[1]] = c
			map.cells[x + (d[0] * 2)][y + (d[1] * 2)] = c

			cells.append((x + (d[0] * 2), y + (d[1] * 2)))
			last_dir = d
		else:
			cells = cells[:-1] # remove the last cell?
			last_dir = None



def can_carve(map, position, direction):
	print('can_carve(%s, %s)' % (position, direction))
	# position and direction are (0, 1), (0, -1), etc...

	destination = (position[0] + (direction[0] * 3), position[1] + (direction[1] * 3))

	if destination[0] <= 0 or destination[0] >= map.width:
		return False
	if destination[1] <= 0 or destination[1] >= map.height:
		return False

	# check that we don't open a wall to other cell
	destination = (position[0] + (direction[0] * 2), position[1] + (direction[1] * 2))

	return map.cells[destination[0]][destination[1]] == '#'



def add_rooms(map, map_size, rooms):
	num_room_tries = 10
	for i in range(num_room_tries):
		size = randint(3, 3)
		# rectangularity = randint(0, 1 + int(size / 2)) * 2
		width = size
		height = size

		# 9 - 3 = 6                 # 9 - 4 = 5
		# 6 / 2 = 3 randint(0, 3)   # 5 / 2 = 2 randint(0, 2)
		# 3 * 2 = 6                 # 2 * 2 = 4
		# 6 + 1 = 7                 # 4 + 1 = 5
		x = randint(1, int((map_size - 1 - width) / 2)) * 2 + 1
		y = randint(1, int((map_size - 1 - height) / 2)) * 2 + 1

		room = SimpleNamespace(
			x=x,
			y=y,
			width=width,
			height=height,
		)

		overlaps = False
		for other in rooms:
			if distance_to(room, other) <= 0:
				overlaps = True
				break

		if overlaps:
			continue

		rooms.append(room)

	return rooms



def distance_to(tile, other):
	if tile.y >= (other.y + other.height):
		vertical = tile.y - (other.y + other.height)
	elif (tile.y + tile.height) <= other.y:
		vertical = other.y - (tile.y + tile.height)
	else:
		vertical = -1

	if tile.x >= (other.x + other.width):
		horizontal = tile.x - (other.x + other.width)
	elif (tile.x + tile.width) < other.x:
		horizontal = other.x - (tile.x + tile.width)
	else:
		horizontal = -1

	if vertical == -1 and horizontal == -1:
		return -1
	if vertical == -1:
		return horizontal
	if horizontal == -1:
		return vertical

	return horizontal + vertical



if __name__ == '__main__':
	map_size = 31
	m = generate(map_size)

	for y in range(map_size):
		print(''.join(m.cells[y]))

