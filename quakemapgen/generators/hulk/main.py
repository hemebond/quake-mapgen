import os
import logging
import yaml
import random

from enum import Enum
from copy import deepcopy

# from vgio.quake import map as m
# from qmap import QuakeMap
import numpy
import math


import tcod
import tcod.event
import tcod.tileset
from tcod import libtcodpy


logging.basicConfig(level=logging.ERROR)
log = logging.getLogger(__name__)


PROJECT_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
HULK_DIR = os.path.join(PROJECT_DIR, 'generators', 'hulk')
TILESETS_DIR = os.path.join(PROJECT_DIR, 'tilesets')
PREFABS_DIR = os.path.join(PROJECT_DIR, 'prefabs')



CLR_DARK_WALL = libtcodpy.Color(0, 0, 100)
CLR_DARK_GRND = libtcodpy.Color(50, 50, 150)


class clr:
	HEADER = '\033[95m'
	OKBLUE = '\033[94m'
	OKGREEN = '\033[92m'
	WARNING = '\033[93m'
	FAIL = '\033[91m'
	ENDC = '\033[0m'
	BOLD = '\033[1m'
	UNDERLINE = '\033[4m'



class Tile(Enum):
	wall = '#'
	door = '+'
	opening = '-'
	floor = '\u00B7'
	empty = ' '
	slot = '⊗'

TILE_WALL = WALL = '#'
TILE_DOOR = DOOR = '+'
TILE_OPENING = OPENING = '-'
TILE_FLOOR = FLOOR = '\u00B7'
TILE_EMPTY = EMPTY = ' '
CCROSS = '\u2297'


NORTH = 0
EAST  = 1
SOUTH = 2
WEST  = 3

UP = 0
RT = 1
DN = 2
LT = 3



tcod_context = None
tcod_console = None



class AsciiPrefabParser:
	"""
	Create instance by passing file, then pass the instance to the generator
	"""
	def __init__(self, template_file):
		self.prefabs = {}

class QuakePrefabParser:
	"""
	Parses Quake map files into prefabs
	"""
	def __init__(self, map_directory):
		self.prefabs = {}



class Waypoint:
	def __init__(self, uid, connections=[]):
		self.uid = uid
		self.connections = connections



class Entity:
	ch = ' '
	bg = (0, 0, 0)
	fg = (0, 255, 255)

	def __init__(self, name='', properties={}):
		self.name = name

		for p, v in properties.items():
			setattr(self, p, v)



class EntityInfoSpawner(Entity):
	name = 'func_spawner'
	ch = '%'
	bg = (255, 0, 0)
	fg = (0, 0, 0)



class EntityFuncDoor(Entity):
	name = 'func_door'
	ch = '+'
	bg = (0, 255, 0)
	fg = (0, 0, 0)



class TileWall:
	ch = ' '
	fg = (128, 128, 128)
	bg = (128, 128, 128)



class TileFloor:
	ch = ' '
	fg = (128, 128, 128)
	bg = (32, 32, 32)



def rotate(matrix, deg):
	if abs(deg) not in [0, 90, 180, 270, 360]:
		raise

	# number of 90 deg counter-clockwise rotations to do
	rotations = int(abs(360 - deg) / 90)

	return numpy.rot90(numpy.array(matrix), rotations)

	# if deg == 0:
	# 	return [list(r) for r in matrix]
	# elif deg > 0:
	# 	return [list(r) for r in rotate(list(zip(*matrix[::-1])), deg - 90)]
	# else:
	# 	return [list(r) for r in rotate(list(zip(*matrix)[::-1]), deg + 90)]



def matrix_to_string(matrix, color=''):
	try:
		return color + '\n'.join([''.join([str(tile.value) for tile in row]) for row in matrix]) + clr.ENDC
	except AttributeError:
		output = str(color)
		for row in matrix:
			row = ''
			for tile in row:
				try:
					row += tile.value
				except AttributeError:
					print(f"Got error on tile {tile}")
			output += row + '\n'
		return output



def v(angle):
	if angle == UP:
		return (0, -1)
	if angle == RT:
		return (1, 0)
	if angle == DN:
		return (0, 1)
	if angle == LT:
		return (-1, 0)



class Slot:
	def __init__(self, x, y, angle):
		self.x = x
		self.y = y
		self.angle = angle

	def __str__(self):
		return 'Slot(%i,%i,%s)' % (self.x, self.y, ['up', 'rt', 'dn', 'lt'][self.angle])



class Prefab:
	def __init__(self):
		self.angle = UP
		self.cells = []
		self.doors = []
		self.slots = []

		self.width  = 0
		self.height = 0

		self.tileset = None
		self.gid = 0
		self.map = None

		self.connections = []

		self.tilemap = None

	def __str__(self):
		return matrix_to_string(self.cells)



class World:
	def __init__(self, size):
		self.cells = [x[:] for x in [[Tile.empty] * size] * size]
		self.slots = []  # openings where prefabs can be placed
		self.rooms = 0

		self.size   = size
		self.width  = size
		self.height = size

		# door locations from prefabs
		self.doors = {}

		self.entities = {}

		# track all the prefabs added to the world
		# so we can convert them to tiles
		# world.prefabs[(x,y)] = prefab
		self.prefabs = {}

		# store waypoints for navigation
		self.waypoints = {}

		# store the current tilemap of the world
		self.tilemap = None


	def __str__(self):
		return matrix_to_string(self.cells)


	def get_tile(self, x, y):
		return self._tiles[(x, y)]


	def cell(self, x, y):
		return self.cells[y][x]


	def entity(self, x, y):
		if (x, y) in self.entities:
			return self.entities[(x, y)]
		return None


	def get_tilemap(self):
		tilemap = [x[:] for x in [[Tile.empty] * self.size] * self.size]

		for prefab in self.prefabs:
			log.debug(prefab)



def tpl_to_matrix(tpl:str):
	'''
	Parse a multi-line prefab template string and return a matrix (list of lists or 2d array)
	'''
	# convert into a list of lists (matrix)
	tpl = tpl.strip('\n')
	tpl = tpl.splitlines()

	# width = max(len(line) for line in tpl)
	width = len(max(tpl, key=len))

	# pad each line to make them all the same length
	tpl = [line.ljust(width, ' ') for line in tpl]

	# split each line into separate cells
	cells = [list([Tile(t) for t in line]) for line in tpl]

	return cells



def parse_prefab(cells:list):
	'''
	Parse a multi-line prefab template string and return a Prefab object
	'''
	pfb = Prefab()

	height = len(cells)
	width = len(cells[0])

	# make a list of doors, and change them to floor tiles
	for y in range(height):
		for x in range(width):
			cell = cells[y][x]

			if cell == Tile.door or cell == Tile.opening:
				cells[y][x] = Tile.floor

				for angle, neighbour in enumerate(get_adjacent_cells(cells, x, y)):
					if neighbour == Tile.empty:
						pfb.slots.append((x, y, angle))

						if cell == Tile.door:
							pfb.doors.append((x, y, angle))

	pfb.cells  = cells
	pfb.width  = width
	pfb.height = height

	return pfb



def show_slots(matrix, slots):
	cells = [row[:] for row in matrix]

	for sx, sy, sa in slots:
		cells[sy][sx] = f'{clr.WARNING}{Tile.slot.value}{clr.OKBLUE}'

	print(matrix_to_string(cells, clr.OKBLUE))



def get_adjacent_cells(matrix, x, y):
	'''
	Returns a list of tuples
	[(tile, x, y), ...]
	Out of bounds checks will return an EMPTY cell
	'''
	cells = []

	# north
	if y == 0:
		cells.append(Tile.empty)
	else:
		cells.append(matrix[y-1][x])

	# east
	if x + 1 >= len(matrix[0]):
		cells.append(Tile.empty)
	else:
		cells.append(matrix[y][x+1])

	# south
	if y + 1 >= len(matrix):
		cells.append(Tile.empty)
	else:
		cells.append(matrix[y+1][x])

	# west
	if x == 0:
		cells.append(Tile.empty)
	else:
		cells.append(matrix[y][x-1])

	return cells



def get_area(matrix, x, y, width, height):
	log.debug(f'{x}, {y}, {width}, {height}')
	cells = []

	for oy in range(height):
		for ox in range(width):
			log.debug(f'{ox},{oy}')
			wx, wy = x + ox, y + oy

			if wx >= len(matrix[0]):
				cells.append(Tile.empty)

			elif wy >= len(matrix):
				cells.append(Tile.empty)

			elif wx < 0 or wy < 0:
				cells.append(Tile.empty)

			else:
				cells.append(matrix[wy][wx])

	return cells



def look_ahead(matrix, x, y, angle, distance=3):
	# up
	if angle == 0:
		return get_area(matrix, x - 1, y - 3, 3, distance)
	# rt
	if angle == 1:
		return get_area(matrix, x + 1, y - 1, distance, 3)
	# dn
	if angle == 2:
		return get_area(matrix, x - 1, y + 1, 3, distance)
	# lt
	if angle == 3:
		return get_area(matrix, x - 3, y - 1, distance, 3)



def scan_opening(matrix, x, y, angle):
	'''
	Check to see what lies beyond a slot opening.

	Returns true if
		the next cell is a floor
		there is a floor cell two cells ahead
		there are six empty cells in front
	'''
	ax, ay = v(angle)


	# check for floor immediately in front
	wx, wy = x + ax, y + ay

	if wx < 0 or wx >= len(matrix[0]):
		return False

	if wy < 0 or wy >= len(matrix):
		return False

	if matrix[wy][wx] == Tile.floor:
		log.debug(f'slot ({x},{y}) found floor ahead ({wx},{wy})')
		return True

	if matrix[wy][wx] != Tile.empty:
		return False


	# make sure we're at least 2 cells from the world edge
	wx, wy = x + (ax * 2), y + (ay * 2)
	if wx < 0 or wx >= len(matrix[0]):
		log.debug(f'slot ({x},{y}) too close to edge')
		return False
	if wy < 0 or wy >= len(matrix):
		log.debug(f'slot ({x},{y}) too close to edge')
		return False


	# check for floor 2 cells ahead
	if matrix[wy][wx] == Tile.floor:
		log.debug(f'slot ({x},{y}) found floor 2 cells ahead ({wx+ax},{wy+ay})')
		return True

	# check for floor 3 cells ahead
	try:
		if matrix[wy][wx] == Tile.empty and matrix[wy+ay][wx+ax] == Tile.floor:
			log.debug(f'slot ({x},{y}) found floor 2 cells ahead ({wx+ax},{wy+ay})')
			return True
	except IndexError:
		pass


	# check the contents of the six cells in front
	cells = look_ahead(matrix, x, y, angle)


	log.debug(cells)
	if all(cell == Tile.empty for cell in cells):
		return True

	return False




def place_prefab(world: World, prefab: Prefab, x: int, y: int):
	'''
	Test to see if a prefab can be placed here

	x and y are the world coordinates for the top-left of the prefab
	'''
	log.debug(f'Prefab slots: {len(prefab.slots)}')

	# ----------------------------------------------------------------
	# check that the placement doesn't go outside the world boundaries
	# ----------------------------------------------------------------
	if x < 0 or y < 0:
		log.debug('Prefab placement out of bounds')
		return False
	if x + prefab.width - 1 >= world.width:
		log.debug(f'Prefab placement out of bounds (x:{x}, y:{y}, width:{prefab.width}, height:{prefab.height})')
		return False
	if y + prefab.height - 1 >= world.height:
		log.debug(f'Prefab placement out of bounds (x:{x}, y:{y}, width:{prefab.width}, height:{prefab.height})')
		return False


	# ----------------------------------------------------------------
	# check each world cell under each prefab cell
	# if world cell is not empty, can't be placed
	# ----------------------------------------------------------------
	for py in range(prefab.height):
		for px in range(prefab.width):

			pcell = prefab.cells[py][px]

			if pcell == Tile.empty:
				continue

			wx, wy = x + px, y + py  # the world coordinates
			if world.cells[wy][wx] != Tile.empty:
				log.debug('Prefab tiles overlap world tiles')
				return False


	# ----------------------------------------------------------------
	# check that each prefab slot goes to a
	# FLOOR tile or EMPTY cell on the world
	# ----------------------------------------------------------------
	for px, py, angle in prefab.slots:
		ax, ay = v(angle)  # convert to a vector tuple

		wx = x + px + ax
		wy = y + py + ay

		log.debug(f'Prefab slot ({px},{py},{angle}) pointing at ({wx},{wy})')

		if wx < 0 or wx >= world.width:
			return False
		if wy < 0 or wy >= world.height:
			return False

		if world.cells[wy][wx] not in [Tile.empty, Tile.floor]:
			log.debug(f'Prefab slot ({wx},{wy}) is looking at a {world.cells[wy][wx]}')
			return False

	# ----------------------------------------------------------------
	# Create a temporary world with both world and prefab cells
	# ----------------------------------------------------------------
	w = World(world.size)
	w.cells = [row[:] for row in world.cells]

	for py in range(prefab.height):
		for px in range(prefab.width):

			pcell = prefab.cells[py][px]

			if pcell == Tile.empty:
				continue

			wx, wy = x + px, y + py  # the world coordinates
			w.cells[wy][wx] = pcell

	w.slots = world.slots[:]

	# ----------------------------------------------------------------
	# check that each prefab and world slot has
	# enough space to place a 3x2 prefab
	# or
	# there is another slot 2 cells away
	# ----------------------------------------------------------------
	if log.level <= logging.INFO:
		print(matrix_to_string(w.cells, clr.WARNING))


	log.info(f'Checking {len(w.slots)} temp world slots')
	log.debug(str(w.slots))


	open_slots = []  # slots that are still open for placements

	for sx, sy, angle in w.slots:
		# check the tiles ahead of the slot
		log.debug(f'> > Examining temp world slot ({sx},{sy})')
		if not scan_opening(w.cells, sx, sy, angle):
			log.debug(f'Temp world slot ({sx},{sy},{angle}) blocked')
			return False

		ax, ay = v(angle)
		wx, wy = sx + ax, sy + ay

		log.info(f'Slot ({wx},{wy},{angle}) points to "{w.cells[wy][wx]}"')
		if w.cells[wy][wx] == Tile.empty:
			open_slots.append((sx, sy, angle))
		elif w.cells[wy][wx] == Tile.floor:
			log.info(f'Slot ({wx},{wy},{angle}) connects another floor, discarding')


	log.debug(f'> Scanning from {len(prefab.slots)} prefab slots')
	for px, py, angle in prefab.slots:
		wx, wy = px + x, py + y
		if not scan_opening(w.cells, wx, wy, angle):
			log.debug(f'> > Prefab slot ({wx},{wy}) blocked')

			if log.level <= logging.DEBUG:
				print(matrix_to_string(w.cells, clr.FAIL))

			return False

		ax, ay = v(angle)
		if w.cells[wy + ay][wx + ax] == Tile.empty:
			log.debug(f'> > Adding prefab slot ({px},{py}) to world ({wx},{wy})')
			open_slots.append((wx, wy, angle))

	# ----------------------------------------------------------------
	# Add room doors to the world
	# ----------------------------------------------------------------
	if not prefab.group == 'paths':
		for dx, dy, da in prefab.doors:
			wx, wy = dx + x, dy + y

			# Check that there's not a door directly in front
			a = v(da)
			in_front = (wx + a[0], wy + a[1])
			if in_front not in world.doors:
				world.doors[(wx, wy)] = EntityFuncDoor(properties={
					'angle': da
				})

	# ----------------------------------------------------------------
	# Move all the prefab information to the world
	# ----------------------------------------------------------------
	world.cells = w.cells
	# these slots are still active and unblocked in the world
	world.slots = open_slots
	# add the prefab using its origin
	world.prefabs[(x + prefab.origin[0], y + prefab.origin[1])] = prefab

	if log.level <= logging.INFO:
		print(matrix_to_string(world.cells, clr.OKGREEN))

	return True



def find_connections(layer):
	"""
	Returns a list of (x,y) tuples of open spaces adjacent to doors
	"""
	return []



def add_prefabs(world, prefabs):
	"""
	Add a prefab to a world if it is allowed to be placed

	prefabs is a list containing every orientation of every prefab
	"""

	#
	# render existing world prefabs to a single array
	#
	layer = render_world_layer(z)

	#
	# Loop through randomised list of prefabs
	#
	for prefab in shuffle(prefabs):
		#
		# Loop through each connection, open spaces adjacent to doors
		#
		for connection in find_connections(layer):
			#
			# Loop through prefab doors
			#
			for door in get_prefab_doors(prefab):
				#
				# Attempt to place prefab by matching the door to the connection
				#
				px = prefab.x
				py = prefab.y

				cx = connection.x
				cy = connection.y

				dx = door.x
				dy = door.y



def build(size, prefabs):
	world = World(size)

	# ----------------------------------------------------------------
	# Select a random room prefab to start the ball rolling
	# ----------------------------------------------------------------
	starting_prefab_group = random.choice(prefabs['rooms'])  # select a random room
	starting_prefab = random.choice(starting_prefab_group)  # select a random orientation of that room

	# get the center of the world
	wx, wy = int(world.width / 2 - starting_prefab.width / 2), int(world.height / 2 - starting_prefab.height / 2)

	if not place_prefab(world, starting_prefab, wx, wy):
		log.error('Failed to place starting prefab')
		exit()

	return build_world(world, prefabs)


def build_world(world, prefabs):
	# we will alternate/cycle between each prefab group
	ptypes = ['paths', 'rooms']

	log.info(f'Slots: {world.slots}')

	# if not world.slots:
	# 	if world.rooms < 10:
	# 		log.warning(f"World only has {world.rooms} rooms")
	# 		return None

	# 	return world

	if not world.slots:
		return world

	# pop a placement slot off the stack
	# ws is world-slot
	# wsa is world-slot-angle
	for wsx, wsy, wsa in world.slots:
		ax, ay = v(wsa)
		wsx, wsy = wsx + ax, wsy + ay
		log.debug(f'Placing in world at ({wsx},{wsy},{wsa})')

		# swap the primary prefab type/group
		# to make a semi-prioritised list of prefabs
		# ptypes = list(reversed(ptypes))
		# plist  = []
		# for ptype in ptypes:
		# 	pgroup = list(prefabs[ptype].values())
		# 	random.shuffle(pgroup)
		# 	plist += pgroup


		# make the prefab list completely random
		plist = []
		for ptype in ptypes:
			pgroup = list(prefabs[ptype])
			random.shuffle(pgroup)
			plist += pgroup
		random.shuffle(plist)


		for pfb in plist:
			# choose a random orientation
			angles = [0, 1, 2, 3]
			random.shuffle(angles)

			for angle in angles:
				p = pfb[angle]

				log.info(f"Choosing from {len(p.slots)} remaning slots")
				random.shuffle(p.slots)
				# ps* is prefab slot
				for psx, psy, psa in p.slots:
					# find the top-left corner of the prefab
					# when it's placed with the prefab slot over the
					# top of the cell in front of the world slot
					wx, wy = wsx - psx, wsy - psy

					if log.level <= logging.DEBUG:
						show_slots(world.cells, world.slots)

					if tcod_context is not None:
						matrix_to_console(world.cells)

					if place_prefab(world, p, wx, wy):
						log.info("Placed new prefab")
						if p.group == 'rooms':
							world.rooms += 1

						# Try and place the next piece
						new_world = build_world(deepcopy(world), deepcopy(prefabs))

						if new_world is not None:
							return new_world

						log.warning("Rolling back")

	# couldn't place any pieces
	log.warning("Ran out of pieces")
	return None



def load_prefabs(templates: dict) -> dict:
	prefabs = {
		'tilesize': templates['tilesize']
	}

	for template_group_name, template_group_or_list in templates['tilesets'].items():
		if isinstance(template_group_or_list, dict):
			prefabs[template_group_name] = parse_template_group((template_group_name,), template_group_or_list)
		else:
			prefabs[template_group_name] = parse_template_list((template_group_name,), template_group_or_list)

	# log.debug(prefabs)
	return prefabs



def parse_template_group(groups: tuple, templates: dict) -> list:
	prefabs = []

	for template_group_name, template_group_or_list in templates.items():
		if isinstance(template_group_or_list, dict):
			prefabs += parse_template_group(groups + (template_group_name,), template_group_or_list)
		else:
			prefabs += parse_template_list(groups + (template_group_name,), template_group_or_list)

	return prefabs



def parse_template_list(groups: tuple, templates: list) -> list:
	prefabs = []

	for idx, template in enumerate(templates):
		if template is None:
			continue

		pfb = parse_template(
			template['tpl'],
			groups,  # the path of the tileset the prefab belongs to
			idx,  # the index of the tile within the tileset
			template['map'],  # the Quake map file or directory assocated with it
		)

		prefabs.append(pfb)

	return prefabs



def parse_template(template, groups, tileset_index, map_file):
	# ----------------------------------------------------------------
	# Load in and parse the prefab templates. Generate 4 versions of
	# each prefab, one for each direction/angle.
	# ----------------------------------------------------------------
	prefab = []

	matrix = tpl_to_matrix(template)

	# calculate the origin of the prefab
	ox, oy = (
		(len(matrix[0]) - 1) // 2,
		(len(matrix) - 1) // 2
	)

	# generate a copy for each rotation
	for i in range(4):
		pfb = parse_prefab(
			rotate(matrix, 90 * i)
		)

		pfb.angle = i
		pfb.group = groups[0]
		pfb.tileset = os.path.join(*groups)
		pfb.idx = tileset_index
		pfb.map = map_file

		new_origin_x, new_origin_y = ox, oy

		# move origin point with rotation
		if i == 1:
			new_origin_x = pfb.width - 1 - oy
			new_origin_y = ox
		if i == 2:
			new_origin_x = pfb.width - 1 - ox
			new_origin_y = pfb.height - 1 - oy
		if i == 3:
			new_origin_x = oy
			new_origin_y = pfb.height - 1 - ox

		pfb.origin = (new_origin_x, new_origin_y)

		prefab.append(pfb)

	return prefab



def add_entities(world):
	return
	for wx in range(1, world.width - 1):
		for wy in range(1, world.height - 1):
			if world.cells[wy][wx] == Tile.floor:
				neighbours = get_adjacent_cells(world.cells, wx, wy)
				if neighbours.count(Tile.floor) == 1:
					world.entities[(wx, wy)] = EntityInfoSpawner({
						'Z': 32,
					})



def generate(prefabs, size=15, max_attempts=10):
	# ----------------------------------------------------------------
	# Attempt to build a level
	# ----------------------------------------------------------------

	for i in range(5):
		world = build(size, prefabs)

		if world is not None and world.rooms > 4:
			break

	if world is None:
		log.error("Couldn't create a world")
	else:
		add_entities(world)
		add_waypoints(world)

	return world



# def create_mappings(templates):
# 	log.debug(templates)
# 	mappings = {
# 		'tilesize': templates['tilesize'],
# 		'tilesets': []
# 	}

# 	for tileset, definitions in templates['tilesets'].items():
# 		ts = {
# 			'filename': tileset + '.png',
# 			'tiles': {}
# 		}

# 		for idx, d in enumerate(definitions):
# 			if d is None:
# 				continue

# 			log.debug('d: %s' % d)

# 			map_path = os.path.abspath(os.path.join(PREFABS_DIR, d['map']))
# 			ts['tiles'][str(idx)] = map_path

# 		mappings['tilesets'].append(ts)

# 	return mappings



def add_waypoints(world):
	waypoint_uid = 0

	for wy in range(world.size):
		for wx in range(world.size):
			if world.cells[wy][wx] == Tile.floor:
				world.waypoints[(wx, wy)] = Waypoint(waypoint_uid)
				waypoint_uid += 1

	for wx, wy in world.waypoints.keys():
		connections = []

		for ox, oy in [v(UP), v(RT), v(DN), v(LT)]:
			nx = wx + ox
			ny = wy + oy

			if (nx, ny) in world.waypoints:
				connections.append(world.waypoints[(nx, ny)].uid)

		world.waypoints[(wx, wy)].connections = connections



# def parse_tile_mappings(parents, children):
# 	tilesets = []

# 	if isinstance(children, dict):
# 		for tileset_dir, tileset in children.items():
# 			tilesets.append(
# 				parse_tile_mappings(parents.append(tileset_dir), children)
# 			)
# 	else:
# 		tiles = {}

# 		for idx, tile in enumerate(v):
# 			if tile is None:
# 				continue

# 			tiles[str(idx)] = os.path.abspath(os.path.join(PREFABS_DIR, tile['map']))

# 		tilesets.append((
# 			tileset_filename,
# 			tiles
# 		))

# 	return tilesets



def matrix_to_console(matrix):
	global tcod_context
	global tcod_console
	w = len(matrix[0])
	h = len(matrix)

	tcod_console.clear()

	for y in range(h):
		for x in range(w):
			ch = matrix[y][x]

			if ch == Tile.wall:
				libtcodpy.console_put_char_ex(tcod_console, x, y, ' ', (128,128,128),(128,128,128))
			if ch == Tile.floor:
				libtcodpy.console_put_char_ex(tcod_console, x, y, ' ', (128,128,128),(32,32,32))

	tcod_context.present(tcod_console)



def world_to_console(world):
	global tcod_context
	global tcod_console

	tcod_console.clear()

	for y in range(world.size):
		for x in range(world.size):
			ch = world.cell(x,y)

			if ch == Tile.wall:
				libtcodpy.console_put_char_ex(tcod_console, x, y, ' ', (128,128,128), (128,128,128))
			if ch == Tile.floor:
				libtcodpy.console_put_char_ex(tcod_console, x, y, ' ', (128,128,128), (64,64,64))

	# Show the prefab origins
	for p in world.prefabs.keys():
		x, y = p
		libtcodpy.console_put_char_ex(tcod_console, x, y, 'x', (255,255,255), (64,64,64))

	for xy, e in world.doors.items():
		x, y, = xy
		ch = [
			libtcodpy.CHAR_ARROW2_N,
			libtcodpy.CHAR_ARROW2_E,
			libtcodpy.CHAR_ARROW2_S,
			libtcodpy.CHAR_ARROW2_W,
		][e.angle]
		libtcodpy.console_put_char_ex(tcod_console, x, y, ch, e.fg, e.bg)

	# Render all entities
	for xy, e in world.entities.items():
		x, y, = xy
		libtcodpy.console_put_char_ex(tcod_console, x, y, e.ch, e.fg, e.bg)

	# Render the waypoints. Should cover all walkable terrain.
	# for pos, _ in world.waypoints.items():
	# 	x, y = pos
	# 	libtcodpy.console_put_char_ex(tcod_buffer, x, y, '\u25cb', (255,255,255), (0,0,0))

	tcod_context.present(tcod_console)



# def world_to_map(world):
# 	worldspawn = m.Entity()
# 	worldspawn.classname = 'worldspawn'
# 	worldspawn.brushes = []
# 	worldspawn.wad = "/home/james/Workspace/quake-wads/prototype_1_2.wad;/home/james/Workspace/quake-wads/jf2.wad;/home/james/Workspace/quake-wads/quake101.wad"
# 	worldspawn._tb_mod = "src"

# 	# {'/path/to/map': Map object}
# 	map_files = {}

# 	for (x, y), prefab in world.prefabs.items():
# 		if prefab.map not in map_files:
# 			file_path = os.path.join(PREFABS_DIR, prefab.map)

# 			if prefab.map[-1:] == os.sep:
# 				# a list of possible prefabs
# 				map_files[prefab.map] = []

# 				for file_name in os.listdir(file_path):
# 					if os.path.isfile(file_name):
# 						with open(file_name, 'r') as fp:
# 							map_files[prefab.map].append(m.loads(fp.read()))
# 			else:
# 				with open(file_path, 'r') as fp:
# 					map_files[prefab.map] = m.loads(fp.read())

# 		print(prefab.angle)
# 		# print(prefab.cells)
# 		# print(prefab.connections)
# 		# print(prefab.doors)
# 		# print(prefab.gid)
# 		# print(prefab.group)
# 		# print(prefab.height)
# 		# print(prefab.idx)
# 		# print(prefab.map)
# 		# print(prefab.origin)
# 		# print(prefab.slots)
# 		# print(prefab.tilemap)
# 		# print(prefab.tileset)
# 		# print(prefab.width)

# 		if type(map_files[prefab.map]) == 'list':
# 			prefab_map = random.choice(map_files[prefab.map])
# 		else:
# 			prefab_map = map_files[prefab.map]

# 		transformed_prefab = transform_prefab(prefab_map, x * 128, y * 128, 0, (prefab.angle * 90))

# 		for entity in transformed_prefab:
# 			for brush in entity.brushes:
# 				worldspawn.brushes.append(brush)

# 	with open('out.map', 'w') as f:
# 		f.write('// Game: Quake\n')
# 		f.write('// Format: Standard\n')
# 		f.write(m.dumps([worldspawn]))

# 	return ""



# if __name__ == '__main__':
# 	#
# 	# This is just a test of the transformations
# 	#
# 	worldspawn = m.Entity()
# 	worldspawn.classname = 'worldspawn'
# 	worldspawn.brushes = []
# 	# worldspawn.wad = ''
# 	worldspawn.wad = "/home/james/Workspace/quake-wads/prototype_1_2.wad;/home/james/Workspace/quake-wads/jf2.wad;/home/james/Workspace/quake-wads/quake101.wad"
# 	worldspawn._tb_mod = "src"

# 	with open('test2.map', 'r') as f:
# 		prefab = m.loads(f.read())

# 	for angle in (0, 90, 180, 270):
# 		# altered_prefab = rotate_prefab(prefab, angle)
# 		# altered_prefab = translate_prefab(altered_prefab, 512, 128, 0)

# 		altered_prefab = transform_prefab(prefab, 512, 128, 0, angle)

# 		for entity in altered_prefab:
# 			for brush in entity.brushes:
# 				worldspawn.brushes.append(brush)

# 	with open('out.map', 'w') as f:
# 		f.write('// Game: Quake\n')
# 		f.write('// Format: Standard\n')
# 		f.write(m.dumps([worldspawn]))




#
# Output Module
#


if __name__ == '__main__':
	import argparse

	parser = argparse.ArgumentParser()
	parser.add_argument(
		"--size",
		default=31,
		type=int,
		help="The size of the map",
	)
	parser.add_argument(
		"--templates",
		default="./quakemapgen/prefabs/tech.yml",
		type=argparse.FileType('r'),
		help="Path to the YAML file containing the prefab templates",
	)
	parser.add_argument(
		"--log-level",
		choices=set(name.lower() for name in logging._nameToLevel),
		default='warning',
		help="Provide logging level. Example --log-level DEBUG",
	)
	parser.add_argument(
		"--tcod",
		action='store_true',
		default=True,
		help="Use a TCOD console to display the map",
	)
	parser.add_argument(
		"--map",
		action="store_true",
		help="Generate a MAP file",
	)

	options = parser.parse_args()
	log.setLevel(logging._nameToLevel.get(options.log_level.upper()))


	if options.templates:
		templates = yaml.safe_load(options.templates)
		options.templates.close()
	else:
		# If no template file provided on the CLI
		# default to the prototype templates and prefabs
		with open(os.path.join(PREFABS_DIR, 'prototype.yml'), 'r') as template_file:
			templates = yaml.safe_load(template_file)


	if options.tcod:
		tileset = tcod.tileset.load_tilesheet(
			"dejavu10x10_gs_tc.png", 32, 8, tcod.tileset.CHARMAP_TCOD,
		)
		# Create the main console.
		console = tcod.console.Console(
			# WIDTH,
			options.size,
			# HEIGHT,
			options.size,
			order="F",
		)
		context = tcod.context.new(  # New window for a console of size columns×rows.
			# columns=console.width,
			columns=options.size,
			# rows=console.height,
			rows=options.size,
			tileset=tileset,
		)

		# tcod.console_set_custom_font('arial12x12.png', tcod.FONT_LAYOUT_TCOD | tcod.FONT_TYPE_GREYSCALE)
		# tcod_root_console = tcod.console_init_root(
		# 	options.size,
		# 	options.size,
		# 	order='F',
		# 	renderer=tcod.RENDERER_SDL2,
		# 	vsync=True
		# )
		tcod_context = context
		# tcod_buffer = tcod.console.Console(options.size, options.size)
		tcod_console = console


	prefabs = load_prefabs(templates)
	world = generate(size=options.size, prefabs=prefabs)

	if not world:
		log.error('World generation failed')
		exit(1)

	if options.map:
		# for origin, pfb in world.prefabs.items():
		# 	log.debug(str(origin))
		# 	log.debug(pfb.map)
		# map_file_string = world_to_map(world, prefabs_dir=PREFABS_DIR)
		map_file_string = world_to_map(world)
		with open('result.map', 'w') as out_file:
			# data = m.dumps(entities)
			out_file.write(map_file_string)

	# if options.tmx:
	# 	tmx = convert_to_tmx(world, options.templates.name)

	# 	import tempfile
	# 	tmx_file_path = tempfile.mktemp(suffix='.tmx')
	# 	tmx.save(tmx_file_path)


	# 	import json
	# 	map_dict = create_mappings(templates)
	# 	map_file_path = os.path.splitext(tmx_file_path)[0] + '.mappings'
	# 	with open(map_file_path, 'w') as out_file:
	# 		json.dump(map_dict, out_file, indent=8)
	# 		log.info(f'Created mappings file {map_file_path}')


	# 	log.info(f'\nSuccessfully created TMX file {tmx_file_path}')
	# 	if log.level <= logging.INFO:
	# 		log.info('\n' + str(world))
	# 	print(tmx_file_path)

	if options.tcod:
		log.info('Rendering world to tcod console')
		world_to_console(world)

		while True:
			for event in tcod.event.wait():
				context.convert_event(event)  # Sets tile coordinates for mouse events.

				match event:
					case tcod.event.Quit():
						raise SystemExit()

		tcod_context.close()
