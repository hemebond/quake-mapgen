import yaml

from .main import *

from tmxlib.map import Map
from tmxlib.tileset import ImageTileset, Tileset


PROJECT_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
TILESETS_DIR = os.path.join(PROJECT_DIR, 'tilesets')

template_file_path = os.path.join(PROJECT_DIR, 'prefabs', 'prototype.yml')

with open(template_file_path, 'r') as template_file:
	yml = yaml.safe_load(template_file)
	prefabs = load_prefabs(yml)


def test_tmx():
	tm = Map((200,200), (1, 1))

	# Load the tilesets and create mappings
	ts_rooms = ImageTileset.open(os.path.join(TILESETS_DIR, 'rooms.tsx'))
	ts_rooms.source = os.path.join(TILESETS_DIR, 'rooms.tsx')
	ts_paths = ImageTileset.open(os.path.join(TILESETS_DIR, 'paths.tsx'))
	ts_paths.source = os.path.join(TILESETS_DIR, 'rooms.tsx')

	tl = tmxlib.TileLayer(tm, "worldspawn")

	tiles = {
		'paths_i1': ts_paths[0],
		'paths_t1': ts_paths[1],
		'paths_c1': ts_paths[2],
		'paths_e1': ts_paths[3],
		'paths_h5': ts_paths[4],
		'paths_h4': ts_paths[5],
		'paths_h3': ts_paths[6],
		'paths_h2': ts_paths[7],
		'paths_h1': ts_paths[8],
		'paths_b1': ts_paths[9],
		'paths_b2': ts_paths[10],

		'rooms_i3':   ts_rooms[10],
		'rooms_t3':   ts_rooms[11],
		'rooms_c3':   ts_rooms[12],
		'rooms_h3':   ts_rooms[13],
		'rooms_e3':   ts_rooms[14],
		'rooms_r1':   ts_rooms[26],
		'rooms_i3x4': ts_rooms[15],
		'rooms_i4x4': ts_rooms[25],
	}

	y = 0
	for i in range(len(list(tiles.keys()))):
		label = list(tiles.keys())[i]
		tile = tiles[label]
		tl[(i % 10) * 9,y * 9] = tile
		y += 1

	tm.layers.append(tl)
	tm.save('/tmp/hulk_test.tmx')


def test_full_build():

	i = prefabs['paths'][0]
	t = prefabs['paths'][1]
	c = prefabs['paths'][2]
	e = prefabs['paths'][3]
	h5 = prefabs['paths'][4]
	h4 = prefabs['paths'][5]
	h3 = prefabs['paths'][6]
	h2 = prefabs['paths'][7]
	h1 = prefabs['paths'][8]
	i3 = prefabs['rooms'][10]
	t3 = prefabs['rooms'][11]
	c3 = prefabs['rooms'][12]
	e3 = prefabs['rooms'][14]


	world = World(34, 45)

	assert place_prefab(world, e[2], 29, 0) == True
	assert len(world.slots) == 1

	assert place_prefab(world, c3[0], 12, 1) == True
	assert len(world.slots) == 3

	assert place_prefab(world, h3[0], 17, 2) == True
	assert place_prefab(world, t[2], 20, 2) == True
	assert place_prefab(world, c[3], 23, 2) == True
	assert place_prefab(world, t[1], 29, 2) == True
	assert place_prefab(world, e[3], 32, 2) == True
	assert len(world.slots) == 4

	assert place_prefab(world, h3[1], 20, 5) == True
	assert place_prefab(world, t[1], 23, 5) == True
	assert place_prefab(world, h3[0], 26, 5) == True
	assert place_prefab(world, i[0], 29, 5) == True
	assert place_prefab(world, e[3], 32, 5) == True
	assert len(world.slots) == 4

	assert place_prefab(world, h2[1], 13, 6) == True
	assert len(world.slots) == 4

	assert place_prefab(world, e3[3], 8, 7) == True
	assert len(world.slots) == 5

	assert place_prefab(world, i[0], 13, 8) == True
	assert place_prefab(world, h4[0], 16, 8) == True
	assert place_prefab(world, t[0], 20, 8) == True
	assert place_prefab(world, t[0], 23, 8) == True
	assert place_prefab(world, h3[0], 26, 8) == True
	assert place_prefab(world, i[0], 29, 8) == True
	assert place_prefab(world, e[3], 32, 8) == True
	assert len(world.slots) == 2

	assert place_prefab(world, h4[1], 13, 11) == True
	assert place_prefab(world, e[0], 29, 11) == True
	assert len(world.slots) == 1

	assert place_prefab(world, c3[0], 5, 15) == True
	assert place_prefab(world, t3[2], 12, 15) == True
	assert len(world.slots) == 4

	assert place_prefab(world, h2[0], 10, 16) == True
	assert place_prefab(world, h5[0], 17, 16) == True
	assert place_prefab(world, e[3], 22, 16) == True
	assert len(world.slots) == 1

	assert place_prefab(world, c[2], 0, 20) == True
	assert place_prefab(world, h3[0], 3, 20) == True
	assert place_prefab(world, t[3], 6, 20) == True
	assert len(world.slots) == 2

	assert place_prefab(world, h5[1], 0, 23) == True
	assert place_prefab(world, t3[3], 5, 23) == True
	assert place_prefab(world, c3[1], 15, 23) == True
	assert len(world.slots) == 5

	assert place_prefab(world, h5[0], 10, 24) == True
	assert len(world.slots) == 3

	assert place_prefab(world, c[1], 0, 28) == True
	assert place_prefab(world, h3[0], 3, 28) == True
	assert place_prefab(world, i[0], 6, 28) == True
	assert place_prefab(world, c[3], 9, 28) == True
	assert place_prefab(world, c[1], 16, 28) == True
	assert place_prefab(world, e[3], 19, 28) == True
	assert len(world.slots) == 2

	assert place_prefab(world, h5[1], 6, 31) == True
	assert place_prefab(world, h5[1], 9, 31) == True
	assert len(world.slots) == 2

	assert place_prefab(world, h4[1], 6, 36) == True
	assert place_prefab(world, h4[1], 9, 36) == True
	assert len(world.slots) == 2

	assert place_prefab(world, e[1], 1, 40) == True
	assert place_prefab(world, t[2], 3, 40) == True
	assert place_prefab(world, i[0], 6, 40) == True
	assert place_prefab(world, i[0], 9, 40) == True
	assert place_prefab(world, e[3], 12, 40) == True
	assert len(world.slots) == 3

	assert place_prefab(world, e[0], 3, 43) == True
	assert place_prefab(world, e[0], 6, 43) == True
	assert place_prefab(world, e[0], 9, 43) == True
	assert len(world.slots) == 0

	final_map = tpl_to_matrix('''
                             ###
            #####            #·#
            #···##########   #·###
            #············#   #···#
            #···#####·##·#   #·###
            ##·##   #·##·#####·###
             #·#    #·##·········#
        ######·#    #·##·#####·###
        #···##·######·##·#####·###
        #························#
        #···##·###############·###
        ######·#             #·#
             #·#             ###
             #·#
             #·#
     #####  ##·##
     #···####···########
     #·················#
     #···####···########
     ##·##  #####
#######·#
#·······#
#·#####·#
#·#  ##·##     #####
#·#  #···#######···#
#·#  #·············#
#·#  #···#######···#
#·#  ##·##     ##·##
#·#####·####    #·###
#··········#    #···#
#######·##·#    #####
      #·##·#
      #·##·#
      #·##·#
      #·##·#
      #·##·#
      #·##·#
      #·##·#
      #·##·#
      #·##·#
 ######·##·###
 #···········#
 ###·##·##·###
   #·##·##·#
   #########''')
	for y in range(world.height):
		for x in range(world.width):
			assert world.cells[y][x] == final_map[y][x]
