import pytest
import yaml

from .main import *



with open(os.path.join(PREFABS_DIR, 'prototype.yml'), 'r') as template_file:
	yml = yaml.safe_load(template_file)
	prefabs = load_prefabs(yml)



class TestParser:
	def test_parse_r1(self):
		p = prefabs['rooms'][26][0]

		assert len(p.doors) == 4
		assert (2,1) in p.doors
		assert (0,4) in p.doors
		assert (4,3) in p.doors
		assert (5,4) in p.doors

		assert (2,1,1) in p.slots
		assert (0,4,3) in p.slots
		assert (4,3,0) in p.slots
		assert (5,4,1) in p.slots

	def test_parse_e1(self):
		assert prefabs['paths'][3][0].slots == [(1,0,0)]

	def test_parse_h1(self):
		assert prefabs['paths'][8][0].slots == [(0,1,1), (0,1,3)]

	def test_parse_h2(self):
		assert prefabs['paths'][7][0].slots == [(0,1,3), (1,1,1)]
		assert prefabs['paths'][7][1].slots == [(1,0,0), (1,1,2)]

	def test_parse_i3(self):
		assert len(prefabs['rooms'][10][0].slots) == 4

	def test_double_doors(self):
		yml_string = '''
tilesize: 128 # Quake units per TMX tile
tilesets:
  # paths.tsx without extension, relative to tilesets directory
  rooms:
    - map: "./tech/rooms/3x4/e2.map"
      tpl: |
        #####
        #···#
        +···#
        +···#
        #···#
        #####
'''
		yml = yaml.safe_load(yml_string)
		prefabs = load_prefabs(yml)

		p = prefabs['rooms'][0][0]

		assert len(p.doors) == 2

class TestOrigin:
	def test_r1(self):
		p = prefabs['rooms'][26]
		assert p[0].origin == (2, 2)
		assert p[1].origin == (3, 2)
		assert p[2].origin == (3, 3)
		assert p[3].origin == (2, 3)

	def test_h4(self):
		p = prefabs['paths'][5]

		assert p[0].origin == (1, 1)
		assert p[1].origin == (1, 1)
		assert p[2].origin == (2, 1)
		assert p[3].origin == (1, 2)

	def test_h2(self):
		p = prefabs['paths'][7]

		assert p[0].origin == (0, 1)
		assert p[1].origin == (1, 0)
		assert p[2].origin == (1, 1)
		assert p[3].origin == (1, 1)

	def test_i3(self):
		p = prefabs['rooms'][10]

		assert p[0].origin == (2, 2)
		assert p[1].origin == (2, 2)
		assert p[2].origin == (2, 2)
		assert p[3].origin == (2, 2)

	def test_i3x4(self):
		p = prefabs['rooms'][15]

		assert p[0].origin == (2, 2)
		assert p[1].origin == (3, 2)
		assert p[2].origin == (2, 3)
		assert p[3].origin == (2, 2)

	def test_i4x4(self):
		p = prefabs['rooms'][25]

		assert p[0].origin == (2, 2)
		assert p[1].origin == (3, 2)
		assert p[2].origin == (3, 3)
		assert p[3].origin == (2, 3)
