import pytest
import yaml


from .main import *


template_file_path = os.path.join(PROJECT_DIR, 'prefabs', 'prototype.yml')
with open(template_file_path, 'r') as template_file:
	yml = yaml.safe_load(template_file)
	prefabs = load_prefabs(yml)



def test_get_adjacent():
	cells = [[1,2,3],
	         [4,5,6],
	         [7,8,9]]
	assert get_adjacent_cells(cells, 1, 1) == [2, 6, 8, 4]
	assert get_adjacent_cells(cells, 0, 0) == [' ', 2, 4, ' ']


def test_look_ahead():
	#            0     1     2     3     4
	cells = [['0_0','1_0','2_0','3_0','4_0'], # 0
	         ['0_1','1_1','2_1','3_1','4_1'], # 1
	         ['0_2','1_2','2_2','3_2','4_2'], # 2
	         ['0_3','1_3','2_3','3_3','4_3']] # 3
	assert look_ahead(cells, 2, 0, 2) == ['1_1','2_1','3_1',
	                                      '1_2','2_2','3_2',
	                                      '1_3','2_3','3_3',]


def test_get_area():
	#          0   1   2   3   4
	cells = [['0','1','·','3',' '], # 0
	         ['1',' ',' ',' ',' '], # 1
	         ['2',' ',' ',' ',' '], # 2
	         ['3',' ','@',' ',' ']] # 3

	assert get_area(cells, 1, 0, 3, 2) == ['1','·','3',' ',' ',' ']
	assert get_area(cells, 0, 0, 3, 2) == ['0','1','·','1',' ',' ']
	assert get_area(cells, 0, 1, 2, 3) == ['1',' ','2',' ','3',' ']


def test_scan_opening():
	#          0   1   2   3   4
	cells = [['#','#','·','#',' '], # 0
	         ['#',' ',' ',' ',' '], # 1
	         ['#',' ',' ',' ',' '], # 2
	         ['#',' ','@',' ',' ']] # 3

	assert scan_opening(cells, 2, 3, 0) == True # a 3x2 gap
	assert scan_opening(cells, 2, 2, 0) == True # because there's a floor ahead

	assert scan_opening(cells, 2, 1, 0) == True # because there's a floor there
	assert scan_opening(cells, 3, 2, 0) == False
	assert scan_opening(cells, 1, 3, 0) == False

	assert scan_opening(cells, 1, 2, 3) == False
	assert scan_opening(cells, 2, 2, 1) == True

	assert scan_opening(world.cells, 3, 5, 3) == True



world = World(11, 11)
#                0   1   2   3   4   5   6   7   8   9   10
world.cells = [[' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '], # 0
               [' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '], # 1
               [' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '], # 2
               [' ',' ',' ','#','#','·','#','#',' ',' ',' '], # 3
               [' ',' ',' ','#','·','·','·','#',' ',' ',' '], # 4
               [' ',' ',' ','·','·','·','·','·',' ',' ',' '], # 5
               [' ',' ',' ','#','·','·','·','#',' ',' ',' '], # 6
               [' ',' ',' ','#','#','·','#','#',' ',' ',' '], # 7
               [' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '], # 8
               [' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '], # 9
               [' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ']] # 10
world.width = 11
world.height = 11
world.slots = [
	(5, 3, 0), # north doorway
	(7, 5, 1), # east doorway
	(5, 7, 2), # south doorway
	(3, 5, 3), # west doorway
]
c = prefabs['paths'][2]
e = prefabs['paths'][3]
i = prefabs['paths'][0]
t = prefabs['paths'][1]
h5 = prefabs['paths'][4]
h2 = prefabs['paths'][7]
h1 = prefabs['paths'][8]
e3 = prefabs['rooms'][14]
t3 = prefabs['rooms'][11]
c3 = prefabs['rooms'][12]

class TestPrefabPlacement:
	def test_out_of_bounds(self):
		assert place_prefab(world, h5[0], 8, 4) == False
		assert place_prefab(world, e3[0], 3, -2) == False


	def test_place_prefab(self):
		assert place_prefab(world, e[0], 4, 8) == True
		assert place_prefab(world, e[0], 5, 8) == False


	def test_blocked_slots(self):
		world = World(15,15)

		assert place_prefab(world, t[1], 0, 3) == True
		assert len(world.slots) == 3
		assert place_prefab(world, c[3], 3, 3) == True
		assert len(world.slots) == 3
		assert place_prefab(world, t[1], 0, 6) == True
		assert len(world.slots) == 4
		assert place_prefab(world, h2[1], 3, 6) == False
		assert len(world.slots) == 4

		world = World(14, 10)

		assert place_prefab(world, t[0], 2, 7) == True
		assert place_prefab(world, t[0], 5, 7) == True
		assert place_prefab(world, c[2], 2, 4) == True
		assert place_prefab(world, h1[1], 5, 6) == False
		assert len(world.slots) == 4

		assert place_prefab(world, t[2], 6, 0) == True
		assert place_prefab(world, t[2], 9, 0) == True
		assert place_prefab(world, c[0], 9, 3) == True
		assert place_prefab(world, c[1], 6, 3) == False
		assert len(world.slots) == 8

		world = World(8, 14)

		assert place_prefab(world, c3[3], 0, 9) == True
		assert place_prefab(world, t[1], 2, 3) == True
		assert place_prefab(world, h1[1], 1, 8) == False
		assert place_prefab(world, e[0], 2, 6) == False

	def test_blocking_4(self):
		world = World(17,7)

		#          #####
		#          #···#
		# #·####·# ·····
		# #······# #···#
		# #·###### ##·##
		#
		#

		assert place_prefab(world, t3[0], 9, 0) == True
		assert len(world.slots) == 3
		assert place_prefab(world, t[1], 0, 2) == True
		assert len(world.slots) == 6
		assert place_prefab(world, h2[0], 3, 2) == True
		assert len(world.slots) == 6
		assert place_prefab(world, c[0], 5, 2) == False
