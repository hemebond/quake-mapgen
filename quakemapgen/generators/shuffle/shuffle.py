from random import randint
from types import SimpleNamespace
from math import floor


# https://jsfiddle.net/4etq5612/

EMPTY_TILE = None
FLOOR_TILE = 1
WALL_TILE = 2


PREFABS = {

}


class Level:
	def __init__(self, size, logger=None):
		self.map = None
		self.map_size = size
		self.rooms = []

	def generate(self, room_min_size=5, room_max_size=15):
		self.map = []

		for x in range(self.map_size):
			self.map.append([])
			for y in range(self.map_size):
				self.map[x].append(EMPTY_TILE)

		room_count = randint(10, 20)
		min_size = room_min_size
		max_size = room_max_size

		attempts = 0
		i = 0
		while i < room_count:
			if attempts > room_count * 1000:
				exit(1)

			attempts += 1

			room = SimpleNamespace()

			room.x = randint(1, self.map_size - max_size - 1)
			room.y = randint(1, self.map_size - max_size - 1)
			room.w = randint(min_size, max_size)
			room.h = randint(min_size, max_size)

			if self.does_collide(room):
				continue

			room.w -= 1
			room.h -= 1

			self.rooms.append(room)

			i += 1

		self.squash_rooms()

		for i in range(room_count):
			room_a = self.rooms[i]
			room_b = self.find_closest_room(room_a)

			point_a = {
				'x': randint(room_a.x, room_a.x + room_a.w),
				'y': randint(room_a.y, room_a.y + room_a.h),
			}
			point_b = {
				'x': randint(room_b.x, room_b.x + room_b.w),
				'y': randint(room_b.y, room_b.y + room_b.h),
			}

			while (point_b['x'] != point_a['x']) or (point_b['y'] != point_a['y']):
				if point_b['x'] != point_a['x']:
					if point_b['x'] > point_a['x']:
						point_b['x'] -= 1
					else:
						point_b['x'] += 1
				elif point_b['y'] != point_a['y']:
					if point_b['y'] > point_a['y']:
						point_b['y'] -= 1
					else:
						point_b['y'] += 1

				self.map[point_b['x']][point_b['y']] = FLOOR_TILE

		for i in range(room_count):
			room = self.rooms[i]

			for x in range(room.x, room.x + room.w):
				for y in range(room.y, room.y + room.h):
					self.map[x][y] = FLOOR_TILE

		for x in range(self.map_size):
			for y in range(self.map_size):
				if self.map[x][y] == FLOOR_TILE:
					for xx in range(x - 1, x + 2):
						for yy in range(y - 1, y + 2):
							if self.map[xx][yy] == EMPTY_TILE:
								self.map[xx][yy] = WALL_TILE


	def find_closest_room(self, room):
		mid = {
			'x': room.x + (room.w / 2),
			'y': room.y + (room.h / 2),
		}
		closest = None
		closest_distance = 1000

		for i in range(len(self.rooms)):
			check = self.rooms[i]

			if check == room:
				continue

			check_mid = {
				'x': check.x + (check.w / 2),
				'y': check.y + (check.h / 2),
			}

			distance = min(abs(mid['x'] - check_mid['x']) - (room.w / 2) - (check.w / 2), abs(mid['y'] - check_mid['y']) - (room.h / 2) - (check.h / 2))

			if distance < closest_distance:
				closest_distance = distance
				closest = check

		return closest


	def squash_rooms(self):
		for i in range(10):
			for j in range(len(self.rooms)):
				room = self.rooms[j]

				while True:
					old_position = {
						'x': room.x,
						'y': room.y,
					}

					# Shuffle the rooms towards the top-left of the map
					# if room.x > 1:
					# 	room.x -= 1
					# if room.y > 1:
					# 	room.y -= 1
					# if room.x == 1 and room.y == 1:
					# 	break

					# Shuffle the rooms towards the center of the map
					if room.x > self.map_size / 2:
						room.x -= 1
					elif room.x < self.map_size / 2:
						room.x += 1
					if room.y > self.map_size / 2:
						room.y -= 1
					elif room.y < self.map_size / 2:
						room.y += 1
					if room.x == self.map_size / 2 and room.y == self.map_size / 2:
						break
					if self.does_collide(room, j):
						room.x = old_position['x']
						room.y = old_position['y']
						break


	def does_collide(self, room, ignore=None):
		for i in range(len(self.rooms)):
			if i == ignore:
				continue

			check = self.rooms[i]
			if not ((room.x + room.w + 1 < check.x) or
			        (room.x > check.x + check.w + 1) or
			        (room.y + room.h + 1 < check.y) or
			        (room.y > check.y + check.h + 1)):
				return True

		return False


if '__main__' == __name__:
	m = Level(64)
	m.generate()

	STAR = '\u26E4'

	for y in range(m.map_size):
		r = ''

		for x in range(m.map_size):
			tile = m.map[x][y]
			if tile == EMPTY_TILE:
				c = ' ' # void
			elif tile == FLOOR_TILE:
				c = '·' # floor
			elif tile == WALL_TILE:
				c = '#' # wall

			r += c

		print(r)
