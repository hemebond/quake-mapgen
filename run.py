#!/usr/bin/env python3

import logging
import subprocess
import os
import shutil
import yaml
import json

from quakemapgen.generators.hulk.main import load_prefabs, generate, convert_to_tmx, create_mappings



logging.basicConfig(level=logging.ERROR)
log = logging.getLogger(__name__)



def tpl_to_tmx(templates, **kwargs):
	world = generate(kwargs.get('size', 15), kwargs.get('size', 15), prefabs=load_prefabs(templates))

	if world is not None:
		tmx = convert_to_tmx(world)

		import tempfile
		tmx_file_path = tempfile.mktemp(suffix='.tmx')
		tmx.save(tmx_file_path)

		return tmx_file_path
	else:
		log.error("Failed to generate world")

	return None



def map_to_bsp(map_file_path, wadpath):
	'''
	Compile a MAP file to a BSP and return the BSP file path
	'''
	working_dir = os.path.dirname(map_file_path)
	map_basename, map_ext = os.path.splitext(os.path.basename(map_file_path))
	bsp_file_path = os.path.join(working_dir, map_basename + '.bsp')


	qbsp_command = ['qbsp', '-leaktest', '-bsp2', map_file_path, bsp_file_path, '-wadpath', wadpath]
	try:
		result = subprocess.run(qbsp_command, check=True, capture_output=True)
	except subprocess.CalledProcessError as e:
		log.error(' '.join(qbsp_command))
		log.error(e.stdout.decode('UTF-8'))
		return None
	else:
		log.info(result.stdout.decode('UTF-8'))


	vis_command = ['vis', bsp_file_path]
	try:
		result = subprocess.run(vis_command, check=True, capture_output=True)
	except subprocess.CalledProcessError as e:
		log.error(' '.join(vis_command))
		log.error(e.stdout.decode('UTF-8'))
		return None
	else:
		log.info(result.stdout.decode('UTF-8'))


	light_command = ['light', bsp_file_path]
	try:
		result = subprocess.run(light_command, check=True, capture_output=True)
	except subprocess.CalledProcessError as e:
		log.error(' '.join(light_command))
		log.error(e.stdout.decode('UTF-8'))
		return None
	else:
		log.info(result.stdout.decode('UTF-8'))

	return bsp_file_path



def tmx_to_map(tmx_file_path):
	'''
	Convert a TMX file to a MAP file and return the path of the MAP file
	'''
	WORKING_DIR = os.path.dirname(tmx_file_path)
	MAP_BASE_NAME, _ = os.path.splitext(os.path.basename(tmx_file_path))
	try:
		MAPPING_FILE_PATH = os.path.join(WORKING_DIR, MAP_BASE_NAME + '.mappings')
	except:
		log.error(tmx_file_path)
		log.error(WORKING_DIR)
		log.error(MAP_BASE_NAME)
		exit()


	map_file_path = os.path.join(WORKING_DIR, MAP_BASE_NAME + '.map')


	tmx2map_command = ['python', './tmx2map/tmx2map.py', '--quiet', '-d', map_file_path, tmx_file_path, MAPPING_FILE_PATH]


	try:
		result = subprocess.run(tmx2map_command, check=True, capture_output=True)
	except subprocess.CalledProcessError as e:
		log.error(e.stdout.decode('UTF-8'))
		return None
	else:
		log.info(result.stdout.decode('UTF-8'))

	return map_file_path



def main():
	import argparse


	levels = {
		'critical': logging.CRITICAL,
		'error': logging.ERROR,
		'warning': logging.WARNING,
		'info': logging.INFO,
		'debug': logging.DEBUG,
	}


	parser = argparse.ArgumentParser()

	tmx_or_map = parser.add_mutually_exclusive_group()
	tmx_or_map.add_argument(
		"--templates",
		help="Generate a new map from this templates file."
	)
	tmx_or_map.add_argument(
		"--tmx",
		help="Compile a MAP from this .tmx file.",
	)
	tmx_or_map.add_argument(
		"--map",
		help="Compile a BSP from this .map file.",
	)

	parser.add_argument(
		"--size",
		type=int,
		help="Size of the map in tiles, e.g., 31",
	)

	parser.add_argument(
		"--wadpath",
		help="Path to the directory of WAD files. Required to compile a BSP.",
	)

	parser.add_argument(
		"--maps_dir",
		help="Path to the Quake maps directory to put the BSP file."
	)

	parser.add_argument(
		"--log",
		choices=levels.keys(),
		default='warning',
		help="Provide logging level. Example --log debug",
	)


	options = parser.parse_args()
	level = levels.get(options.log)
	log.setLevel(level)


	tmx_file_path = options.tmx
	map_file_path = options.map
	bsp_file_path = None


	if options.map and options.wadpath is None:
		log.error('the following arguments are required: --wadpath')
		parser.print_help()
		exit()


	if options.templates:
		with open(options.templates, 'r') as f:
			templates = yaml.safe_load(f)

		tmx_file_path = tpl_to_tmx(templates, size=options.size)

		if tmx_file_path is not None:
			mapping_dict = create_mappings(templates)
			mapping_file_path = os.path.splitext(tmx_file_path)[0] + '.mappings'
			with open(mapping_file_path, 'w') as mapping_file:
				json.dump(mapping_dict, mapping_file, indent=8)
				log.info(f'Created mappings file {mapping_file_path}')
		else:
			log.error("Failed to create world/TMX")
			exit(1)


	if tmx_file_path:
		map_file_path = tmx_to_map(os.path.abspath(tmx_file_path))

		if map_file_path is not None:
			log.info(f'Converted {options.tmx} to {map_file_path}')
		else:
			log.error(f'Failed to convert {tmx_file_path} into map file')
			exit(1)


	if map_file_path and options.wadpath is not None:
		bsp_file_path = map_to_bsp(os.path.abspath(map_file_path), os.path.abspath(options.wadpath))
		log.info(f'Compiled {map_file_path} into {bsp_file_path}')


	if bsp_file_path and options.maps_dir:
		dst = os.path.abspath(options.maps_dir)
		shutil.copy(bsp_file_path, dst)
		log.info(f'Copied BSP file {bsp_file_path} to {dst}')


if __name__ == '__main__':
	main()
