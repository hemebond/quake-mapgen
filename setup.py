from setuptools import setup, find_packages
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
	long_description = f.read()


setup(
	name='quakemapgen',
	version='0.0.1',
	python_requires='>=3.7',
	description='A random Quake map generator',
	long_description=long_description,
	long_description_content_type="text/markdown",
	url='https://gitlab.com/hemebond/quake-mapgen',
	author='James O\'Neill',
	author_email='hemebond+quakemapgen@gmail.com',
	license="MIT",
	classifiers=[
		'Development Status :: 3 - Alpha',
		'Natural Language :: English',
		'License :: OSI Approved :: MIT License',
		'Programming Language :: Python :: 3 :: Only',
		'Programming Language :: Python :: 3.8',
		'Topic :: Games/Entertainment :: First Person Shooters',
	],
	keywords='python quake procedural generation map level',
	packages=find_packages(exclude=['docs', 'tests']),
	install_requires=[
		'tmxlib @ https://github.com/hemebond/pytmxlib/tarball/pointobject#egg=tmxlib',
		'pyyaml',
	],
	# entry_points={
	#     'console_scripts': [
	#         'helloworld=pypkgtemp.__main__:main'
	#     ]
	# }
	project_urls={  # Optional
		'Bug Reports': 'https://gitlab.com/hemebond/quake-mapgen/-/issues',
		'Source': 'https://gitlab.com/hemebond/quake-mapgen',
	},
)
